var classTangle_1_1Examples_1_1Notifications =
[
    [ "Awake", "classTangle_1_1Examples_1_1Notifications.html#ac9907c9e896c41e8d78968843c9d2b38", null ],
    [ "DisplayFailure", "classTangle_1_1Examples_1_1Notifications.html#a70a67447e295b91000f3bc23f6a593da", null ],
    [ "DisplayNotification", "classTangle_1_1Examples_1_1Notifications.html#a14ebde7ffdb05d97f4cd59a391454812", null ],
    [ "DisplaySuccess", "classTangle_1_1Examples_1_1Notifications.html#a4f47b7015dc2908486c11610e3a68057", null ],
    [ "HideNotification", "classTangle_1_1Examples_1_1Notifications.html#a24c833b01d04d6f41ded220765032d6e", null ],
    [ "RevealNotification", "classTangle_1_1Examples_1_1Notifications.html#acf58a1ad5b92f7c74f95d4d9493d3961", null ],
    [ "_displayTime", "classTangle_1_1Examples_1_1Notifications.html#a5ca250d771d399802088ca908b805644", null ],
    [ "_failureColor", "classTangle_1_1Examples_1_1Notifications.html#a441d299478f211285869c2b3c57c7d64", null ],
    [ "_hideTime", "classTangle_1_1Examples_1_1Notifications.html#a9770b7784944f34925b50c3c0661a043", null ],
    [ "_notificationText", "classTangle_1_1Examples_1_1Notifications.html#a1696c74521002e3a5ab4d749719d53a6", null ],
    [ "_revealTime", "classTangle_1_1Examples_1_1Notifications.html#a264dbddea67c062d356c23c4044cfd34", null ],
    [ "_successColor", "classTangle_1_1Examples_1_1Notifications.html#a8fa7fc4e034446fe81874c3f06043552", null ]
];