var interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet =
[
    [ "Accounts", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a920921acfb563f41a5fba4eebcf10264", null ],
    [ "Alias", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#ae0e76931714c2cc3e8931c2fe0f0b2ad", null ],
    [ "Balances", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#ad27e9d798eb605fd4baa4ce291d7c3c7", null ],
    [ "ClientPtr", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#afdc2b2adcfa97dc5e2355d6dfa83d0fb", null ],
    [ "CoinType", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#ac4a5719b1d93c8ee251a033fe494862f", null ],
    [ "CurrentAccountIndex", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#aafe1a1c5a6e982a9d28881b2b1063b8b", null ],
    [ "DestinationAddress", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a8c5ed967da2588ea11fa3e07cf9dd8af", null ],
    [ "Index", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a1cc6333a49dba4d2e5a14dd711f2f469", null ],
    [ "NewAccountName", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a64c273bfc80127a86d6778c06d762ecf", null ],
    [ "Password", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a729a27f4ea31aa377f09f441e217a19c", null ],
    [ "SecretManagerPtr", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#aea1d217c3b395a897a36a675925a8966", null ],
    [ "SecureMnemonic", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a2fc064a0d4d150c43d6b374e113f69c0", null ],
    [ "SendAmount", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#ac9f3f4e66151eecf68dfea332c923304", null ],
    [ "StrongholdLocation", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#ae0bc0e331ae8f174f5d408896acc2094", null ],
    [ "WalletLocation", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a5eaf05265631f4b293b2b2a9b37281ef", null ],
    [ "WalletPtr", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a0dd340c75c810969ccdce9001cf8b65d", null ]
];