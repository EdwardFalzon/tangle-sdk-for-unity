var classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload =
[
    [ "aliases", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html#acf231576950f4bfdd37d7b8508b3520f", null ],
    [ "baseCoin", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html#a013fa7646317a0bdf3114ed8af3dbe56", null ],
    [ "foundries", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html#a4ee0aaffef042d5d18d9033e88eca6bd", null ],
    [ "nativeTokens", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html#a6c8b71be802272fec95edd3abc9d8a2c", null ],
    [ "nfts", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html#aac7eee2f8b88a4708027654695f63b78", null ],
    [ "potentiallyLockedOutputs", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html#af5d06aa5bfb5c9c04b8a79ff7670fd17", null ],
    [ "requiredStorageDeposit", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html#a4b1e4cdfcd2d216c1e9614714100a63c", null ]
];