var classNFTCommands =
[
    [ "BurnNft", "classNFTCommands.html#ae624a57ebe220f478cca35906e14c4e7", null ],
    [ "GenerateAndSetImmutableMetadata", "classNFTCommands.html#a48dda0301905c2fbeb9f828c44eb5bf2", null ],
    [ "SendNft", "classNFTCommands.html#a5a7098ea354dc9aeb6676fb4436a02ea", null ],
    [ "attribType1", "classNFTCommands.html#aca9c2ddc596d1df4d2dc8a7bf37dd415", null ],
    [ "attribType2", "classNFTCommands.html#aee6e09209396641b785f9046c7c03159", null ],
    [ "attribVal1", "classNFTCommands.html#ae5fbb434f3cd359b3e9f95f014e12320", null ],
    [ "attribVal2", "classNFTCommands.html#aa57c8d1462aea942aa1bd71244bc46e5", null ],
    [ "collectionName", "classNFTCommands.html#a87211aba135d1dd469a7ed8c81eb8086", null ],
    [ "description", "classNFTCommands.html#a5f2edac215ae8b9643653a5b0afd50d2", null ],
    [ "issuerAddress", "classNFTCommands.html#aa4d9490e807c1cba93f4272e25b3687e", null ],
    [ "issuerName", "classNFTCommands.html#a447bb1c0d88e87a2707b64dab79c76e1", null ],
    [ "ledger", "classNFTCommands.html#a5950bdc524762f0c429d7c6baafbd372", null ],
    [ "nftIdBurn", "classNFTCommands.html#af38fe509d0e584beee0bebc1b0866625", null ],
    [ "nftIdSend", "classNFTCommands.html#a8c52499cdc7de8fb1ce3158978c6e2bf", null ],
    [ "nftName", "classNFTCommands.html#a751f3e62651c216acda2665302769281", null ],
    [ "nftTag", "classNFTCommands.html#a3ffebcc4bc54da75c0990fbf8d0e9d05", null ],
    [ "nftUrl", "classNFTCommands.html#adc3590ab092ae9a0e2ceda9458ee6015", null ],
    [ "sendAddress", "classNFTCommands.html#a194bb04153c3804216652cc6e64c7f84", null ]
];