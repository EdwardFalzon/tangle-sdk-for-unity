var interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken =
[
    [ "burnTokenAmount", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a6c5bc5cdd14195a96202d77a209706ee", null ],
    [ "circulatingSupply", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a03c6579a228254fe72c31f724cdb6206", null ],
    [ "claimOutputIds", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#ac62327485762e786fd96ffd8b364b36b", null ],
    [ "destroyFoundryID", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a1974fa1bde26c30b832abe17dad9a0f2", null ],
    [ "foundryID", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#aa62cc50aafdaa8c4119ff68fa8e0c48e", null ],
    [ "maximumSupply", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a78ff0d3275afe03d90a959980d80f3c5", null ],
    [ "meltTokenAmount", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a66666c2f71f2f396eebe9cdc5e728dc2", null ],
    [ "outputsPayload", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a2ce01537ec95d7a34999653cfc4b6782", null ],
    [ "preparedTransactionPayload", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a6bab156637c7879435a29bccf7a96b4a", null ],
    [ "recipientAddress", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#ab11cccb749e9590433021eabd688ed6f", null ],
    [ "returnStrategy", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a3c70154a2f54557d27e2f2516fc33f13", null ],
    [ "sendTokenAmount", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#acd05abe673f99ced73519380d4c87b08", null ],
    [ "tokenDecimals", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a55f147b63fcb144c893d4d9db132bcde", null ],
    [ "tokenDescription", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#ad3f13ae013fcafed70e961b344ad0ef6", null ],
    [ "tokenID", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#af940b63b347cc313a26ad591db5b62bb", null ],
    [ "tokenImmutableData", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a66c14a286b8af57b52fb2670f492afd0", null ],
    [ "tokenName", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a808547b1f92b94917c2136ea6bc2fb9b", null ],
    [ "tokenSymbol", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a4af7f0444a8342d21db6313a48c70283", null ],
    [ "tokenURL", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a837e0df5449bf2ffb3f76063911eed89", null ]
];