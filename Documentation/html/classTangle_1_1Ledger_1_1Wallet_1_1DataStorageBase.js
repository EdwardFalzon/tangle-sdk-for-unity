var classTangle_1_1Ledger_1_1Wallet_1_1DataStorageBase =
[
    [ "_ledger", "classTangle_1_1Ledger_1_1Wallet_1_1DataStorageBase.html#a82595455d4beeb4c0364d4488a3cf46f", null ],
    [ "_ledgerEvents", "classTangle_1_1Ledger_1_1Wallet_1_1DataStorageBase.html#af525a37610df41b6fda72a9f42a4b0f2", null ],
    [ "Metadata", "classTangle_1_1Ledger_1_1Wallet_1_1DataStorageBase.html#a1cebba4b058e7e764efd479d591cb9ec", null ],
    [ "RecipientAddress", "classTangle_1_1Ledger_1_1Wallet_1_1DataStorageBase.html#a589c0c56ff0ecc0a223883253e3767cd", null ],
    [ "Tag", "classTangle_1_1Ledger_1_1Wallet_1_1DataStorageBase.html#a850786e265871f45dad92c2c77c874df", null ]
];