var annotated_dup =
[
    [ "Tangle", "namespaceTangle.html", [
      [ "Examples", "namespaceTangle_1_1Examples.html", [
        [ "Notifications", "classTangle_1_1Examples_1_1Notifications.html", "classTangle_1_1Examples_1_1Notifications" ]
      ] ],
      [ "Ledger", "namespaceTangle_1_1Ledger.html", [
        [ "IotaSdk010", "namespaceTangle_1_1Ledger_1_1IotaSdk010.html", [
          [ "AccountPayload", "classTangle_1_1Ledger_1_1IotaSdk010_1_1AccountPayload.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1AccountPayload" ],
          [ "AccountResult", "classTangle_1_1Ledger_1_1IotaSdk010_1_1AccountResult.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1AccountResult" ],
          [ "Address", "classTangle_1_1Ledger_1_1IotaSdk010_1_1Address.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1Address" ],
          [ "BalancePayload", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload" ],
          [ "BalanceResult", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalanceResult.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalanceResult" ],
          [ "BaseCoin", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BaseCoin.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BaseCoin" ],
          [ "ErrorPayload", "classTangle_1_1Ledger_1_1IotaSdk010_1_1ErrorPayload.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1ErrorPayload" ],
          [ "ErrorResult", "classTangle_1_1Ledger_1_1IotaSdk010_1_1ErrorResult.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1ErrorResult" ],
          [ "IotaSdk010", "classTangle_1_1Ledger_1_1IotaSdk010_1_1IotaSdk010.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1IotaSdk010" ],
          [ "NativeToken", "classTangle_1_1Ledger_1_1IotaSdk010_1_1NativeToken.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1NativeToken" ],
          [ "RequiredStorageDeposit", "classTangle_1_1Ledger_1_1IotaSdk010_1_1RequiredStorageDeposit.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1RequiredStorageDeposit" ]
        ] ],
        [ "Wallet", "namespaceTangle_1_1Ledger_1_1Wallet.html", [
          [ "DataStorage", "classTangle_1_1Ledger_1_1Wallet_1_1DataStorage.html", "classTangle_1_1Ledger_1_1Wallet_1_1DataStorage" ],
          [ "DataStorageBase", "classTangle_1_1Ledger_1_1Wallet_1_1DataStorageBase.html", "classTangle_1_1Ledger_1_1Wallet_1_1DataStorageBase" ],
          [ "IDataStorage", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IDataStorage.html", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IDataStorage" ],
          [ "INft", "interfaceTangle_1_1Ledger_1_1Wallet_1_1INft.html", "interfaceTangle_1_1Ledger_1_1Wallet_1_1INft" ],
          [ "IToken", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken" ],
          [ "IWallet", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet" ],
          [ "Nft", "classTangle_1_1Ledger_1_1Wallet_1_1Nft.html", "classTangle_1_1Ledger_1_1Wallet_1_1Nft" ],
          [ "NftBase", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase" ],
          [ "NftData", "classTangle_1_1Ledger_1_1Wallet_1_1NftData.html", "classTangle_1_1Ledger_1_1Wallet_1_1NftData" ],
          [ "NftDatum", "classTangle_1_1Ledger_1_1Wallet_1_1NftDatum.html", "classTangle_1_1Ledger_1_1Wallet_1_1NftDatum" ],
          [ "Properties", "classTangle_1_1Ledger_1_1Wallet_1_1Properties.html", "classTangle_1_1Ledger_1_1Wallet_1_1Properties" ],
          [ "Token", "classTangle_1_1Ledger_1_1Wallet_1_1Token.html", "classTangle_1_1Ledger_1_1Wallet_1_1Token" ],
          [ "TokenBase", "classTangle_1_1Ledger_1_1Wallet_1_1TokenBase.html", "classTangle_1_1Ledger_1_1Wallet_1_1TokenBase" ],
          [ "Wallet", "classTangle_1_1Ledger_1_1Wallet_1_1Wallet.html", "classTangle_1_1Ledger_1_1Wallet_1_1Wallet" ],
          [ "WalletBase", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase" ]
        ] ],
        [ "ILedger", "interfaceTangle_1_1Ledger_1_1ILedger.html", "interfaceTangle_1_1Ledger_1_1ILedger" ],
        [ "LedgerBase", "classTangle_1_1Ledger_1_1LedgerBase.html", "classTangle_1_1Ledger_1_1LedgerBase" ],
        [ "LedgerEvents", "classTangle_1_1Ledger_1_1LedgerEvents.html", "classTangle_1_1Ledger_1_1LedgerEvents" ]
      ] ],
      [ "Utilities", "namespaceTangle_1_1Utilities.html", [
        [ "DictionaryEvent", "classTangle_1_1Utilities_1_1DictionaryEvent.html", null ],
        [ "DisplayMnemonic", "classTangle_1_1Utilities_1_1DisplayMnemonic.html", "classTangle_1_1Utilities_1_1DisplayMnemonic" ],
        [ "ImageDisplay", "classTangle_1_1Utilities_1_1ImageDisplay.html", "classTangle_1_1Utilities_1_1ImageDisplay" ],
        [ "IRC30Token", "classTangle_1_1Utilities_1_1IRC30Token.html", "classTangle_1_1Utilities_1_1IRC30Token" ]
      ] ],
      [ "NetworkDetails", "classTangle_1_1NetworkDetails.html", "classTangle_1_1NetworkDetails" ]
    ] ],
    [ "AccountIndex", "classAccountIndex.html", "classAccountIndex" ],
    [ "ButtonsManager", "classButtonsManager.html", "classButtonsManager" ],
    [ "CopyAddress", "classCopyAddress.html", "classCopyAddress" ],
    [ "DataStorage", "classDataStorage.html", "classDataStorage" ],
    [ "DisplayBalanceAndAccounts", "classDisplayBalanceAndAccounts.html", "classDisplayBalanceAndAccounts" ],
    [ "LoadAlias", "classLoadAlias.html", "classLoadAlias" ],
    [ "LoadFoundry", "classLoadFoundry.html", "classLoadFoundry" ],
    [ "LoadNfts", "classLoadNfts.html", "classLoadNfts" ],
    [ "LoadTokens", "classLoadTokens.html", "classLoadTokens" ],
    [ "NFTCommands", "classNFTCommands.html", "classNFTCommands" ],
    [ "PasswordValidator", "classPasswordValidator.html", "classPasswordValidator" ],
    [ "PlayerPrefsManager", "classPlayerPrefsManager.html", "classPlayerPrefsManager" ],
    [ "RuntimeFileBrowser", "classRuntimeFileBrowser.html", "classRuntimeFileBrowser" ],
    [ "SubmitTransaction", "classSubmitTransaction.html", "classSubmitTransaction" ],
    [ "TokenCommands", "classTokenCommands.html", "classTokenCommands" ]
];