var classTangle_1_1Utilities_1_1IRC30Token =
[
    [ "SetDecimals", "classTangle_1_1Utilities_1_1IRC30Token.html#a843911018df12df3f8eeaa1836bb8d21", null ],
    [ "SetDescription", "classTangle_1_1Utilities_1_1IRC30Token.html#a1e5f5054e972f89a0c074948b0244d97", null ],
    [ "SetLogo", "classTangle_1_1Utilities_1_1IRC30Token.html#a30070a28ee48cd5bcac2d05948dd7fd3", null ],
    [ "SetLogoUrl", "classTangle_1_1Utilities_1_1IRC30Token.html#a9113cdf1bfe20e615bdefd84f7458716", null ],
    [ "SetName", "classTangle_1_1Utilities_1_1IRC30Token.html#ab94f9929c44c7b4fae45122397c2923f", null ],
    [ "SetSymbol", "classTangle_1_1Utilities_1_1IRC30Token.html#a25c66bb71b2a935296ce83dedac49b3a", null ],
    [ "SetUrl", "classTangle_1_1Utilities_1_1IRC30Token.html#ad9f8711d6f18b7663e5b364390fe04f9", null ],
    [ "ToJson", "classTangle_1_1Utilities_1_1IRC30Token.html#abce0c235c412135965aa7338f0ebd08f", null ],
    [ "decimals", "classTangle_1_1Utilities_1_1IRC30Token.html#a436843b1cd5a7a6faa6e204786a75ed0", null ],
    [ "description", "classTangle_1_1Utilities_1_1IRC30Token.html#ab92ab20d12b34ed21c358efa9a9ccd29", null ],
    [ "logo", "classTangle_1_1Utilities_1_1IRC30Token.html#adcd80317293e688dab9246896d03c2e7", null ],
    [ "logoUrl", "classTangle_1_1Utilities_1_1IRC30Token.html#aba13f51aec1e466fa20463fdd8d45539", null ],
    [ "name", "classTangle_1_1Utilities_1_1IRC30Token.html#ad6c13697604891dac20202d98e18683a", null ],
    [ "standard", "classTangle_1_1Utilities_1_1IRC30Token.html#acaa793bbde3b14a902e809ee44e8c477", null ],
    [ "symbol", "classTangle_1_1Utilities_1_1IRC30Token.html#ae95539baedf2d517a44e7598964c42fa", null ],
    [ "url", "classTangle_1_1Utilities_1_1IRC30Token.html#ab51cc199e2431f3a5421278325589d64", null ]
];