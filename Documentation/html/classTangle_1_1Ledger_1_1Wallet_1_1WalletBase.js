var classTangle_1_1Ledger_1_1Wallet_1_1WalletBase =
[
    [ "_ledger", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#ac3afd6d36e10e5148f573c367df39895", null ],
    [ "_ledgerEvents", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#ad4595b998a4b03335617fff1d90a8843", null ],
    [ "Accounts", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a0b6d6045fe9381ef663eb6d69d2c337e", null ],
    [ "Alias", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a7a4f19545979c85a70f733fcb2bc84f3", null ],
    [ "Balances", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a26f44604683b00850ec4274dcf017b27", null ],
    [ "ClientPtr", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a7da3b831b2221adee4bd2d329362f5f9", null ],
    [ "CoinBalances", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a43f5b2878aafa7fbbbe00676929e9c24", null ],
    [ "CoinType", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a17224ea20c804559e25e7bbec5ef9a20", null ],
    [ "CurrentAccountIndex", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a6c66c9289a7aa70bbf2b5c50c39973eb", null ],
    [ "DestinationAddress", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a93892187b1d06435d8b005384c7001a8", null ],
    [ "Index", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#aac60c50f78754e26ccc02a69717eecdc", null ],
    [ "NewAccountName", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a185944272962c507e62616799fd89959", null ],
    [ "Password", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a8e159ccf1ed03a478c2d694d9ef7b41d", null ],
    [ "SecretManagerPtr", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a773636e229a46de4fb7eae5492f0d070", null ],
    [ "SecureMnemonic", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a8b9d881a3987aec15b82e06fdfc1c130", null ],
    [ "SendAmount", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a96b0c71905eeae4919990ee3e7db258f", null ],
    [ "StrongholdLocation", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a278d154217139e064eae20e01c3325be", null ],
    [ "WalletLocation", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a8c81eb4ca04e878c8634e225bc5bf8e7", null ],
    [ "WalletPtr", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#af9e2c6dca55290bacb93cccec8627e79", null ]
];