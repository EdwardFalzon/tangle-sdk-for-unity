var interfaceTangle_1_1Ledger_1_1Wallet_1_1INft =
[
    [ "burnNftIds", "interfaceTangle_1_1Ledger_1_1Wallet_1_1INft.html#a9fa7b97681372162940d43350e72bad9", null ],
    [ "nftAliases", "interfaceTangle_1_1Ledger_1_1Wallet_1_1INft.html#afaff3ae7180a6e27419bb5f62f167f13", null ],
    [ "nftImmutableMetadata", "interfaceTangle_1_1Ledger_1_1Wallet_1_1INft.html#aa1b3609ac18cb7c82786e040f4da6264", null ],
    [ "nftIssuerAddress", "interfaceTangle_1_1Ledger_1_1Wallet_1_1INft.html#a230856304a6793368ce69b90fd812d67", null ],
    [ "nftTag", "interfaceTangle_1_1Ledger_1_1Wallet_1_1INft.html#a13936b48aac0832361b36759a4994ddd", null ],
    [ "sendNftAddress", "interfaceTangle_1_1Ledger_1_1Wallet_1_1INft.html#a32b927a0ff032dad9ce71ada792569f3", null ],
    [ "sendNftId", "interfaceTangle_1_1Ledger_1_1Wallet_1_1INft.html#a647c092990c062ff9cbe639323d39274", null ]
];