/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Tangle SDK for Unity", "index.html", [
    [ "Tangle SDK for Unity", "md_README.html", [
      [ "Description", "md_README.html#autotoc_md1", null ],
      [ "Supporting Technology", "md_README.html#autotoc_md2", null ],
      [ "Compatibility", "md_README.html#autotoc_md3", null ],
      [ "Installation", "md_README.html#autotoc_md4", null ],
      [ "Usage", "md_README.html#autotoc_md5", [
        [ "Wallet", "md_README.html#autotoc_md6", null ],
        [ "Native Tokens", "md_README.html#autotoc_md7", null ],
        [ "NFTs", "md_README.html#autotoc_md8", null ],
        [ "Data Storage", "md_README.html#autotoc_md9", null ]
      ] ],
      [ "Usage", "md_README.html#autotoc_md10", null ],
      [ "Acknowledgment", "md_README.html#autotoc_md11", null ],
      [ "License", "md_README.html#autotoc_md12", null ],
      [ "Project status", "md_README.html#autotoc_md13", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Package List", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", null ],
        [ "Properties", "functions_prop.html", null ],
        [ "Events", "functions_evnt.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Typedefs", "globals_type.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"AccountResult_8cs.html",
"classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html#acf231576950f4bfdd37d7b8508b3520f",
"classTangle_1_1Ledger_1_1LedgerBase.html#a7d0d9377d05a1f48e33cea4100734755",
"classTangle_1_1Ledger_1_1LedgerEvents.html#a556f0c9ca833964bff0fc926e9b3bf33",
"classTangle_1_1Ledger_1_1LedgerEvents.html#ae76cbb14308e893f832352b06a64e4d9",
"classTangle_1_1Utilities_1_1IRC30Token.html#ab51cc199e2431f3a5421278325589d64",
"interfaceTangle_1_1Ledger_1_1ILedger.html#abb407bdb9851a9381ddc3f0e72a21bee"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';