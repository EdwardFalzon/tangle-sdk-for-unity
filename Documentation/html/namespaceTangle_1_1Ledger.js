var namespaceTangle_1_1Ledger =
[
    [ "IotaSdk010", "namespaceTangle_1_1Ledger_1_1IotaSdk010.html", "namespaceTangle_1_1Ledger_1_1IotaSdk010" ],
    [ "Wallet", "namespaceTangle_1_1Ledger_1_1Wallet.html", "namespaceTangle_1_1Ledger_1_1Wallet" ],
    [ "ILedger", "interfaceTangle_1_1Ledger_1_1ILedger.html", "interfaceTangle_1_1Ledger_1_1ILedger" ],
    [ "LedgerBase", "classTangle_1_1Ledger_1_1LedgerBase.html", "classTangle_1_1Ledger_1_1LedgerBase" ],
    [ "LedgerEvents", "classTangle_1_1Ledger_1_1LedgerEvents.html", "classTangle_1_1Ledger_1_1LedgerEvents" ]
];