var BalanceResult_8cs =
[
    [ "Tangle.Ledger.IotaSdk010.BalanceResult", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalanceResult.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalanceResult" ],
    [ "Tangle.Ledger.IotaSdk010.BalancePayload", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload" ],
    [ "Tangle.Ledger.IotaSdk010.BaseCoin", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BaseCoin.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BaseCoin" ],
    [ "Tangle.Ledger.IotaSdk010.RequiredStorageDeposit", "classTangle_1_1Ledger_1_1IotaSdk010_1_1RequiredStorageDeposit.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1RequiredStorageDeposit" ],
    [ "Tangle.Ledger.IotaSdk010.NativeToken", "classTangle_1_1Ledger_1_1IotaSdk010_1_1NativeToken.html", "classTangle_1_1Ledger_1_1IotaSdk010_1_1NativeToken" ]
];