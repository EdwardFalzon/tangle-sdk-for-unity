var searchData=
[
  ['maximumsupply_0',['maximumsupply',['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a78ff0d3275afe03d90a959980d80f3c5',1,'Tangle.Ledger.Wallet.IToken.maximumSupply'],['../classTangle_1_1Ledger_1_1Wallet_1_1TokenBase.html#a66be608ac78035e074dbfca54c0a7299',1,'Tangle.Ledger.Wallet.TokenBase.maximumSupply']]],
  ['melttokenamount_1',['melttokenamount',['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a66666c2f71f2f396eebe9cdc5e728dc2',1,'Tangle.Ledger.Wallet.IToken.meltTokenAmount'],['../classTangle_1_1Ledger_1_1Wallet_1_1TokenBase.html#a16a2e090b7368bb9d997448a0fb97475',1,'Tangle.Ledger.Wallet.TokenBase.meltTokenAmount']]],
  ['metadata_2',['metadata',['../classTangle_1_1Ledger_1_1IotaSdk010_1_1NativeToken.html#af9dd188398cfc30f297c7836bee6e678',1,'Tangle.Ledger.IotaSdk010.NativeToken.metadata'],['../classTangle_1_1Ledger_1_1Wallet_1_1DataStorageBase.html#a1cebba4b058e7e764efd479d591cb9ec',1,'Tangle.Ledger.Wallet.DataStorageBase.Metadata'],['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IDataStorage.html#aef91085475d54c15ac3138e56d763b91',1,'Tangle.Ledger.Wallet.IDataStorage.Metadata']]]
];
