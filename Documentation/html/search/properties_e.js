var searchData=
[
  ['recipientaddress_0',['recipientaddress',['../classTangle_1_1Ledger_1_1Wallet_1_1DataStorageBase.html#a589c0c56ff0ecc0a223883253e3767cd',1,'Tangle.Ledger.Wallet.DataStorageBase.RecipientAddress'],['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IDataStorage.html#aa28b21f5562dcd7d26016ffdab5066de',1,'Tangle.Ledger.Wallet.IDataStorage.RecipientAddress'],['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#ab11cccb749e9590433021eabd688ed6f',1,'Tangle.Ledger.Wallet.IToken.recipientAddress'],['../classTangle_1_1Ledger_1_1Wallet_1_1TokenBase.html#ae8ca55893791cc6a5bedebe0af028be6',1,'Tangle.Ledger.Wallet.TokenBase.recipientAddress']]],
  ['requiredstoragedeposit_1',['requiredStorageDeposit',['../classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html#a4b1e4cdfcd2d216c1e9614714100a63c',1,'Tangle::Ledger::IotaSdk010::BalancePayload']]],
  ['returnstrategy_2',['returnstrategy',['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a3c70154a2f54557d27e2f2516fc33f13',1,'Tangle.Ledger.Wallet.IToken.returnStrategy'],['../classTangle_1_1Ledger_1_1Wallet_1_1TokenBase.html#a4319d1a455d96c5b1c7bd98cdeecf7bd',1,'Tangle.Ledger.Wallet.TokenBase.returnStrategy']]]
];
