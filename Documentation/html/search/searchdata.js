var indexSectionsWithContent =
{
  0: "_abcdefghijklmnoprstuw",
  1: "abcdeilnprstw",
  2: "t",
  3: "abcdeijlnprstw",
  4: "abcdghlmnoprstu",
  5: "_abcdfilmnprstuw",
  6: "cds",
  7: "_abcdefiklmnoprstuw",
  8: "bcdgmprst",
  9: "fstu"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "properties",
  8: "events",
  9: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Properties",
  8: "Events",
  9: "Pages"
};

