var searchData=
[
  ['datastorage_0',['datastorage',['../interfaceTangle_1_1Ledger_1_1ILedger.html#a07a3b810d565caf46f7564e2df90d450',1,'Tangle.Ledger.ILedger.DataStorage'],['../classTangle_1_1Ledger_1_1LedgerBase.html#a6fb9b3b701ed9812bd333a0b330663e9',1,'Tangle.Ledger.LedgerBase.DataStorage']]],
  ['destinationaddress_1',['destinationaddress',['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a8c5ed967da2588ea11fa3e07cf9dd8af',1,'Tangle.Ledger.Wallet.IWallet.DestinationAddress'],['../classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a93892187b1d06435d8b005384c7001a8',1,'Tangle.Ledger.Wallet.WalletBase.DestinationAddress']]],
  ['destroyfoundryid_2',['destroyfoundryid',['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a1974fa1bde26c30b832abe17dad9a0f2',1,'Tangle.Ledger.Wallet.IToken.destroyFoundryID'],['../classTangle_1_1Ledger_1_1Wallet_1_1TokenBase.html#a7c7208193aa4daf1512098b1b9ba0a03',1,'Tangle.Ledger.Wallet.TokenBase.destroyFoundryID']]]
];
