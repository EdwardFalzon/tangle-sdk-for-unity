var searchData=
[
  ['wallet_0',['wallet',['../classTangle_1_1Ledger_1_1Wallet_1_1Wallet.html',1,'Tangle.Ledger.Wallet.Wallet'],['../interfaceTangle_1_1Ledger_1_1ILedger.html#a80e2cd295d98222e453e388a12e5590c',1,'Tangle.Ledger.ILedger.Wallet'],['../classTangle_1_1Ledger_1_1LedgerBase.html#adfde1b93756042a4f670a6f6008c3e7e',1,'Tangle.Ledger.LedgerBase.Wallet'],['../md_README.html#autotoc_md6',1,'Wallet']]],
  ['wallet_2ecs_1',['Wallet.cs',['../Wallet_8cs.html',1,'']]],
  ['walletbase_2',['WalletBase',['../classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html',1,'Tangle::Ledger::Wallet']]],
  ['walletbase_2ecs_3',['WalletBase.cs',['../WalletBase_8cs.html',1,'']]],
  ['walletlocation_4',['walletlocation',['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a5eaf05265631f4b293b2b2a9b37281ef',1,'Tangle.Ledger.Wallet.IWallet.WalletLocation'],['../classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a8c81eb4ca04e878c8634e225bc5bf8e7',1,'Tangle.Ledger.Wallet.WalletBase.WalletLocation']]],
  ['walletpanel_5',['WalletPanel',['../classButtonsManager.html#ad24e54aa4d529adb1427d8e6b255030c',1,'ButtonsManager']]],
  ['walletptr_6',['walletptr',['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a0dd340c75c810969ccdce9001cf8b65d',1,'Tangle.Ledger.Wallet.IWallet.WalletPtr'],['../classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#af9e2c6dca55290bacb93cccec8627e79',1,'Tangle.Ledger.Wallet.WalletBase.WalletPtr']]]
];
