var searchData=
[
  ['ledger_0',['ledger',['../classButtonsManager.html#a5c20632681029eec4058acd7108630eb',1,'ButtonsManager.ledger'],['../classDataStorage.html#a74c338df63188d2a773d6c068066949d',1,'DataStorage.ledger'],['../classNFTCommands.html#a5950bdc524762f0c429d7c6baafbd372',1,'NFTCommands.ledger'],['../classSubmitTransaction.html#a2e2ea282976de53de34bad395b0fcbb0',1,'SubmitTransaction.ledger'],['../classTokenCommands.html#aaecac66346a80ff2eb41e18749524770',1,'TokenCommands.ledger']]],
  ['loginscreen_1',['LoginScreen',['../classButtonsManager.html#a2ac43721415cedde9db7be352e623823',1,'ButtonsManager']]],
  ['logo_2',['logo',['../classTangle_1_1Utilities_1_1IRC30Token.html#adcd80317293e688dab9246896d03c2e7',1,'Tangle::Utilities::IRC30Token']]],
  ['logourl_3',['logoUrl',['../classTangle_1_1Utilities_1_1IRC30Token.html#aba13f51aec1e466fa20463fdd8d45539',1,'Tangle::Utilities::IRC30Token']]]
];
