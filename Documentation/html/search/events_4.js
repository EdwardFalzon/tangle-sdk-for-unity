var searchData=
[
  ['meltnativetokenfailure_0',['meltnativetokenfailure',['../interfaceTangle_1_1Ledger_1_1ILedger.html#a80a36669f09d5701e10a3cb53d2ba8e4',1,'Tangle.Ledger.ILedger.MeltNativeTokenFailure'],['../classTangle_1_1Ledger_1_1LedgerBase.html#a17c6d6eb3b2324cab61aebda5ae0cb23',1,'Tangle.Ledger.LedgerBase.MeltNativeTokenFailure']]],
  ['meltnativetokensuccess_1',['meltnativetokensuccess',['../interfaceTangle_1_1Ledger_1_1ILedger.html#a3f0def0983145dbd1d66166c139f590c',1,'Tangle.Ledger.ILedger.MeltNativeTokenSuccess'],['../classTangle_1_1Ledger_1_1LedgerBase.html#a2d24535e1bd3e57f5a1368c2de5a4689',1,'Tangle.Ledger.LedgerBase.MeltNativeTokenSuccess']]],
  ['mintnftfailure_2',['mintnftfailure',['../interfaceTangle_1_1Ledger_1_1ILedger.html#af823dc4215ffd1227a81d3f599a3f20c',1,'Tangle.Ledger.ILedger.MintNFTFailure'],['../classTangle_1_1Ledger_1_1LedgerBase.html#a1dc9d314bf648f951028c4d95e2eb028',1,'Tangle.Ledger.LedgerBase.MintNFTFailure']]],
  ['mintnftsuccess_3',['mintnftsuccess',['../interfaceTangle_1_1Ledger_1_1ILedger.html#a8ca9994f1b860d058a992fafe1ed2451',1,'Tangle.Ledger.ILedger.MintNFTSuccess'],['../classTangle_1_1Ledger_1_1LedgerBase.html#a98bc75c3f28129d9756a89440c874a9c',1,'Tangle.Ledger.LedgerBase.MintNFTSuccess']]]
];
