var searchData=
[
  ['wallet_0',['wallet',['../interfaceTangle_1_1Ledger_1_1ILedger.html#a80e2cd295d98222e453e388a12e5590c',1,'Tangle.Ledger.ILedger.Wallet'],['../classTangle_1_1Ledger_1_1LedgerBase.html#adfde1b93756042a4f670a6f6008c3e7e',1,'Tangle.Ledger.LedgerBase.Wallet']]],
  ['walletlocation_1',['walletlocation',['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a5eaf05265631f4b293b2b2a9b37281ef',1,'Tangle.Ledger.Wallet.IWallet.WalletLocation'],['../classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a8c81eb4ca04e878c8634e225bc5bf8e7',1,'Tangle.Ledger.Wallet.WalletBase.WalletLocation']]],
  ['walletptr_2',['walletptr',['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#a0dd340c75c810969ccdce9001cf8b65d',1,'Tangle.Ledger.Wallet.IWallet.WalletPtr'],['../classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#af9e2c6dca55290bacb93cccec8627e79',1,'Tangle.Ledger.Wallet.WalletBase.WalletPtr']]]
];
