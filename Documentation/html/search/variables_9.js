var searchData=
[
  ['name_0',['name',['../classTangle_1_1Ledger_1_1Wallet_1_1Properties.html#a25b7f3a6cb17d9766af596e0d939c251',1,'Tangle.Ledger.Wallet.Properties.name'],['../classTangle_1_1Utilities_1_1IRC30Token.html#ad6c13697604891dac20202d98e18683a',1,'Tangle.Utilities.IRC30Token.name']]],
  ['nftdropdown_1',['nftDropdown',['../classLoadNfts.html#a96447f790dd23a8d3f884d7d61045a59',1,'LoadNfts']]],
  ['nftidburn_2',['nftIdBurn',['../classNFTCommands.html#af38fe509d0e584beee0bebc1b0866625',1,'NFTCommands']]],
  ['nftidsend_3',['nftIdSend',['../classNFTCommands.html#a8c52499cdc7de8fb1ce3158978c6e2bf',1,'NFTCommands']]],
  ['nftname_4',['nftName',['../classNFTCommands.html#a751f3e62651c216acda2665302769281',1,'NFTCommands']]],
  ['nftpanel_5',['NFTPanel',['../classButtonsManager.html#a0cf316e48ee691f1e0c11cf2ada4de05',1,'ButtonsManager']]],
  ['nfttag_6',['nftTag',['../classNFTCommands.html#a3ffebcc4bc54da75c0990fbf8d0e9d05',1,'NFTCommands']]],
  ['nfturl_7',['nftUrl',['../classNFTCommands.html#adc3590ab092ae9a0e2ceda9458ee6015',1,'NFTCommands']]],
  ['node_8',['node',['../classTangle_1_1NetworkDetails.html#a8ffcb5aade96025f0f84777d7a628adc',1,'Tangle::NetworkDetails']]]
];
