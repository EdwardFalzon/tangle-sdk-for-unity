var searchData=
[
  ['balances_0',['balances',['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html#ad27e9d798eb605fd4baa4ce291d7c3c7',1,'Tangle.Ledger.Wallet.IWallet.Balances'],['../classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html#a26f44604683b00850ec4274dcf017b27',1,'Tangle.Ledger.Wallet.WalletBase.Balances']]],
  ['basecoin_1',['baseCoin',['../classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html#a013fa7646317a0bdf3114ed8af3dbe56',1,'Tangle::Ledger::IotaSdk010::BalancePayload']]],
  ['basic_2',['basic',['../classTangle_1_1Ledger_1_1IotaSdk010_1_1RequiredStorageDeposit.html#a843cab1a7d178d79bf87d63475e74a28',1,'Tangle::Ledger::IotaSdk010::RequiredStorageDeposit']]],
  ['burnnftids_3',['burnnftids',['../interfaceTangle_1_1Ledger_1_1Wallet_1_1INft.html#a9fa7b97681372162940d43350e72bad9',1,'Tangle.Ledger.Wallet.INft.burnNftIds'],['../classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html#a85c72d916e4ee03f7d5417767cb72d06',1,'Tangle.Ledger.Wallet.NftBase.burnNftIds']]],
  ['burntokenamount_4',['burntokenamount',['../interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html#a6c5bc5cdd14195a96202d77a209706ee',1,'Tangle.Ledger.Wallet.IToken.burnTokenAmount'],['../classTangle_1_1Ledger_1_1Wallet_1_1TokenBase.html#abcd8f2c8af974a2ab3637ac561244e9f',1,'Tangle.Ledger.Wallet.TokenBase.burnTokenAmount']]]
];
