var searchData=
[
  ['destroyaliasfailure_0',['destroyaliasfailure',['../interfaceTangle_1_1Ledger_1_1ILedger.html#aa4b72ba005a7a56101717addd4b6a95d',1,'Tangle.Ledger.ILedger.DestroyAliasFailure'],['../classTangle_1_1Ledger_1_1LedgerBase.html#af8c7e7458db4bb61f5273887b152565b',1,'Tangle.Ledger.LedgerBase.DestroyAliasFailure']]],
  ['destroyaliassuccess_1',['destroyaliassuccess',['../interfaceTangle_1_1Ledger_1_1ILedger.html#a0ca450d0082de81844e8d428ef1d0beb',1,'Tangle.Ledger.ILedger.DestroyAliasSuccess'],['../classTangle_1_1Ledger_1_1LedgerBase.html#a0338e1149ad14ca6592da50425f578d8',1,'Tangle.Ledger.LedgerBase.DestroyAliasSuccess']]],
  ['destroyfoundryfailure_2',['destroyfoundryfailure',['../interfaceTangle_1_1Ledger_1_1ILedger.html#a69efa1027939ffe04f7da6f691e5758f',1,'Tangle.Ledger.ILedger.DestroyFoundryFailure'],['../classTangle_1_1Ledger_1_1LedgerBase.html#a31046d6e984e6457f4072ca08d18faf2',1,'Tangle.Ledger.LedgerBase.DestroyFoundryFailure']]],
  ['destroyfoundrysuccess_3',['destroyfoundrysuccess',['../interfaceTangle_1_1Ledger_1_1ILedger.html#a3e8211dfca088ac0a9d439ad2398e2e4',1,'Tangle.Ledger.ILedger.DestroyFoundrySuccess'],['../classTangle_1_1Ledger_1_1LedgerBase.html#a0b0a8908e6e7f4c24276d1d90ee1a711',1,'Tangle.Ledger.LedgerBase.DestroyFoundrySuccess']]]
];
