var hierarchy =
[
    [ "AccountIndex", "classAccountIndex.html", null ],
    [ "Tangle.Ledger.IotaSdk010.AccountPayload", "classTangle_1_1Ledger_1_1IotaSdk010_1_1AccountPayload.html", null ],
    [ "Tangle.Ledger.IotaSdk010.AccountResult", "classTangle_1_1Ledger_1_1IotaSdk010_1_1AccountResult.html", null ],
    [ "Tangle.Ledger.IotaSdk010.Address", "classTangle_1_1Ledger_1_1IotaSdk010_1_1Address.html", null ],
    [ "Tangle.Ledger.IotaSdk010.BalancePayload", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalancePayload.html", null ],
    [ "Tangle.Ledger.IotaSdk010.BalanceResult", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BalanceResult.html", null ],
    [ "Tangle.Ledger.IotaSdk010.BaseCoin", "classTangle_1_1Ledger_1_1IotaSdk010_1_1BaseCoin.html", null ],
    [ "Tangle.Ledger.IotaSdk010.ErrorPayload", "classTangle_1_1Ledger_1_1IotaSdk010_1_1ErrorPayload.html", null ],
    [ "Tangle.Ledger.IotaSdk010.ErrorResult", "classTangle_1_1Ledger_1_1IotaSdk010_1_1ErrorResult.html", null ],
    [ "Tangle.Ledger.Wallet.IDataStorage", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IDataStorage.html", [
      [ "Tangle.Ledger.Wallet.DataStorageBase", "classTangle_1_1Ledger_1_1Wallet_1_1DataStorageBase.html", [
        [ "Tangle.Ledger.Wallet.DataStorage", "classTangle_1_1Ledger_1_1Wallet_1_1DataStorage.html", null ]
      ] ]
    ] ],
    [ "Tangle.Ledger.ILedger", "interfaceTangle_1_1Ledger_1_1ILedger.html", [
      [ "Tangle.Ledger.LedgerBase", "classTangle_1_1Ledger_1_1LedgerBase.html", [
        [ "Tangle.Ledger.IotaSdk010.IotaSdk010", "classTangle_1_1Ledger_1_1IotaSdk010_1_1IotaSdk010.html", null ],
        [ "Tangle.Ledger.IotaSdk010.IotaSdk010", "classTangle_1_1Ledger_1_1IotaSdk010_1_1IotaSdk010.html", null ]
      ] ]
    ] ],
    [ "Tangle.Ledger.Wallet.INft", "interfaceTangle_1_1Ledger_1_1Wallet_1_1INft.html", [
      [ "Tangle.Ledger.Wallet.NftBase", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html", [
        [ "Tangle.Ledger.Wallet.Nft", "classTangle_1_1Ledger_1_1Wallet_1_1Nft.html", null ]
      ] ]
    ] ],
    [ "Tangle.Utilities.IRC30Token", "classTangle_1_1Utilities_1_1IRC30Token.html", null ],
    [ "Tangle.Ledger.Wallet.IToken", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IToken.html", [
      [ "Tangle.Ledger.Wallet.TokenBase", "classTangle_1_1Ledger_1_1Wallet_1_1TokenBase.html", [
        [ "Tangle.Ledger.Wallet.Token", "classTangle_1_1Ledger_1_1Wallet_1_1Token.html", null ]
      ] ]
    ] ],
    [ "Tangle.Ledger.Wallet.IWallet", "interfaceTangle_1_1Ledger_1_1Wallet_1_1IWallet.html", [
      [ "Tangle.Ledger.Wallet.WalletBase", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html", [
        [ "Tangle.Ledger.Wallet.Wallet", "classTangle_1_1Ledger_1_1Wallet_1_1Wallet.html", null ]
      ] ]
    ] ],
    [ "MonoBehaviour", null, [
      [ "ButtonsManager", "classButtonsManager.html", null ],
      [ "CopyAddress", "classCopyAddress.html", null ],
      [ "DataStorage", "classDataStorage.html", null ],
      [ "DisplayBalanceAndAccounts", "classDisplayBalanceAndAccounts.html", null ],
      [ "LoadAlias", "classLoadAlias.html", null ],
      [ "LoadFoundry", "classLoadFoundry.html", null ],
      [ "LoadNfts", "classLoadNfts.html", null ],
      [ "LoadTokens", "classLoadTokens.html", null ],
      [ "NFTCommands", "classNFTCommands.html", null ],
      [ "PasswordValidator", "classPasswordValidator.html", null ],
      [ "PlayerPrefsManager", "classPlayerPrefsManager.html", null ],
      [ "RuntimeFileBrowser", "classRuntimeFileBrowser.html", null ],
      [ "SubmitTransaction", "classSubmitTransaction.html", null ],
      [ "Tangle.Examples.Notifications", "classTangle_1_1Examples_1_1Notifications.html", null ],
      [ "Tangle.Ledger.LedgerBase", "classTangle_1_1Ledger_1_1LedgerBase.html", null ],
      [ "Tangle.Ledger.LedgerEvents", "classTangle_1_1Ledger_1_1LedgerEvents.html", null ],
      [ "Tangle.Ledger.Wallet.DataStorageBase", "classTangle_1_1Ledger_1_1Wallet_1_1DataStorageBase.html", null ],
      [ "Tangle.Ledger.Wallet.NftBase", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html", null ],
      [ "Tangle.Ledger.Wallet.TokenBase", "classTangle_1_1Ledger_1_1Wallet_1_1TokenBase.html", null ],
      [ "Tangle.Ledger.Wallet.WalletBase", "classTangle_1_1Ledger_1_1Wallet_1_1WalletBase.html", null ],
      [ "Tangle.Utilities.DisplayMnemonic", "classTangle_1_1Utilities_1_1DisplayMnemonic.html", null ],
      [ "Tangle.Utilities.ImageDisplay", "classTangle_1_1Utilities_1_1ImageDisplay.html", null ],
      [ "TokenCommands", "classTokenCommands.html", null ]
    ] ],
    [ "Tangle.Ledger.IotaSdk010.NativeToken", "classTangle_1_1Ledger_1_1IotaSdk010_1_1NativeToken.html", null ],
    [ "Tangle.Ledger.Wallet.NftData", "classTangle_1_1Ledger_1_1Wallet_1_1NftData.html", null ],
    [ "Tangle.Ledger.Wallet.NftDatum", "classTangle_1_1Ledger_1_1Wallet_1_1NftDatum.html", null ],
    [ "Tangle.Ledger.Wallet.Properties", "classTangle_1_1Ledger_1_1Wallet_1_1Properties.html", null ],
    [ "Tangle.Ledger.IotaSdk010.RequiredStorageDeposit", "classTangle_1_1Ledger_1_1IotaSdk010_1_1RequiredStorageDeposit.html", null ],
    [ "ScriptableObject", null, [
      [ "Tangle.NetworkDetails", "classTangle_1_1NetworkDetails.html", null ]
    ] ],
    [ "UnityEvent", null, [
      [ "Tangle.Utilities.DictionaryEvent", "classTangle_1_1Utilities_1_1DictionaryEvent.html", null ]
    ] ]
];