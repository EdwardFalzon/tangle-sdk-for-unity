var classTangle_1_1Ledger_1_1Wallet_1_1NftBase =
[
    [ "_ledger", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html#a2db7adad0ad921823ad65fe695b323db", null ],
    [ "_ledgerEvents", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html#a828f8718174b8a673653291c5c435a93", null ],
    [ "burnNftIds", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html#a85c72d916e4ee03f7d5417767cb72d06", null ],
    [ "nftAliases", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html#a865eb904b6d2e5ece965ddd0cced869d", null ],
    [ "nftImmutableMetadata", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html#a7f98aedf33dcbc91505ea66d7b407815", null ],
    [ "nftIssuerAddress", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html#aadf68ed689fac7286998340bb5ff9085", null ],
    [ "nftTag", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html#a454c8df974685102142ea22a67075448", null ],
    [ "sendNftAddress", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html#a8918d68ec829da2ddd0b0eab098db7ad", null ],
    [ "sendNftId", "classTangle_1_1Ledger_1_1Wallet_1_1NftBase.html#a5ec4675907c887f04d8dd7ba1180cbdf", null ]
];