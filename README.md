# Tangle SDK for Unity
v1.00.010.1

## Description
Tangle SDK for Unity is a toolkit to allow Unity developers to connect their games and projects to IOTA Foundation's Tangle crypto network.

From within Unity, you can create and connect to wallets; work with coins and NFTs; send and retrieve data from the Tangle. Technical knowledge of the Tangle's DAG or crypto-centric technology is not necessary.

## Supporting Technology
This project implements and includes the compiled .dll, .dylib and .so libraries from [IOTA SDK Native Bindings](https://github.com/iotaledger/iota-sdk-native-bindings).

Read the complete project description [here](https://drive.google.com/file/d/1_0xIo8svEYEycYXcuceu45HgCQXYU0v9/view?usp=sharing).

## Compatibility
Tangle SDK is developed for Unity. We have used Unity 2021.3, but is expected to work fine on any version from 2020 and up.

It is written entirely in C#, so it may be reasonably straightforward to port it to Stride3D, though we've not tried.

## Installation
Simply download this repository from GitLab and copy its root directory "Tangle" into your Unity project's "Assets" directory.

We're also working with Unity Technologies to publishing the SDK as a Decentralised asset on the Unity Asset Store.

## Usage
All wallet functions can be performed in Unity with this SDK:

### Wallet

  * Create Account
  * Generate Address
  * Get Funds from Faucet
  * Get Balance
  * Transaction

### Native Tokens

  * Mint
  * Send
  * Melt
  * Burn
  * Destroy Foundry

### NFTs
  * Mint NFTs
  * Send NFTs
  * Burn NFTs
  * Destroy Alias

### Data Storage
  * Store and notarize data on the Tangle

## Usage
The project has several technical example scenes that take you step-by-step through the processes behind each transaction. The sample scenes show JSON outputs for each step, to demonstrate the information that devs can use in their projects.

![Technical display of wallet transactions](https://gitlab.com/EdwardFalzon/tangle-sdk-for-unity/-/raw/main/Images/TechWallet.png)

Such technical back-and-forth is undesirable for end-users, however, so also included is a full-featured Complete Wallet scene that has a more user-centric design. Loosely based on Tangle's Bloom wallet, it hides the JSON and multi-step processes, simplifying the interface and creating a more-streamlined experience for users.

![Sample user-friendly wallet](https://gitlab.com/EdwardFalzon/tangle-sdk-for-unity/-/raw/main/Images/TangleWallet.png)

## Documentation
You'll find extensive code-documentation in the Documentation directory. Open the [index.html](https://gitlab.com/EdwardFalzon/tangle-sdk-for-unity/-/raw/main/Documentation/html/index.html) file in the html directory.

## Acknowledgment
This project is funded by The Tangle Community Treasury, project ID TCT-20. Read the complete project description [here](https://drive.google.com/file/d/1_0xIo8svEYEycYXcuceu45HgCQXYU0v9/view?usp=sharing).

Also used are a number of third-party assets, distributed under various licenses. See them in the "3rd Party" directory.

## License
This project is developed under the MIT License.

## Project status
The Tangle SDK for Unity is under active development by Edward Falzon and the team at [Pluck Games](https://pluck.games).
