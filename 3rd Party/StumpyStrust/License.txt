Space Backdrop
https://opengameart.org/content/space-backdrop

By beren77
https://opengameart.org/users/beren77

Redistributed with Tangle SDK for Unity under the public domain license
https://creativecommons.org/publicdomain/zero/1.0/
