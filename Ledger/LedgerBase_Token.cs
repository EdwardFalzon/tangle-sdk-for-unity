﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tangle.Ledger.Wallet;
using UnityEngine;

namespace Tangle.Ledger
{
    public abstract partial class LedgerBase
    {
        [SerializeField] protected TokenBase _token;
        public IToken Token
        {
            get { return _token; }
            set { _token = (TokenBase)value; }
        }

        #region CreateAliasOuput
        public abstract Task<string> CreateAliasOutput();
        public event Action<string> CreateAliasOutputSuccess;
        public event Action CreateAliasOutputFailure;
        protected virtual void CallCreateAliasOutput(bool successful, string result = null)
        {
            if (successful) CreateAliasOutputSuccess?.Invoke(result);
            else CreateAliasOutputFailure?.Invoke();
        }
        #endregion

        #region CreateNativeToken
        public abstract Task<string> CreateNativeToken();
        public event Action<string> CreateNativeTokenSuccess;
        public event Action CreateNativeTokenFailure;
        protected virtual void CallCreateNativeToken(bool successful, string result = null)
        {
            if (successful) CreateNativeTokenSuccess?.Invoke(result);
            else CreateNativeTokenFailure?.Invoke();
        }
        #endregion

        #region SendNativeToken
        public abstract Task<string> SendNativeToken();
        public event Action<string> SendNativeTokenSuccess;
        public event Action SendNativeTokenFailure;
        protected virtual void CallSendNativeToken(bool successful, string result = null)
        {
            if (successful) SendNativeTokenSuccess?.Invoke(result);
            else SendNativeTokenFailure?.Invoke();
        }
        #endregion

        #region PrepareTransaction
        public abstract Task<string> PrepareTransaction();
        public event Action<string> PrepareTransactionSuccess;
        public event Action PrepareTransactionFailure;
        protected virtual void CallPrepareTransaction(bool successful, string result = null)
        {
            if (successful) PrepareTransactionSuccess?.Invoke(result);
            else PrepareTransactionFailure?.Invoke();
        }
        #endregion

        #region MeltNativeToken
        public abstract Task<string> MeltNativeToken();
        public event Action<string> MeltNativeTokenSuccess;
        public event Action MeltNativeTokenFailure;
        protected virtual void CallMeltNativeToken(bool successful, string result = null)
        {
            if (successful) MeltNativeTokenSuccess?.Invoke(result);
            else MeltNativeTokenFailure?.Invoke();
        }
        #endregion

        #region BurnNativeToken
        public abstract Task<string> BurnNativeToken();
        public event Action<string> BurnNativeTokenSuccess;
        public event Action BurnNativeTokenFailure;
        protected virtual void CallBurnNativeToken(bool successful, string result = null)
        {
            if (successful) BurnNativeTokenSuccess?.Invoke(result);
            else BurnNativeTokenFailure?.Invoke();
        }
        #endregion

        #region GetUnspentOutputs
        public abstract Task<string> GetUnspentOutputs();
        public event Action<string> GetUnspentOuputsSuccess;
        public event Action GetUnspentOuputsFailure;
        protected virtual void CallGetUnspentOuputs(bool successful, string result = null)
        {
            if (successful) GetUnspentOuputsSuccess?.Invoke(result);
            else GetUnspentOuputsFailure?.Invoke();
        }
        #endregion

        #region SignAndSubmitTransaction
        public abstract Task<string> SignAndSubmitTransaction();
        public event Action<string> SignAndSubmitTransactionSuccess;
        public event Action SignAndSubmitTransactionFailure;
        protected virtual void CallSignAndSubmitTransaction(bool successful, string result = null)
        {
            if (successful) SignAndSubmitTransactionSuccess?.Invoke(result);
            else SignAndSubmitTransactionFailure?.Invoke();
        }
        #endregion

        #region DestroyFoundry
        public abstract Task<string> DestroyFoundry();
        public event Action<string> DestroyFoundrySuccess;
        public event Action DestroyFoundryFailure;
        protected virtual void CallDestroyFoundry(bool successful, string result = null)
        {
            if (successful) DestroyFoundrySuccess?.Invoke(result);
            else DestroyFoundryFailure?.Invoke();
        }
        #endregion

        #region ClaimOutputs
        public abstract Task<string> ClaimOutputs();
        public event Action<string> ClaimOutputsSuccess;
        public event Action ClaimOutputsFailure;
        protected virtual void CallClaimOutputs(bool successful, string result = null)
        {
            if (successful) ClaimOutputsSuccess?.Invoke(result);
            else ClaimOutputsFailure?.Invoke();
        }
        #endregion

        #region GetClaimableOutputs
        public abstract Task<string> GetClaimableOutputs();
        public event Action<string> GetClaimableOutputsSuccess;
        public event Action GetClaimableOutputsFailure;
        protected virtual void CallGetClaimableOutputs(bool successful, string result = null)
        {
            if (successful) GetClaimableOutputsSuccess?.Invoke(result);
            else GetClaimableOutputsFailure?.Invoke();
        }
        #endregion
    }
}
