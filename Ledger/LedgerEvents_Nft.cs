﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Events;
using UnityEngine;

namespace Tangle.Ledger
{
    public partial class LedgerEvents
    {
        #region UnityEvents
        // Use these events (NOT events from the LedgerBase component) to make a UI and scene respond to Ledger
        // events.
        [SerializeField] protected UnityEvent<string> _OnMintNFTSuccess;
        [SerializeField] protected UnityEvent _OnMintNFTFailure;

        [SerializeField] protected UnityEvent<string> _OnSendNFTSuccess;
        [SerializeField] protected UnityEvent _OnSendNFTFailure;

        [SerializeField] protected UnityEvent<string> _OnBurnNFTSuccess;
        [SerializeField] protected UnityEvent _OnBurnNFTFailure;

        [SerializeField] protected UnityEvent<string> _OnDestroyAliasSuccess;
        [SerializeField] protected UnityEvent _OnDestroyAliasFailure;
        #endregion

        protected virtual void SubscribeNFT()
        {
            _ledgerPassthrough.MintNFTSuccess += HandleMintNFTSuccess;
            _ledgerPassthrough.MintNFTFailure += HandleMintNFTFailure;

            _ledgerPassthrough.SendNFTSuccess += HandleSendNFTSuccess;
            _ledgerPassthrough.SendNFTFailure += HandleSendNFTFailure;

            _ledgerPassthrough.BurnNFTSuccess += HandleBurnNFTSuccess;
            _ledgerPassthrough.BurnNFTFailure += HandleBurnNFTFailure;

            _ledgerPassthrough.DestroyAliasSuccess += HandleDestroyAliasSuccess;
            _ledgerPassthrough.DestroyAliasFailure += HandleDestroyAliasFailure;
        }

        protected virtual void UnsubscribeNFT()
        {
            _ledgerPassthrough.MintNFTSuccess -= HandleMintNFTSuccess;
            _ledgerPassthrough.MintNFTFailure -= HandleMintNFTFailure;
            ;

            _ledgerPassthrough.SendNFTSuccess -= HandleSendNFTSuccess;
            _ledgerPassthrough.SendNFTFailure -= HandleSendNFTFailure;

            _ledgerPassthrough.BurnNFTSuccess -= HandleBurnNFTSuccess;
            _ledgerPassthrough.BurnNFTFailure -= HandleBurnNFTFailure;

            _ledgerPassthrough.DestroyAliasSuccess -= HandleDestroyAliasSuccess;
            _ledgerPassthrough.DestroyAliasFailure -= HandleDestroyAliasFailure;
        }

        protected virtual void HandleMintNFTSuccess(string result) => _OnMintNFTSuccess?.Invoke(result);
        protected virtual void HandleMintNFTFailure() => _OnMintNFTFailure?.Invoke();

        protected virtual void HandleSendNFTSuccess(string result) => _OnSendNFTSuccess?.Invoke(result);
        protected virtual void HandleSendNFTFailure() => _OnSendNFTFailure?.Invoke();

        protected virtual void HandleBurnNFTSuccess(string result) => _OnBurnNFTSuccess?.Invoke(result);
        protected virtual void HandleBurnNFTFailure() => _OnBurnNFTFailure?.Invoke();

        protected virtual void HandleDestroyAliasSuccess(string result) => _OnDestroyAliasSuccess?.Invoke(result);
        protected virtual void HandleDestroyAliasFailure() => _OnDestroyAliasFailure?.Invoke();

        #region Ledger passthrough commands
        public void MintNFT() => _ledgerPassthrough.MintNFT();
        public void SendNFT() => _ledgerPassthrough.SendNFT();
        public void BurnNFT() => _ledgerPassthrough.BurnNFT();
        public void DestroyAlias() => _ledgerPassthrough.DestroyAlias();
        #endregion

        #region NFT passthrough commands
        public void SetOutputMetadata(string metadata) => _dataStoragePassthrough.Metadata = metadata;
        public void SetOutputTag(string tag) => _dataStoragePassthrough.Tag = tag;
        public void SetOutputRecipientAddress(string reciepientAddress) => _dataStoragePassthrough.RecipientAddress = reciepientAddress;
        #endregion
    }
}
