﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tangle.Ledger.Wallet;

namespace Tangle.Ledger
{
    public partial interface ILedger
    {
        IDataStorage DataStorage { get; set; }

        #region BuildOutput
        /// <summary>
        /// Builds an ouput with a Tag and Metadata then use PrepareTransaction to create a preparedTransaction payload for
        /// SignAndSubmitTransaction.
        /// </summary>
        /// <returns>A string that contains the outputs payload result of the command that was converted from a pointer.</returns>
        Task<string> BuildOutput();
        event Action<string> BuildOutputSuccess;
        event Action BuildOutputFailure;
        #endregion
    }
}
