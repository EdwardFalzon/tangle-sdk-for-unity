﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tangle.Ledger.Wallet;

namespace Tangle.Ledger
{
    public partial interface ILedger
    {
        INft Nft { get; set; }

        #region MintNFT
        /// <summary>
        /// Mints an NFT. The resulting payload will contain the preparedTransaction data needed by SignAndSubmitTransaction to finalize the command.
        /// </summary>
        /// <returns>A string that contains the preparedTransaction payload result of the command that was converted from a pointer.</returns>
        Task<string> MintNFT();
        event Action<string> MintNFTSuccess;
        event Action MintNFTFailure;
        #endregion

        #region SendNFT
        /// <summary>
        /// Sends an NFT to a specified recipient address. The resulting payload will contain the preparedTransaction data needed by SignAndSubmitTransaction to finalize the command.
        /// </summary>
        /// <returns>A string that contains the preparedTransaction payload result of the command that was converted from a pointer.</returns>
        Task<string> SendNFT();
        event Action<string> SendNFTSuccess;
        event Action SendNFTFailure;
        #endregion

        #region BurnNFT
        /// <summary>
        /// Burns one or more NFTs. The resulting payload will contain the preparedTransaction data needed by SignAndSubmitTransaction to finalize the command.
        /// </summary>
        /// <returns>A string that contains the preparedTransaction payload result of the command that was converted from a pointer.</returns>
        Task<string> BurnNFT();
        event Action<string> BurnNFTSuccess;
        event Action BurnNFTFailure;
        #endregion

        #region DestroyAlias
        /// <summary>
        /// Destroys an Alias Output from the account. The resulting payload will contain the preparedTransaction data needed by SignAndSubmitTransaction to finalize the command.
        /// </summary>
        /// <returns>A string that contains the preparedTransaction payload result of the command that was converted from a pointer.</returns>
        Task<string> DestroyAlias();
        event Action<string> DestroyAliasSuccess;
        event Action DestroyAliasFailure;
        #endregion
    }
}
