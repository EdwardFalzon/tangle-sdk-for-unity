﻿using System;
using System.Security;
using Tangle.Ledger.Wallet;
using UnityEngine;
using UnityEngine.Events;
using Security = Tangle.Utilities.Security;

namespace Tangle.Ledger
{
    /// <summary>
    /// LedgerEvents is a bridge between the Unity Editor and the commands and events for both the Ledger and Wallet.
    /// It is used by Unity designers who wish to wire up Ledger commands and events from within the Editor. This is
    /// useful for UI elements like buttons to initiate a ledger command, and for in-scene objects that should react to
    /// ledger events or changes in Wallet state. Place this component in the same scene (usually on the same
    /// GameObject) as the Ledger component being used.
    ///
    /// For UI buttons (etc), wire the interaction events to the public methods found on THIS component; for responsive
    /// objects (such as a coin-balance display, etc) add the component's method to the exposed UnityEvents on THIS
    /// component.
    ///
    /// This is a separate component so the Ledger can be swapped out for another without losing all the in-editor
    /// connections that have been wired up.
    /// </summary>
    public partial class LedgerEvents
    {
        #region UnityEvents
        // Use these events (NOT events from the LedgerBase component) to make a UI and scene respond to Ledger
        // events.
        [SerializeField] protected UnityEvent<IntPtr> _OnCreateWalletSuccess;
        [SerializeField] protected UnityEvent _OnCreateWalletFailure;

        [SerializeField] protected UnityEvent<SecureString> _OnRequestNewMnemonicSuccess;
        [SerializeField] protected UnityEvent _OnRequestNewMnemonicFailure;

        [SerializeField] protected UnityEvent _OnStoreMnemonicSuccess;
        [SerializeField] protected UnityEvent _OnStoreMnemonicFailure;

        [SerializeField] protected UnityEvent<string> _OnCreateNewAccountSuccess;
        [SerializeField] protected UnityEvent _OnCreateNewAccountFailure;

        [SerializeField] protected UnityEvent<string> _OnGetBalancesSuccess;
        [SerializeField] protected UnityEvent _OnGetBalancesFailure;

        [SerializeField] protected UnityEvent<string> _OnSyncAccountSuccess;
        [SerializeField] protected UnityEvent _OnSyncAccountFailure;

        [SerializeField] protected UnityEvent<string> _OnGenerateAddressSuccess;
        [SerializeField] protected UnityEvent _OnGenerateAddressFailure;

        [SerializeField] protected UnityEvent<string> _OnGetFundsSuccess;
        [SerializeField] protected UnityEvent _OnGetFundsFailure;

        [SerializeField] protected UnityEvent<string> _OnTransactionSuccess;
        [SerializeField] protected UnityEvent _OnTransactionFailure;

        [SerializeField] protected UnityEvent _OnMicroTransactionSuccess;
        [SerializeField] protected UnityEvent _OnMicroTransactionFailure;

        [SerializeField] protected UnityEvent<string> _OnGetAccountWithAliasSuccess;
        [SerializeField] protected UnityEvent _OnGetAccountWithAliasFailure;

        [SerializeField] protected UnityEvent<string> _OnGetAccountIndexesSuccess;
        [SerializeField] protected UnityEvent _OnGetAccountIndexesFailure;

        [SerializeField] protected UnityEvent<string> _OnGetAccountWithIndexSuccess;
        [SerializeField] protected UnityEvent _OnGetAccountWithIndexFailure;

        [SerializeField] protected UnityEvent<bool> _OnCloseWalletSuccess;
        [SerializeField] protected UnityEvent<bool> _OnCloseWalletFailure;
        #endregion

        protected virtual void SubscribeWallet()
        {
            _ledgerPassthrough.CreateNewWalletSuccess += HandleCreateNewWalletSuccess;
            _ledgerPassthrough.CreateNewWalletFailure += HandleCreateNewWalletFailure;

            _ledgerPassthrough.RequestNewMnemonicSuccess += HandleRequestNewMnemonicSuccess;
            _ledgerPassthrough.RequestNewMnemonicFailure += HandleRequestNewMnemonicFailure;

            _ledgerPassthrough.StoreMnemonicSuccess += HandleStoreMnemonicSuccess;
            _ledgerPassthrough.StoreMnemonicFailure += HandleStoreMnemonicFailure;

            _ledgerPassthrough.CreateNewAccountSuccess += HandleCreateNewAccountSuccess;
            _ledgerPassthrough.CreateNewAccountFailure += HandleCreateNewAccountFailure;

            _ledgerPassthrough.GenerateAddressSuccess += HandleGenerateAddressSuccess;
            _ledgerPassthrough.GenerateAddressFailure += HandleGenerateAddressFailure;

            _ledgerPassthrough.GetBalancesSuccess += HandleGetBalancesSuccess;
            _ledgerPassthrough.GetBalancesFailure += HandleGetBalancesFailure;

            _ledgerPassthrough.SyncAccountSuccess += HandleSyncAccountSuccess;
            _ledgerPassthrough.SyncAccountFailure += HandleSyncAccountFailure;

            _ledgerPassthrough.RequestFundsFromFaucetSuccess += HandleRequestFundsFromFaucetSuccess;
            _ledgerPassthrough.RequestFundsFromFaucetFailure += HandleRequestFundsFromFaucetFailure;

            _ledgerPassthrough.TransactionSuccess += HandleTransactionSuccess;
            _ledgerPassthrough.TransactionFailure += HandleTransactionFailure;

            _ledgerPassthrough.GetAccountWithAliasSuccess += HandleGetAccountWithAliasSuccess;
            _ledgerPassthrough.GetAccountWithAliasFailure += HandleGetAccountWithAliasFailure;

            _ledgerPassthrough.GetAccountIndexesSuccess += HandleGetAccountIndexesSuccess;
            _ledgerPassthrough.GetAccountIndexesFailure += HandleGetAccountIndexesFailure;

            _ledgerPassthrough.GetAccountWithIndexSuccess += HandleGetAccountWithIndexSuccess;
            _ledgerPassthrough.GetAccountWithIndexFailure += HandleGetAccountWithIndexFailure;

            _ledgerPassthrough.CloseWalletSuccess += HandleCloseWalletSuccess;
            _ledgerPassthrough.CloseWalletFailure += HandleCloseWalletFailure;

            //_ledgerPassthrough.MicroTransactionSuccess += HandleMicroTransactionSuccess;
            //_ledgerPassthrough.MicroTransactionFailure += HandleMicroTransactionFailure;
        }

        protected virtual void UnsubscribeWallet()
        {
            _ledgerPassthrough.CreateNewWalletSuccess -= HandleCreateNewWalletSuccess;
            _ledgerPassthrough.CreateNewWalletFailure -= HandleCreateNewWalletFailure;

            _ledgerPassthrough.RequestNewMnemonicSuccess -= HandleRequestNewMnemonicSuccess;
            _ledgerPassthrough.RequestNewMnemonicFailure -= HandleRequestNewMnemonicFailure;

            _ledgerPassthrough.CreateNewAccountSuccess -= HandleCreateNewAccountSuccess;
            _ledgerPassthrough.CreateNewAccountFailure -= HandleCreateNewAccountFailure;

            _ledgerPassthrough.GenerateAddressSuccess -= HandleGenerateAddressSuccess;
            _ledgerPassthrough.GenerateAddressFailure -= HandleGenerateAddressFailure;

            _ledgerPassthrough.GetBalancesSuccess -= HandleGetBalancesSuccess;
            _ledgerPassthrough.GetBalancesFailure -= HandleGetBalancesFailure;

            _ledgerPassthrough.SyncAccountSuccess -= HandleSyncAccountSuccess;
            _ledgerPassthrough.SyncAccountFailure -= HandleSyncAccountFailure;

            _ledgerPassthrough.RequestFundsFromFaucetSuccess -= HandleRequestFundsFromFaucetSuccess;
            _ledgerPassthrough.RequestFundsFromFaucetFailure -= HandleRequestFundsFromFaucetFailure;

            _ledgerPassthrough.TransactionSuccess -= HandleTransactionSuccess;
            _ledgerPassthrough.TransactionFailure -= HandleTransactionFailure;

            _ledgerPassthrough.GetAccountWithAliasSuccess -= HandleGetAccountWithAliasSuccess;
            _ledgerPassthrough.GetAccountWithAliasFailure -= HandleGetAccountWithAliasFailure;

            _ledgerPassthrough.GetAccountIndexesSuccess -= HandleGetAccountIndexesSuccess;
            _ledgerPassthrough.GetAccountIndexesFailure -= HandleGetAccountIndexesFailure;

            _ledgerPassthrough.GetAccountWithIndexSuccess -= HandleGetAccountWithIndexSuccess;
            _ledgerPassthrough.GetAccountWithIndexFailure -= HandleGetAccountWithIndexFailure;

            _ledgerPassthrough.CloseWalletSuccess -= HandleCloseWalletSuccess;
            _ledgerPassthrough.CloseWalletFailure -= HandleCloseWalletFailure;

            //_ledgerPassthrough.MicroTransactionSuccess -= HandleMicroTransactionSuccess;
            //_ledgerPassthrough.MicroTransactionFailure -= HandleMicroTransactionFailure;
        }

        protected virtual void HandleCreateNewWalletSuccess(IntPtr wallet) => _OnCreateWalletSuccess?.Invoke(wallet);
        protected virtual void HandleCreateNewWalletFailure() => _OnCreateWalletFailure?.Invoke();

        protected virtual void HandleRequestNewMnemonicSuccess(SecureString mnemonic) =>
            _OnRequestNewMnemonicSuccess?.Invoke(mnemonic);
        protected virtual void HandleRequestNewMnemonicFailure() => _OnRequestNewMnemonicFailure?.Invoke();

        protected virtual void HandleStoreMnemonicSuccess() => _OnStoreMnemonicSuccess?.Invoke();
        protected virtual void HandleStoreMnemonicFailure() => _OnStoreMnemonicFailure?.Invoke();

        protected virtual void HandleCreateNewAccountSuccess(string accountDetails) => _OnCreateNewAccountSuccess?.Invoke(accountDetails);
        protected virtual void HandleCreateNewAccountFailure() => _OnCreateNewAccountFailure?.Invoke();

        protected virtual void HandleGenerateAddressSuccess(string addressDetails) =>
            _OnGenerateAddressSuccess?.Invoke(addressDetails);
        protected virtual void HandleGenerateAddressFailure() => _OnGenerateAddressFailure?.Invoke();

        protected virtual void HandleGetBalancesSuccess(string balances) => _OnGetBalancesSuccess?.Invoke(balances);
        protected virtual void HandleGetBalancesFailure() => _OnGetBalancesFailure?.Invoke();

        protected virtual void HandleSyncAccountSuccess(string balance) => _OnSyncAccountSuccess?.Invoke(balance);
        protected virtual void HandleSyncAccountFailure() => _OnSyncAccountFailure?.Invoke();

        protected virtual void HandleRequestFundsFromFaucetSuccess(string result) => _OnGetFundsSuccess?.Invoke(result);
        protected virtual void HandleRequestFundsFromFaucetFailure() => _OnGetFundsFailure?.Invoke();

        protected virtual void HandleTransactionSuccess(string result) => _OnTransactionSuccess?.Invoke(result);
        protected virtual void HandleTransactionFailure() => _OnTransactionFailure?.Invoke();

        protected virtual void HandleMicroTransactionSuccess() => _OnMicroTransactionSuccess?.Invoke();
        protected virtual void HandleMicroTransactionFailure() => _OnMicroTransactionFailure?.Invoke();

        protected virtual void HandleGetAccountWithAliasSuccess(string account) => _OnGetAccountWithAliasSuccess?.Invoke(account);
        protected virtual void HandleGetAccountWithAliasFailure() => _OnGetAccountWithAliasFailure?.Invoke();

        protected virtual void HandleGetAccountIndexesSuccess(string account) => _OnGetAccountIndexesSuccess?.Invoke(account);
        protected virtual void HandleGetAccountIndexesFailure() => _OnGetAccountIndexesFailure?.Invoke();

        protected virtual void HandleGetAccountWithIndexSuccess(string account) => _OnGetAccountWithIndexSuccess?.Invoke(account);
        protected virtual void HandleGetAccountWithIndexFailure() => _OnGetAccountWithIndexFailure?.Invoke();

        protected virtual void HandleCloseWalletSuccess(bool result) => _OnCloseWalletSuccess?.Invoke(result);
        protected virtual void HandleCloseWalletFailure(bool result) => _OnCloseWalletFailure?.Invoke(result);

        #region Ledger passthrough commands
        public void CreateNewWallet() => _ledgerPassthrough.CreateNewWallet();
        public void RequestNewMnemonic() => _ledgerPassthrough.RequestNewMnemonic();
        public void StoreMnemonic() => _ledgerPassthrough.StoreMnemonic();
        public void CreateNewAccount() => _ledgerPassthrough.CreateNewAccount();
        public void GenerateNewAddresses() => _ledgerPassthrough.GenerateNewAddress();
        public async void GetBalance() => await _ledgerPassthrough.GetBalance();
        public async void SyncAccount() => await _ledgerPassthrough.SyncAccount();
        public void RequestFundsFromFaucet() => _ledgerPassthrough.RequestFundsFromFaucet();
        public void Transaction() => _ledgerPassthrough.Transaction();
        public void GetAccountWithAlias() => _ledgerPassthrough.GetAccountWithAlias();
        public void GetAccountIndexes() => _ledgerPassthrough.GetAccountIndexes();
        public void GetAccountWithIndex() => _ledgerPassthrough.GetAccountWithIndex();
        public void CloseWallet() => _ledgerPassthrough.CloseWallet();
        //public void MicroTransaction() => _ledgerPassthrough.MicroTransaction();
        #endregion

        #region Wallet passthrough commands
        public void SetWalletDirectory(string path) => _walletPassthrough.WalletLocation = path;
        public void SetStrongholdNameAndPath(string path) => _walletPassthrough.StrongholdLocation = path;
        public void SetWalletPassword(string password) => _walletPassthrough.Password = Security.Secure(password);
        public void SetAccountName(string accountName) => _walletPassthrough.NewAccountName = accountName;
        public void SetCoinType(string coinType) => _walletPassthrough.SendAmount = coinType;
        public void SetSendAmount(string sendAmount) => _walletPassthrough.SendAmount = sendAmount;
        public void SetDestinationAddress(string destinationAddress) => _walletPassthrough.DestinationAddress = destinationAddress;
        public void SetAccountIndex(string index) => _walletPassthrough.Index = index;
        public void SetAlias(string alias) => _walletPassthrough.Alias = alias;
        #endregion
    }
}