using System;
using System.Security;
using System.Threading.Tasks;
using Tangle.Ledger.Wallet;
using UnityEngine;

namespace Tangle.Ledger
{
	/// <summary>
	/// LedgerBase is an abstract class that only implements ILedger (qv). It is a convenience class that inherits
	/// from MonoBehaviour for more-convenient compatibility with Unity, which doesn't always play well with
	/// interfaces. Developers can create variables, fields, properties and other references of type ILedger or
	/// LedgerBase (qv), depending on needs, but for consistency, all concrete Unity/MonoBehaviour implementations of
	/// ILedger should inherit from this LedgerBase rather than implementing ILedger directly.
	///
	/// Included in this class are virtual event-callers to allow descendant classes to safely invoke this abstract
	/// class' events.
	/// 
	/// Also refer to ILedger for additional information on the abstract method-signatures herein.
	/// </summary>
	[DisallowMultipleComponent]
	public abstract partial class LedgerBase : MonoBehaviour, ILedger
	{
		/// <summary>
		/// For Unity, we need a WalletBase (qv) MonoBehaviour for visibility in the Editor, but it's passed in code as
		/// the IWallet (qv) property.
		/// </summary>
		[SerializeField] protected WalletBase _wallet;
		public IWallet Wallet
		{
			get { return _wallet;}
			set { _wallet = (WalletBase) value; }
		}

		#region CreateNewWallet
			public abstract Task<IntPtr> CreateNewWallet();
			public event Action<IntPtr> CreateNewWalletSuccess;
			public event Action CreateNewWalletFailure;
			
			/// <summary>
			/// Allows descendant classes to invoke the events declared in this ancestor.
			/// </summary>
			/// <param name="successful">Whether the event to be called is Success. If false, the Failure event is
			/// called.</param>
			/// <param name="walletPtr">Pointer to the created wallet; send IntPtr.Zero if creation failed.</param>
			protected virtual void CallCreateWallet(bool successful, IntPtr walletPtr)
			{
				if (successful)
					if (walletPtr != IntPtr.Zero)
					{
						CreateNewWalletSuccess?.Invoke(walletPtr);
					}
					else
					{
						Debug.LogError("[LedgerBase.CallCreateWallet] ERROR: Successful wallet-creation but zero pointer!");
						CreateNewWalletFailure?.Invoke();
					}
				else
				{
					CreateNewWalletFailure?.Invoke();
				}
			}
		#endregion

		#region RequestNewMnemonic
			public abstract Task<SecureString> RequestNewMnemonic();
			public event Action<SecureString> RequestNewMnemonicSuccess;
			public event Action RequestNewMnemonicFailure;

			/// <summary>
			/// Allows descendant classes to invoke the events declared in this ancestor.
			/// </summary>
			/// <param name="successful">Whether the event to be called is Success. If false, the Failure event is
			/// called.</param>
			/// <param name="mnemonic">Optional. Mnemonic received from ledger. NOTE: This is not yet attached to any
			/// wallet.</param>
			protected virtual void CallRequestNewMnemonic(bool successful, SecureString mnemonic = null)
			{
				if (successful)
					if (mnemonic != null)
					{
						RequestNewMnemonicSuccess?.Invoke(mnemonic);
					}
					else
					{
						Debug.LogError("[LedgerBase.CallCreateWallet] ERROR: Successful wallet-creation but zero pointer!");
						RequestNewMnemonicFailure?.Invoke();
					}
				else
				{
					RequestNewMnemonicFailure?.Invoke();
				}
			}
		#endregion
		
		#region StoreMnemonic
			public abstract Task<bool> StoreMnemonic();
			public event Action StoreMnemonicSuccess;
			public event Action StoreMnemonicFailure;

			/// <summary>
			/// Allows descendant classes to invoke the events declared in this ancestor.
			/// </summary>
			/// <param name="successful">Whether the event to be called is Success. If false, the Failure event is
			/// called.</param>
			protected virtual void CallStoreMnemonic(bool successful)
			{
				if (successful) StoreMnemonicSuccess?.Invoke();
				else StoreMnemonicFailure?.Invoke();
			}
		#endregion

		#region CreateNewAccount
			/// <summary>
			/// Creates an account in the attached wallet, of the name specified in the wallet.
			/// </summary>
			public abstract Task<string> CreateNewAccount ();
			public event Action<string> CreateNewAccountSuccess;
			public event Action CreateNewAccountFailure;
			
			/// <summary>
			/// Allows descendant classes to invoke the events declared in this ancestor.
			/// </summary>
			/// <param name="successful"></param>
			/// <param name="accountDetails"></param>
			protected virtual void CallCreateNewAccount(bool successful, string accountDetails = null)
			{
				if (successful) CreateNewAccountSuccess?.Invoke(accountDetails);
				else CreateNewAccountFailure?.Invoke();
			}
		#endregion

		#region GenerateNewAddress
			/// <summary>
			/// Generates a new address for the account.
			/// </summary>
			/// <returns>A string indicating a successful or failed command execution.</returns>
			public abstract Task<string> GenerateNewAddress();
			public event Action<string> GenerateAddressSuccess;
			public event Action GenerateAddressFailure;
			
			/// <summary>
			/// Allows descendant classes to invoke the events declared in this ancestor.
			/// </summary>
			/// <param name="successful">Whether the event to be called is Success. If false, the Failure event is
			/// called.</param>
			/// <param name="newAddress"></param>
			protected virtual void CallGenerateAddress(bool successful, string newAddress = null)
			{
				if (successful) GenerateAddressSuccess?.Invoke(newAddress);
				else GenerateAddressFailure?.Invoke();
			}
		#endregion

		#region RequestFundsFromFaucet
			/// <summary>
			/// Requests funds from the network.
			/// </summary>
			/// <returns>A string indicating a successful or failed command execution.</returns>
			public abstract Task<string> RequestFundsFromFaucet();
			public event Action<string> RequestFundsFromFaucetSuccess;
			public event Action RequestFundsFromFaucetFailure;
			
			/// <summary>
			/// Allows descendant classes to invoke the events declared in this ancestor.
			/// </summary>
			/// <param name="successful">Whether the event to be called is Success. If false, the Failure event is
			/// called.</param>
			/// <param name="result"></param>
			protected virtual void CallRequestFundsFromFaucet(bool successful, string result = null)
			{
				if (successful) RequestFundsFromFaucetSuccess?.Invoke(result);
				else RequestFundsFromFaucetFailure?.Invoke();
			}
		#endregion

		#region GetBalance
		/// <summary>
		/// Gets the account balance that is stored locally.
		/// </summary>
		/// <returns>A string containing the account balance.</returns>
		public abstract Task<string> GetBalance();
			public event Action<string> GetBalancesSuccess;
			public event Action GetBalancesFailure;
			
			/// <summary>
			/// Allows descendant classes to invoke the events declared in this ancestor.
			/// </summary>
			/// <param name="successful">Whether the event to be called is Success. If false, the Failure event is
			/// called.</param>
			/// <param name="newBalances"></param>
			protected virtual void CallGetBalances(bool successful, string newBalances = null)
			{
				if (successful) GetBalancesSuccess?.Invoke(newBalances);
				else GetBalancesFailure?.Invoke();
			}
        #endregion

        #region SyncAccount
		/// <summary>
		/// Syncs the local account with the network.
		/// </summary>
		/// <returns>A string containing the account balance.</returns>
        public abstract Task<string> SyncAccount();
	        public event Action<string> SyncAccountSuccess;
	        public event Action SyncAccountFailure;

	        /// <summary>
	        /// Allows descendant classes to invoke the events declared in this ancestor.
	        /// </summary>
	        /// <param name="successful">Whether the event to be called is Success. If false, the Failure event is
	        /// called.</param>
	        /// <param name="newBalances"></param>
	        protected virtual void CallSyncAccount(bool successful, string newBalances = null)
	        {
	            if (successful) SyncAccountSuccess?.Invoke(newBalances);
	            else SyncAccountFailure?.Invoke();
	        }
        #endregion

        #region GetAccountWithAlias
		/// <summary>
		/// Retrieves the account using its alias or name.
		/// </summary>
		/// <returns>A string containing the account details.</returns>
        public abstract Task<string> GetAccountWithAlias();
	        public event Action<string> GetAccountWithAliasSuccess;
	        public event Action GetAccountWithAliasFailure;
	        
	        /// <summary>
	        /// Allows descendant classes to invoke the events declared in this ancestor.
	        /// </summary>
	        /// <param name="successful">Whether the event to be called is Success. If false, the Failure event is
	        /// called.</param>
	        /// <param name="accountDetails"></param>
	        protected virtual void CallGetAccountWithAlias(bool successful, string accountDetails = null)
	        {
	            if (successful) GetAccountWithAliasSuccess?.Invoke(accountDetails);
	            else GetAccountWithAliasFailure?.Invoke();
	        }
        #endregion

        #region GetAccountIndexes
		/// <summary>
		/// Retrieves a list of account indexes stored in the wallet.
		/// </summary>
		/// <returns>A string containing the list of the indexes of all accounts in the wallet.</returns>
        public abstract Task<string> GetAccountIndexes();
        public event Action<string> GetAccountIndexesSuccess;
        public event Action GetAccountIndexesFailure;
        protected virtual void CallGetAccountIndexes(bool successful, string accountDetails = null)
        {
            if (successful) GetAccountIndexesSuccess?.Invoke(accountDetails);
            else GetAccountIndexesFailure?.Invoke();
        }
        #endregion

        #region GetAccountWithIndex
        /// <summary>
        /// Retrieves the account using its index.
        /// </summary>
        /// <returns>A string containing the account details.</returns>
        public abstract Task<string> GetAccountWithIndex();
        public event Action<string> GetAccountWithIndexSuccess;
        public event Action GetAccountWithIndexFailure;
        protected virtual void CallGetAccountWithIndex(bool successful, string accountDetails = null)
        {
            if (successful) GetAccountWithIndexSuccess?.Invoke(accountDetails);
            else GetAccountWithIndexFailure?.Invoke();
        }
        #endregion

        #region Transaction
        /// <summary>
        /// Sends base coin to another account.
        /// </summary>
        public abstract Task<string> Transaction();
        public event Action<string> TransactionSuccess;
        public event Action TransactionFailure;

        /// <summary>
        /// Allows descendant classes to invoke the events declared in this ancestor.
        /// </summary>
        /// <param name="successful">Whether the event to be called is Success. If false, the Failure event is
        /// called.</param>
        protected virtual void CallTransaction(bool successful, string sendFunds = null)
        {
            if (successful) TransactionSuccess?.Invoke(sendFunds);
            else TransactionFailure?.Invoke();
        }
        #endregion

        #region CloseWallet
        /// <summary>
        /// Logs out the user and destroys the wallet handle in memory.
        /// </summary>
        /// <returns>A bool indicating if the wallet handle was successfully destroyed.</returns>
        public abstract Task<bool> CloseWallet();
	        public event Action<bool> CloseWalletSuccess;
	        public event Action<bool> CloseWalletFailure;
	        
	        /// <summary>
	        /// Allows descendant classes to invoke the events declared in this ancestor.
	        /// </summary>
	        /// <param name="successful">Whether the event to be called is Success. If false, the Failure event is
	        /// called.</param>
	        protected virtual void CallCloseWallet(bool successful)
	        {
	            if (successful) CloseWalletSuccess?.Invoke(successful);
	            else CloseWalletFailure?.Invoke(successful);
	        }
        #endregion

        /*
            #region MicroTransaction
                public abstract Task MicroTransaction(float amountToSend, string coinType, string destinationAddress);
                public event Action MicroTransactionSuccess;
                public event Action MicroTransactionFailure;
            #endregion
        */


        protected abstract Task<IntPtr> GetClient(IntPtr walletPtr);

		/*
		// WIP: Generic method-caller for LedgerBase
		protected virtual void CallAction<T>(string eventName, T param)
		{
			Type ILedgerType      = typeof(ILedger);
			EventInfo eventInfo   = ILedgerType.GetEvent(eventName);
			Debug.Log(eventInfo.Name);
			Action<SecureString> eventHandler = (Action<SecureString>)eventInfo.GetAddMethod().CreateDelegate(typeof(Action<SecureString>), gameObject);
			
			//MethodInfo methodInfo = eventInfo.GetRaiseMethod();
			//Debug.Log(methodInfo.Name);
			object t = this;
			object[] o = new object[] { param };
			eventHandler?.Invoke(param as SecureString);
		}
		*/
	}
}