using System;
using System.Collections.Generic;
using System.Security;
using Tangle.Ledger.IotaSdk010;

namespace Tangle.Ledger.Wallet
{
	/// <summary>
	/// IWallet is an Interface for a crypto-wallet. It is NOT responsible for on-chain transactions, but rather it
	/// passively listens for events invoked by the selected Ledger (of type Iledger or LedgerBase) and stores/caches
	/// the information returned. It is this cache that devs should use to populate the UI. IWallet also holds
	/// connections (such as pointers) to the active wallet, Secret Manager and so on.
	///
	/// Although you could use IWallet or WalletBase directly to obtain and display wallet information to the player,
	/// it is recommended that you use LedgerEvents as an intermediary. This allows removal and replacement of a wallet
	/// without resetting all the UI wiring to events and commands.
	/// </summary>
	public interface IWallet
	{
		IntPtr WalletPtr { get; set; }
		IntPtr ClientPtr { get; set; }
		IntPtr SecretManagerPtr { get; set; }
		
		// List of all accounts in the current wallet.
		Dictionary<int, AccountPayload> Accounts { get; }
		int CurrentAccountIndex { get; set; }

		// List of the balances of all accounts. Use the same CurrentAccountIndex.
		Dictionary<int, BalancePayload> Balances { get; }

		// The location of the wallet on the player's drive. This could be fixed by the dev or player-defined.
		string WalletLocation { get; set; }

		// The location of the Stronghold on the player's drive. This could be fixed by the dev or player-defined.
		string StrongholdLocation { get; set; }
		
		// When creating a new account, set the account name here.
		string NewAccountName { get; set; }
		
		// When sending funds, set the coin type here.
		string CoinType { get; set; }
		
		// When sending funds, set the amount here.
		string SendAmount { get; set; }
		
		// When sending funds, set the destination address here.
		string DestinationAddress { get; set; }

		// When getting an account with an index, set the index here.
        string Index { get; set; }

        // When getting an account with an alias, set the alias here.
        string Alias { get; set; }
		
		// When creating a wallet, set the mnemonic here.
		SecureString SecureMnemonic { get; set; }
		
		// When creating a wallet, set the password here.
		SecureString Password { get; set; }
	}
}