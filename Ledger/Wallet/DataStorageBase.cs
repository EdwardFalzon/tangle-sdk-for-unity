﻿using NaughtyAttributes;
using Tangle.Ledger;
using Tangle.Ledger.Wallet;
using UnityEngine;

namespace Tangle.Ledger.Wallet
{
    public abstract class DataStorageBase : MonoBehaviour, IDataStorage
    {
        [Required("LedgerBase component is required. Please provide one.")]
        [SerializeField]
        protected LedgerBase _ledger;

        [Tooltip("Use a LedgerEvents component to more-safely wire up the UI.")]
        [SerializeField]
        protected LedgerEvents _ledgerEvents;

        public string RecipientAddress { get; set; }
        public string Metadata { get; set; }
        public string Tag { get; set; }
    }
}
