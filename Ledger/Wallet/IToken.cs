﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tangle.Ledger.Wallet
{
    public interface IToken
    {
        // When creating a new native token, set the circulating supply here. 
        int circulatingSupply { get; set; }
        // When creating a new native token, set the maximum supply here.
        int maximumSupply { get; set; }

        #region Immutable Metadata Contents
        // These fields are not directly passed to the CreateNativeToken command.
        // You can use it in your scene or you can also just use local variables.
        // Set the token name here.
        string tokenName { get; set; }
        // Set the token description here.
        string tokenDescription { get; set; }
        // Set the token symbol here.
        string tokenSymbol { get; set; }
        // Set the decimal places here.
        uint tokenDecimals { get; set; }
        // Set the token URL here.
        string tokenURL { get; set; }
        #endregion

        // Use this when creating Native Tokens. This is passed to the command.
        string tokenImmutableData { get; set; }
        // When sending native tokens, set the amount to send here.
        int sendTokenAmount { get; set; }
        // When sending native tokens, set the recipient address here.
        string recipientAddress { get; set; }
        // When sending, melting, and burning native tokens, set the token ID here.
        string tokenID { get; set; }
        // When sending native tokens, set the return strategy here.
        string returnStrategy { get; set; }
        // When sending native tokens, you'll get an outputsPayload, set it here.
        string outputsPayload { get; set; }
        // When melting native tokens, set the amount to melt here.
        int meltTokenAmount { get; set; }
        // When burning native tokens, set the amount to burn here.
        int burnTokenAmount { get; set; }
        // When using GetUnspentOutputs, set the foundry ID here.
        string foundryID { get; set; }
        // When receiving a preparedTransaction from the commands, set the preparedTransaction payload here.
        string preparedTransactionPayload { get; set; }
        // When destroying a foundry, set the foundry ID here.
        string destroyFoundryID { get; set; }
        // When claiming locked outputs, set the output IDs here.
        string claimOutputIds { get; set; }
    }
}
