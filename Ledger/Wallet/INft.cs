﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tangle.Ledger.Wallet
{
    public interface INft
    {
        // When creating an NFT, set the immutable metadata here.
        string nftImmutableMetadata { get; set; }
        // When creating an NFT, set the NFT tag here.
        string nftTag { get; set; }
        // When creating an NFT, set the issuer address here.
        string nftIssuerAddress { get; set; }
        // When sending an NFT, set the recipient address here.
        string sendNftAddress { get; set; }
        // When sending an NFT, set the NFT ID here.
        string sendNftId { get; set; }
        // When burning NFTs, set the NFT IDs here.
        List<string> burnNftIds { get; set; }
        // When destroying alias outputs, set the alias output IDs here.
        List<string> nftAliases { get; set; }
    }
}
