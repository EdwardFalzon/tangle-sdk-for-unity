﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tangle.Ledger.Wallet
{
    public class DataStorage: DataStorageBase
    {
        protected virtual void OnValidate()
        {
            AssignLedgerComponents();
        }

        /// <summary>
        /// This is a quality-of-life method. It looks for LedgerBase and LedgerEvents and assigns them to the requisite variables.
        /// </summary>
        private void AssignLedgerComponents(LedgerBase newLedger = null, LedgerEvents newLedgerEvents = null)
        {
            if (newLedger != null) _ledger = newLedger;

            // _ledger must not be null and _ledgerEvents must be assigned if it exists in the project.
            // If they are null, check the GameObject for both components.
            _ledger ??= GetComponentInParent<LedgerBase>();
            _ledgerEvents ??= GetComponentInParent<LedgerEvents>();

            // If either is still null, perform a slower search across the scene.
            _ledger ??= FindObjectOfType<LedgerBase>();
            _ledgerEvents ??= FindObjectOfType<LedgerEvents>();

            if (_ledger != null) _ledger.DataStorage = this;
        }
    }
}
