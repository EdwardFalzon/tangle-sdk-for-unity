using System;
using System.Security;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using Tangle.Ledger.IotaSdk010;

namespace Tangle.Ledger.Wallet
{
    public abstract class WalletBase : MonoBehaviour, IWallet
    {
        [Required ("LedgerBase component is required. Please provide one.")] [SerializeField]
        protected LedgerBase _ledger;

        [Tooltip("Use a LedgerEvents component to more-safely wire up the UI.")] [SerializeField]
        protected LedgerEvents _ledgerEvents;

        public List<Dictionary<string, string>> CoinBalances { get; set; }

        public IntPtr WalletPtr { get; set; }
        public IntPtr ClientPtr { get; set; }
        public IntPtr SecretManagerPtr { get; set; }
        public Dictionary<int, AccountPayload> Accounts { get; protected set; } = new Dictionary<int, AccountPayload>();
        public int CurrentAccountIndex { get; set; }
        public Dictionary<int, BalancePayload> Balances { get; private set; } = new Dictionary<int, BalancePayload>();
        public string WalletLocation { get; set; }
        public string StrongholdLocation { get; set; }
        public string NewAccountName { get; set; }
        public string CoinType { get; set; }
        public string SendAmount { get; set; }
        public string DestinationAddress { get; set; }
        public SecureString Password { get; set; }
        public SecureString SecureMnemonic { get; set; }
        public string Index { get; set; }
        public string Alias { get; set; }
    }
}