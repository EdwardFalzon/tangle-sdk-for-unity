﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tangle.Ledger.Wallet
{
    public interface IDataStorage
    {
        // When using data storage, set the recipient address here.
        string RecipientAddress { get; set; }
        // When using data storage, set the tag here.
        string Tag { get; set; }
        // When using data storage, set the metadata here.
        string Metadata { get; set; }
    }
}
