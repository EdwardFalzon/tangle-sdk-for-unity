using System;
using System.Security;
using Newtonsoft.Json;
using Tangle.Ledger.IotaSdk010;
using UnityEngine;

namespace Tangle.Ledger.Wallet
{
	/// <summary>
	/// Wallet contains a local cache of token balances and NFT data, along
	/// along with methods to read, set or reset that data locally. It must be
	/// placed on the same GameObject as the Ledger you're using. At this time,
	/// there should only be one Wallet in the project.
	/// </summary>
	[DisallowMultipleComponent]
	public class Wallet : WalletBase
	{
		protected virtual void OnValidate()
		{
			AssignLedgerComponents();
		}

		/// <summary>
		/// This is a quality-of-life method. It looks for LedgerBase and LedgerEvents and assigns them to the requisite variables.
		/// </summary>
		private void AssignLedgerComponents(LedgerBase newLedger = null, LedgerEvents newLedgerEvents = null)
		{
			if (newLedger != null) _ledger = newLedger;
			
			// _ledger must not be null and _ledgerEvents must be assigned if it exists in the project.
			// If they are null, check the GameObject for both components.
			_ledger ??= GetComponentInParent<LedgerBase>();
			_ledgerEvents ??= GetComponentInParent<LedgerEvents>();

			// If either is still null, perform a slower search across the scene.
			_ledger ??= FindObjectOfType<LedgerBase>();
			_ledgerEvents ??= FindObjectOfType<LedgerEvents>();

			if (_ledger != null) _ledger.Wallet = this;
		}

		protected virtual void OnEnable()
		{
			OnValidate();
			if (_ledger != null)
			{
				Subscribe();
			}
			else
			{
				Debug.LogError("[Wallet.OnEnable] Error: No ledger found! On-chain commands will fail.");
			}
		}

		protected virtual void OnDisable()
		{
			Unsubscribe();
		}
		
		protected virtual void Subscribe()
		{
			_ledger.CreateNewWalletSuccess += CacheWalletPointer;
			_ledger.GetBalancesSuccess += CacheBalance;
			_ledger.SyncAccountSuccess += CacheBalance;
            _ledger.RequestNewMnemonicSuccess += CacheMnemonic;
			_ledger.CreateNewAccountSuccess += CacheAccount;
			_ledger.GetAccountWithAliasSuccess += CacheAccount;
            _ledger.GetAccountWithIndexSuccess += CacheAccount;
        }

		protected virtual void Unsubscribe()
		{
			_ledger.CreateNewWalletSuccess -= CacheWalletPointer;
			_ledger.GetBalancesSuccess -= CacheBalance;
            _ledger.SyncAccountSuccess -= CacheBalance;
            _ledger.RequestNewMnemonicSuccess -= CacheMnemonic;
			_ledger.CreateNewAccountSuccess -= CacheAccount;
            _ledger.GetAccountWithAliasSuccess -= CacheAccount;
            _ledger.GetAccountWithIndexSuccess -= CacheAccount;
        }

		protected virtual void CacheWalletPointer(IntPtr newWalletPtr)
		{
			WalletPtr = newWalletPtr;
		}

		/// <summary>
		/// Converts and caches the received balance payload. NOTE: Uses the CurrentAccountIndex to store the incoming
		/// balance, possibly overwriting an existing record.
		/// </summary>
		/// <param name="json">String containing the raw balance information.</param>
		protected virtual void CacheBalance(string json)
		{
			Debug.Log(json);
			var newBalanceResult = JsonConvert.DeserializeObject<BalanceResult>(json);
			Balances[CurrentAccountIndex] = newBalanceResult.payload;
		}
		
		/// <summary>
		/// Caches a newly generated mnemonic and stores it in the current wallet's Secret Manager.
		///
		/// WARNING: Irrevocably overwrites the existing mnemonic.
		/// </summary>
		/// <param name="newMnemonic"></param>
		protected virtual void CacheMnemonic(SecureString newMnemonic)
		{
			if (newMnemonic != null)
			{
				SecureMnemonic = newMnemonic;
			}
			else
				Debug.LogError("[Wallet.CacheMnemonic] ERROR: Mnemonic provided is null. Cannot store null.");
		}

		protected void CacheAccount(string json)
		{
			var accountResult = JsonConvert.DeserializeObject<AccountResult>(json);
			CurrentAccountIndex = accountResult.payload.index;
			Accounts[CurrentAccountIndex] = accountResult.payload;
		}
		
		protected void CacheNewAddress(string json)
		{
			// Place-holder code to extract Shimmer address.
//			CurrentAddress = json.Substring(json.IndexOf("rms1"), 63);
			#if DEBUG
//				Debug.Log($"[Wallet.CacheNewAddress] Address cached: >>{CurrentAddress}<<");
			#endif
		}
	}
}