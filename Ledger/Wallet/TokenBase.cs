﻿using NaughtyAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Tangle.Ledger.Wallet
{
    public abstract class TokenBase : MonoBehaviour, IToken
    {
        [Required("LedgerBase component is required. Please provide one.")][SerializeField]
        protected LedgerBase _ledger;

        [Tooltip("Use a LedgerEvents component to more-safely wire up the UI.")][SerializeField]
        protected LedgerEvents _ledgerEvents;

        public int circulatingSupply { get; set; }
        public int maximumSupply { get; set; }
        public string tokenName { get; set; }
        public string tokenDescription { get; set; }
        public string tokenSymbol { get; set; }
        public uint tokenDecimals { get; set; }
        public string tokenURL { get; set; }
        public string tokenImmutableData { get; set; }
        public int sendTokenAmount { get; set; }
        public string recipientAddress { get; set; }
        public string tokenID { get; set; }
        public string returnStrategy { get; set; }
        public string outputsPayload { get; set; }
        public int meltTokenAmount { get; set; }
        public int burnTokenAmount { get; set; }
        public string foundryID { get; set; }
        public string preparedTransactionPayload { get; set; }
        public string destroyFoundryID { get; set; }
        public string claimOutputIds { get; set; }
    }
}
