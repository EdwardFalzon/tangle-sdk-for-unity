﻿using NaughtyAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tangle.Ledger;
using Tangle.Ledger.Wallet;
using UnityEngine;

namespace Tangle.Ledger.Wallet
{
    public abstract class NftBase : MonoBehaviour, INft
    {
        [Required("LedgerBase component is required. Please provide one.")][SerializeField]
        protected LedgerBase _ledger;

        [Tooltip("Use a LedgerEvents component to more-safely wire up the UI.")][SerializeField]
        protected LedgerEvents _ledgerEvents;

        public string nftTag { get; set; }
        public string nftIssuerAddress { get; set; }
        public string sendNftAddress { get; set; }
        public string sendNftId { get; set; }
        public List<string> burnNftIds { get; set; }
        public List<string> nftAliases { get; set; }
        public string nftImmutableMetadata { get; set; }
    }
}
