﻿using System;
using System.Security;
using System.Threading.Tasks;
using Tangle.Ledger.Wallet;

namespace Tangle.Ledger
{
	/// <summary>
	/// ILedger is the interface for all Ledger APIs developed under the Tangle namespace. It contains all available
	/// transactions, commands and events for the APIs, along with success and failure events for each.
	///
	/// NOTE: You must populate all necessary fields in Wallet (qv) before calling any method. This approach allows
	/// you to wire up UI elements with dynamic variables.
	///
	/// For Unity projects, it's advisable to inherit ILedger from LedgerBase, which implements ILedger.
	///
	/// (Currently, all ledger transactions are in this somewhat monolithic interface. In time, it will be broken down
	/// into subcategories.)
	/// </summary>
	public partial interface ILedger
	{
		/// <summary>
		/// Any ledger requires a connection to local wallet. This field stores cached values from the wallet, as
		/// results come in from the API calls. This Wallet is also used to store the parameters required for all
		/// ledger transactions herein.
		/// </summary>
		IWallet Wallet { get; set; }
		
		#region CreateNewWallet
			/// <summary>
			/// Creates a new, empty wallet and snapshot/stronghold on the local device. Checks the IWallet for
			/// wallet location, snapshot location and password. It does not have a mnemonic in the secret manager
			/// and therefore it also has no accounts.
			/// </summary>
			/// <returns>Pointer to the newly created wallet.</returns>
			Task<IntPtr> CreateNewWallet ();
			event Action<IntPtr> CreateNewWalletSuccess;
			event Action CreateNewWalletFailure;
		#endregion
		
		#region RequestNewMnemonic
			/// <summary>
			/// Creates a new, secure mnemonic. Does not connect the mnemonic to any wallet (see StoreMnemonic).
			/// </summary>
			/// <returns>The new mnemonic as a SecureString.</returns>
			Task<SecureString> RequestNewMnemonic ();
			event Action<SecureString> RequestNewMnemonicSuccess;
			event Action RequestNewMnemonicFailure;
		#endregion

		#region StoreMnemonic
			/// <summary>
			/// Stores a given mnemonic into a given wallet, integrating the two for security purposes. This step is
			/// required before creating an account or pretty much doing anything with a new wallet. Does not create
			/// the mnemonic (see RequestNewMnemonic).
			/// </summary>
			/// <returns>True if mnemonic is successfully stored; false otherwise.</returns>
			Task<bool> StoreMnemonic();
			event Action StoreMnemonicSuccess;
			event Action StoreMnemonicFailure;
		#endregion

		#region CreateNewAccount
			/// <summary>
			/// Creates a new account in the current wallet.
			/// </summary>
			/// <returns>Result from Network request, in JSON format.</returns>
			Task<string> CreateNewAccount ();
			event Action<string> CreateNewAccountSuccess;
			event Action CreateNewAccountFailure;
		#endregion

		#region GenerateNewAddress
			/// <summary>
			/// Creates a new wallet address for the current account.
			/// </summary>
			/// <returns>Result from Network request, in JSON format.</returns>
			Task<string> GenerateNewAddress ();
			event Action<string> GenerateAddressSuccess;
			event Action GenerateAddressFailure;
		#endregion

		#region RequestFundsFromFaucet
			/// <summary>
			/// Requests coins from a faucet, if the configured network has one.
			/// </summary>
			/// <returns>Result from Network request, in JSON format.</returns>
			Task<string> RequestFundsFromFaucet();
			event Action<string> RequestFundsFromFaucetSuccess;
			event Action RequestFundsFromFaucetFailure;
		#endregion

		#region GetBalance
			/// <summary>
			/// Obtains the balances of all coins and metadata for all NFTs in the Wallet's current account.
			/// </summary>
			/// <returns>Result from Network request, in JSON format.</returns>
			Task<string> GetBalance ();
			event Action<string> GetBalancesSuccess;
			event Action GetBalancesFailure;
        #endregion

        #region SyncAccount
			/// <summary>
			/// Synchronizes the locally cached account with the network to get the most-recent transactions.
			/// Recommended to use this before and after every network command to keep local wallet up-to-date.
			/// </summary>
			/// <returns>Result from Network request, in JSON format.</returns>
			Task<string> SyncAccount();
			event Action<string> SyncAccountSuccess;
			event Action SyncAccountFailure;
        #endregion

        #region Transaction
        /// <summary>
        /// Sends tokens from the current account to the specified wallet address.
        /// </summary>
        Task<string> Transaction();
            event Action<string> TransactionSuccess;
			event Action TransactionFailure;
        #endregion

        #region GetAccountWithAlias
		/// <summary>
		/// Retrieves an account in the wallet using its alias.
		/// </summary>
			/// <returns>A string containing the account information, in JSON format.</returns>
        Task<string> GetAccountWithAlias();
	        event Action<string> GetAccountWithAliasSuccess;
	        event Action GetAccountWithAliasFailure;
        #endregion

        #region GetAccountIndexes
        /// <summary>
        /// Retrieves the list of the account indexes stored in the wallet.
        /// </summary>
        /// <returns>A string containing the account info.</returns>
        Task<string> GetAccountIndexes();
        event Action<string> GetAccountIndexesSuccess;
        event Action GetAccountIndexesFailure;
        #endregion

        #region GetAccountWithIndex
        /// <summary>
        /// Retrieves an account in the wallet using its index.
        /// </summary>
        /// <returns>A string containing the account details.</returns>
        Task<string> GetAccountWithIndex();
        event Action<string> GetAccountWithIndexSuccess;
        event Action GetAccountWithIndexFailure;
        #endregion

        #region CloseWallet
			/// <summary>
			/// Destroys the wallet pointer to close the wallet.
			/// </summary>
			/// <returns></returns>
	        Task<bool> CloseWallet();
	        event Action<bool> CloseWalletSuccess;
	        event Action<bool> CloseWalletFailure;
        #endregion

        /*
            #region MicroTransaction
                /// <summary>
                /// Send an amount that's smaller than the storage deposit.
                /// </summary>
                Task MicroTransaction (float amountToSend, string coinType, string destinationAddress);
                event Action MicroTransactionSuccess;
                event Action MicroTransactionFailure;
            #endregion
        */
    }
}