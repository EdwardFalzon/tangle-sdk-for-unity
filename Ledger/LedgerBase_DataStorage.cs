﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tangle.Ledger.Wallet;
using UnityEngine;

namespace Tangle.Ledger
{
    public abstract partial class LedgerBase
    {
        [SerializeField] protected DataStorageBase _dataStorage;
        public IDataStorage DataStorage
        {
            get { return _dataStorage; }
            set { _dataStorage = (DataStorageBase)value; }
        }

        #region BuildOutput
        public abstract Task<string> BuildOutput();
        public event Action<string> BuildOutputSuccess;
        public event Action BuildOutputFailure;
        protected virtual void CallBuildOutput(bool successful, string result = null)
        {
            if (successful) BuildOutputSuccess?.Invoke(result);
            else BuildOutputFailure?.Invoke();
        }
        #endregion
    }
}
