﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Events;
using UnityEngine;
using Convert = Tangle.Utilities.Convert;

namespace Tangle.Ledger
{
    public partial class LedgerEvents
    {
        #region UnityEvents
        // Use these events (NOT events from the LedgerBase component) to make a UI and scene respond to Ledger
        // events.
        [SerializeField] protected UnityEvent<string> _OnCreateAliasOutputSuccess;
        [SerializeField] protected UnityEvent _OnCreateAliasOutputFailure;

        [SerializeField] protected UnityEvent<string> _OnCreateNativeTokenSuccess;
        [SerializeField] protected UnityEvent _OnCreateNativeTokenFailure;

        [SerializeField] protected UnityEvent<string> _OnSendNativeTokenSuccess;
        [SerializeField] protected UnityEvent _OnSendNativeTokenFailure;

        [SerializeField] protected UnityEvent<string> _OnPrepareTransactionSuccess;
        [SerializeField] protected UnityEvent _OnPrepareTransactionFailure;

        [SerializeField] protected UnityEvent<string> _OnMeltNativeTokenSuccess;
        [SerializeField] protected UnityEvent _OnMeltNativeTokenFailure;

        [SerializeField] protected UnityEvent<string> _OnBurnNativeTokenSuccess;
        [SerializeField] protected UnityEvent _OnBurnNativeTokenFailure;

        [SerializeField] protected UnityEvent<string> _OnGetUnspentOutputsSuccess;
        [SerializeField] protected UnityEvent _OnGetUnspentOutputsFailure;

        [SerializeField] protected UnityEvent<string> _OnSignAndSubmitTransactionSuccess;
        [SerializeField] protected UnityEvent _OnSignAndSubmitTransactionFailure;

        [SerializeField] protected UnityEvent<string> _OnDestroyFoundrySuccess;
        [SerializeField] protected UnityEvent _OnDestroyFoundryFailure;

        [SerializeField] protected UnityEvent<string> _OnClaimOutputsSuccess;
        [SerializeField] protected UnityEvent _OnClaimOutputsFailure;

        [SerializeField] protected UnityEvent<string> _OnGetClaimableOutputsSuccess;
        [SerializeField] protected UnityEvent _OnGetClaimableOutputsFailure;
        #endregion

        protected virtual void SubscribeTokens()
        {
            _ledgerPassthrough.CreateAliasOutputSuccess += HandleCreateAliasOutputSuccess;
            _ledgerPassthrough.CreateAliasOutputFailure += HandleCreateAliasOutputFailure;

            _ledgerPassthrough.CreateNativeTokenSuccess += HandleCreateNativeTokenSuccess;
            _ledgerPassthrough.CreateNativeTokenFailure += HandleCreateNativeTokenFailure;

            _ledgerPassthrough.SendNativeTokenSuccess += HandleSendNativeTokenSuccess;
            _ledgerPassthrough.SendNativeTokenFailure += HandleSendNativeTokenFailure;

            _ledgerPassthrough.PrepareTransactionSuccess += HandlePrepareTransactionSuccess;
            _ledgerPassthrough.PrepareTransactionFailure += HandlePrepareTransactionFailure;

            _ledgerPassthrough.MeltNativeTokenSuccess += HandleMeltNativeTokenSuccess;
            _ledgerPassthrough.MeltNativeTokenFailure += HandleMeltNativeTokenFailure;

            _ledgerPassthrough.BurnNativeTokenSuccess += HandleBurnNativeTokenSuccess;
            _ledgerPassthrough.BurnNativeTokenFailure += HandleBurnNativeTokenFailure;

            _ledgerPassthrough.GetUnspentOuputsSuccess += HandleGetUnspentOutputsSuccess;
            _ledgerPassthrough.GetUnspentOuputsFailure += HandleGetUnspentOutputsFailure;

            _ledgerPassthrough.SignAndSubmitTransactionSuccess += HandleSignAndSubmitTransactionSuccess;
            _ledgerPassthrough.SignAndSubmitTransactionFailure += HandleSignAndSubmitTransactionFailure;

            _ledgerPassthrough.DestroyFoundrySuccess += HandleDestroyFoundrySuccess;
            _ledgerPassthrough.DestroyFoundryFailure += HandleDestroyFoundryFailure;

            _ledgerPassthrough.ClaimOutputsSuccess += HandleClaimOutputsSuccess;
            _ledgerPassthrough.ClaimOutputsFailure += HandleClaimOutputsFailure;

            _ledgerPassthrough.GetClaimableOutputsSuccess += HandleGetClaimableOutputsSuccess;
            _ledgerPassthrough.GetClaimableOutputsFailure += HandleGetClaimableOutputsFailure;
        }

        protected virtual void UnsubscribeTokens()
        {
            _ledgerPassthrough.CreateAliasOutputSuccess -= HandleCreateAliasOutputSuccess;
            _ledgerPassthrough.CreateAliasOutputFailure -= HandleCreateAliasOutputFailure;

            _ledgerPassthrough.CreateNativeTokenSuccess -= HandleCreateNativeTokenSuccess;
            _ledgerPassthrough.CreateNativeTokenFailure -= HandleCreateNativeTokenFailure;

            _ledgerPassthrough.SendNativeTokenSuccess -= HandleSendNativeTokenSuccess;
            _ledgerPassthrough.SendNativeTokenFailure -= HandleSendNativeTokenFailure;

            _ledgerPassthrough.PrepareTransactionSuccess -= HandlePrepareTransactionSuccess;
            _ledgerPassthrough.PrepareTransactionFailure -= HandlePrepareTransactionFailure;

            _ledgerPassthrough.MeltNativeTokenSuccess -= HandleMeltNativeTokenSuccess;
            _ledgerPassthrough.MeltNativeTokenFailure -= HandleMeltNativeTokenFailure;

            _ledgerPassthrough.BurnNativeTokenSuccess -= HandleBurnNativeTokenSuccess;
            _ledgerPassthrough.BurnNativeTokenFailure -= HandleBurnNativeTokenFailure;

            _ledgerPassthrough.GetUnspentOuputsSuccess -= HandleGetUnspentOutputsSuccess;
            _ledgerPassthrough.GetUnspentOuputsFailure -= HandleGetUnspentOutputsFailure;

            _ledgerPassthrough.SignAndSubmitTransactionSuccess -= HandleSignAndSubmitTransactionSuccess;
            _ledgerPassthrough.SignAndSubmitTransactionFailure -= HandleSignAndSubmitTransactionFailure;

            _ledgerPassthrough.DestroyFoundrySuccess -= HandleDestroyFoundrySuccess;
            _ledgerPassthrough.DestroyFoundryFailure -= HandleDestroyFoundryFailure;

            _ledgerPassthrough.ClaimOutputsSuccess -= HandleClaimOutputsSuccess;
            _ledgerPassthrough.ClaimOutputsFailure -= HandleClaimOutputsFailure;

            _ledgerPassthrough.GetClaimableOutputsSuccess -= HandleGetClaimableOutputsSuccess;
            _ledgerPassthrough.GetClaimableOutputsFailure -= HandleGetClaimableOutputsFailure;
        }

        protected virtual void HandleCreateAliasOutputSuccess(string result) => _OnCreateAliasOutputSuccess?.Invoke(result);
        protected virtual void HandleCreateAliasOutputFailure() => _OnCreateAliasOutputFailure?.Invoke();

        protected virtual void HandleCreateNativeTokenSuccess(string result) => _OnCreateNativeTokenSuccess?.Invoke(result);
        protected virtual void HandleCreateNativeTokenFailure() => _OnCreateNativeTokenFailure?.Invoke();

        protected virtual void HandleSendNativeTokenSuccess(string result) => _OnSendNativeTokenSuccess?.Invoke(result);
        protected virtual void HandleSendNativeTokenFailure() => _OnSendNativeTokenFailure?.Invoke();

        protected virtual void HandlePrepareTransactionSuccess(string result) => _OnPrepareTransactionSuccess?.Invoke(result);
        protected virtual void HandlePrepareTransactionFailure() => _OnPrepareTransactionFailure?.Invoke();

        protected virtual void HandleMeltNativeTokenSuccess(string result) => _OnMeltNativeTokenSuccess?.Invoke(result);
        protected virtual void HandleMeltNativeTokenFailure() => _OnMeltNativeTokenFailure?.Invoke();

        protected virtual void HandleBurnNativeTokenSuccess(string result) => _OnBurnNativeTokenSuccess?.Invoke(result);
        protected virtual void HandleBurnNativeTokenFailure() => _OnBurnNativeTokenFailure?.Invoke();

        protected virtual void HandleGetUnspentOutputsSuccess(string result) => _OnGetUnspentOutputsSuccess?.Invoke(result);
        protected virtual void HandleGetUnspentOutputsFailure() => _OnGetUnspentOutputsFailure?.Invoke();

        protected virtual void HandleSignAndSubmitTransactionSuccess(string result) => _OnSignAndSubmitTransactionSuccess?.Invoke(result);
        protected virtual void HandleSignAndSubmitTransactionFailure() => _OnSignAndSubmitTransactionFailure?.Invoke();

        protected virtual void HandleDestroyFoundrySuccess(string result) => _OnDestroyFoundrySuccess?.Invoke(result);
        protected virtual void HandleDestroyFoundryFailure() => _OnDestroyFoundryFailure?.Invoke();

        protected virtual void HandleClaimOutputsSuccess(string result) => _OnClaimOutputsSuccess?.Invoke(result);
        protected virtual void HandleClaimOutputsFailure() => _OnClaimOutputsFailure?.Invoke();

        protected virtual void HandleGetClaimableOutputsSuccess(string result) => _OnGetClaimableOutputsSuccess?.Invoke(result);
        protected virtual void HandleGetClaimableOutputsFailure() => _OnGetClaimableOutputsFailure?.Invoke();

        #region Ledger passthrough commands
        public void CreateAliasOutput() => _ledgerPassthrough.CreateAliasOutput();
        public void CreateNativeToken() => _ledgerPassthrough.CreateNativeToken();
        public void SendNativeToken() => _ledgerPassthrough.SendNativeToken();
        public void PrepareTransaction() => _ledgerPassthrough.PrepareTransaction();
        public void MeltNativeToken() => _ledgerPassthrough.MeltNativeToken();
        public void BurnNativeToken() => _ledgerPassthrough.BurnNativeToken();
        public void GetUnspentOutputs() => _ledgerPassthrough.GetUnspentOutputs();
        public void SignAndSubmitTransaction() => _ledgerPassthrough.SignAndSubmitTransaction();
        public void DestroyFoundry() => _ledgerPassthrough.DestroyFoundry();
        public void ClaimOutputs() => _ledgerPassthrough.ClaimOutputs();
        public void GetClaimableOutputs() => _ledgerPassthrough.GetClaimableOutputs();
        #endregion

        #region Token passthrough commands
        public void SetCirculatingSupply(string circSupply) => _tokenPassthrough.circulatingSupply = Convert.ToInt(circSupply);
        public void SetMaximumSupply(string maxSupply) => _tokenPassthrough.maximumSupply = Convert.ToInt(maxSupply);
        public void SetTokenName(string tokenName) => _tokenPassthrough.tokenName = tokenName;
        public void SetTokenDescription(string tokenDescription) => _tokenPassthrough.tokenDescription = tokenDescription;
        public void SetTokenSymbol(string tokenSymbol) => _tokenPassthrough.tokenSymbol = tokenSymbol;
        public void SetTokenDecimal(string tokenDecimal) => _tokenPassthrough.tokenDecimals = Convert.ToUint(tokenDecimal);
        public void SetTokenURL(string tokenURL) => _tokenPassthrough.tokenURL = tokenURL;
        public void SetSendTokenAmount(string sendAmount) => _tokenPassthrough.sendTokenAmount = Convert.ToInt(sendAmount);
        public void SetTokenImmutableMedata(string metadata) => _tokenPassthrough.tokenImmutableData = metadata;
        public void SetRecipientAddress(string recipientAddress) => _tokenPassthrough.recipientAddress = recipientAddress;
        public void SetTokenID(string tokenID) => _tokenPassthrough.tokenID = tokenID;
        public void SetReturnStrategy(string returnStrat) => _tokenPassthrough.returnStrategy = returnStrat;
        public void SetOutputsPayload(string outputsPayload) => _tokenPassthrough.outputsPayload = outputsPayload;
        public void SetMeltTokenAmount(string meltAmount) => _tokenPassthrough.meltTokenAmount = Convert.ToInt(meltAmount);
        public void SetBurnTokenAmount(string burnAmount) => _tokenPassthrough.burnTokenAmount = Convert.ToInt(burnAmount);
        public void SetFoundryID(string foundryID) => _tokenPassthrough.foundryID = foundryID;
        public void SetPreparedTransactionPayload(string preparedTransactionPayload) => _tokenPassthrough.preparedTransactionPayload = preparedTransactionPayload;
        public void SetDestroyFoundryID(string destroyFoundryID) => _tokenPassthrough.destroyFoundryID = destroyFoundryID;
        public void SetClaimOutputIds(string outputIds) => _tokenPassthrough.claimOutputIds = outputIds;
        #endregion
    }
}
