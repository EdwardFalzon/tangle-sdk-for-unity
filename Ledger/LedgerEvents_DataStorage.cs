﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine.Events;
using UnityEngine;

namespace Tangle.Ledger
{
    public partial class LedgerEvents
    {
        #region UnityEvents
        // Use these events (NOT events from the LedgerBase component) to make a UI and scene respond to Ledger
        // events.
        [SerializeField] protected UnityEvent<string> _OnBuildOutputSuccess;
        [SerializeField] protected UnityEvent _OnBuildOutputFailure;
        #endregion

        protected virtual void SubscribeDataStorage()
        {
            _ledgerPassthrough.BuildOutputSuccess += HandleBuildOutputSuccess;
            _ledgerPassthrough.BuildOutputFailure += HandleBuildOutputFailure;
        }

        protected virtual void UnsubscribeDataStorage()
        {
            _ledgerPassthrough.BuildOutputSuccess -= HandleBuildOutputSuccess;
            _ledgerPassthrough.BuildOutputFailure -= HandleBuildOutputFailure;
        }

        protected virtual void HandleBuildOutputSuccess(string result) => _OnBuildOutputSuccess?.Invoke(result);
        protected virtual void HandleBuildOutputFailure() => _OnBuildOutputFailure?.Invoke();

        #region Ledger passthrough commands
        public void BuildOutput() => _ledgerPassthrough.BuildOutput();
        #endregion

        #region DataStorage passthrough commands
        public void SetNftImmutableMetadata(string immutableMetadata) => _nftPassthrough.nftImmutableMetadata = immutableMetadata;
        public void SetNftTag(string tag) => _nftPassthrough.nftTag = tag;
        public void SetNftIssuerAddress(string issuer) => _nftPassthrough.nftIssuerAddress = issuer;
        public void SetSendNftAddress(string sendAddress) => _nftPassthrough.sendNftAddress = sendAddress;
        public void SetSendNftId(string sendNftId) => _nftPassthrough.sendNftId = sendNftId;
        public void SetBurnNftId(List<string> burnNftId) => _nftPassthrough.burnNftIds = burnNftId;
        public void SetNftAliases(List<string> nftAlias) => _nftPassthrough.nftAliases = nftAlias;
        #endregion
    }
}
