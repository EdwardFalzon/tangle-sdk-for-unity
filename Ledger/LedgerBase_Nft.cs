﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tangle.Ledger.Wallet;
using UnityEngine;

namespace Tangle.Ledger
{
    public abstract partial class LedgerBase
    {
        [SerializeField] protected NftBase _nft;
        public INft Nft
        {
            get { return _nft; }
            set { _nft = (NftBase)value; }
        }

        #region MintNFT
        public abstract Task<string> MintNFT();
        public event Action<string> MintNFTSuccess;
        public event Action MintNFTFailure;
        protected virtual void CallMintNFT(bool successful, string result = null)
        {
            if (successful) MintNFTSuccess?.Invoke(result);
            else MintNFTFailure?.Invoke();
        }
        #endregion

        #region SendNFT
        public abstract Task<string> SendNFT();
        public event Action<string> SendNFTSuccess;
        public event Action SendNFTFailure;
        protected virtual void CallSendNFT(bool successful, string result = null)
        {
            if (successful) SendNFTSuccess?.Invoke(result);
            else SendNFTFailure?.Invoke();
        }
        #endregion

        #region BurnNFT
        public abstract Task<string> BurnNFT();
        public event Action<string> BurnNFTSuccess;
        public event Action BurnNFTFailure;
        protected virtual void CallBurnNFT(bool successful, string result = null)
        {
            if (successful) BurnNFTSuccess?.Invoke(result);
            else BurnNFTFailure?.Invoke();
        }
        #endregion

        #region DestroyAlias
        public abstract Task<string> DestroyAlias();
        public event Action<string> DestroyAliasSuccess;
        public event Action DestroyAliasFailure;
        protected virtual void CallDestroyAlias(bool successful, string result = null)
        {
            if (successful) DestroyAliasSuccess?.Invoke(result);
            else DestroyAliasFailure?.Invoke();
        }
        #endregion
    }
}
