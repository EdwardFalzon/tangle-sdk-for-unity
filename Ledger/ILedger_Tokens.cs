using System;
using System.Security;
using System.Threading.Tasks;
using Tangle.Ledger.Wallet;

namespace Tangle.Ledger
{
	public partial interface ILedger
	{
        IToken Token { get; set; }

        #region CreateAliasOuput
        /// <summary>
        /// Creates an Alias Output that is needed to create a foundry used to create native tokens. 
        /// The resulting payload will contain the preparedTransaction data needed by SignAndSubmitTransaction to finalize the command.
        /// </summary>
        /// <returns>A string that contains the preparedTransaction payload result of the command that was converted from a pointer.</returns>
        Task<string> CreateAliasOutput();
        event Action<string> CreateAliasOutputSuccess;
        event Action CreateAliasOutputFailure;
        #endregion

        #region CreateNativeToken
        /// <summary>
        /// Creates a Native Token. The resulting payload will contain the preparedTransaction data needed by SignAndSubmitTransaction to finalize the command.
        /// </summary>
        /// <returns>A string that contains the preparedTransaction payload result of the command that was converted from a pointer.</returns>
        Task<string> CreateNativeToken();
        event Action<string> CreateNativeTokenSuccess;
        event Action CreateNativeTokenFailure;
        #endregion

        #region SendNativeToken
        /// <summary>
        /// Sends Native Tokens to another address. The resulting payload will contain the output data needed by PrepareTransaction to generate the essence payload.
        /// The Ouput must be converted to an array before sending it to PrepareTransaction.
        /// </summary>
        /// <returns>A string that contains the outputs payload result of the command that was converted from a pointer.</returns>
        Task<string> SendNativeToken();
        event Action<string> SendNativeTokenSuccess;
        event Action SendNativeTokenFailure;
        #endregion

        #region PrepareTransaction
        /// <summary>
        /// Prepares the Send Native Token command and generate a preparedTransaction payload used by SignAndSubmitTransaction.
        /// </summary>
        /// <returns>A string that contains the preparedTransaction payload result of the command that was converted from a pointer.</returns>
        Task<string> PrepareTransaction();
        event Action<string> PrepareTransactionSuccess;
        event Action PrepareTransactionFailure;
        #endregion

        #region MeltNativeToken
        /// <summary>
        /// Melts Native Tokens and generate the preparedTransaction payload for SignAndSubmitTransaction. Melting will destroy the tokens.
        /// Use this to destroy all the tokens before destroying the foundry they are associated with.
        /// </summary>
        /// <returns>A string that contains the preparedTransaction payload result of the command that was converted from a pointer.</returns>
        Task<string> MeltNativeToken();
        event Action<string> MeltNativeTokenSuccess;
        event Action MeltNativeTokenFailure;
        #endregion

        #region BurnNativeToken
        /// <summary>
        /// Burns Native Tokens and generate the preparedTransaction payload for SignAndSubmitTransaction. Burning will remove tokens from circulation but doesn't destroy them.
        /// If you burn native tokens, the foundry associated by it might never be destroyed.
        /// </summary>
        /// <returns>A string that contains the preparedTransaction payload result of the command that was converted from a pointer.</returns>
        Task<string> BurnNativeToken();
        event Action<string> BurnNativeTokenSuccess;
        event Action BurnNativeTokenFailure;
        #endregion

        #region GetUnspentOutputs
        /// <summary>
        /// Gets the token scheme that contains the amount minted, melted, and maximum supply. Account must contain the foundry ID passed or it will not return anything.
        /// </summary>
        /// <returns>A string that contains the payload result of the command that was converted from a pointer.</returns>
        Task<string> GetUnspentOutputs();
        event Action<string> GetUnspentOuputsSuccess;
        event Action GetUnspentOuputsFailure;
        #endregion

        #region SignAndSubmitTransaction
        /// <summary>
        /// Finalizes the transaction. Required by Create, Send/PrepareTransaction, Melt, and Burn Native Tokens. 
        /// Any command that has a 'Prepare' in its Json structure's method name will need this.
        /// </summary>
        /// <returns>A string that contains the sentTransaction payload result of the command that was converted from a pointer.</returns>
        Task<string> SignAndSubmitTransaction();
        event Action<string> SignAndSubmitTransactionSuccess;
        event Action SignAndSubmitTransactionFailure;
        #endregion

        #region DestroyFoundry
        /// <summary>
        /// Destroys the foundry in the account. All tokens that were generated by the foundry must all be melted first.
        /// </summary>
        /// <returns>A string that contains the preparedTransaction payload result of the command that was converted from a pointer.</returns>
        Task<string> DestroyFoundry();
        event Action<string> DestroyFoundrySuccess;
        event Action DestroyFoundryFailure;
        #endregion

        #region ClaimOutputs
        /// <summary>
        /// Claims Tokens and NFTs that are sent from other wallets.
        /// </summary>
        /// <returns>A string that contains the sentTransaction payload result of the command that was converted from a pointer.</returns>
        Task<string> ClaimOutputs();
        event Action<string> ClaimOutputsSuccess;
        event Action ClaimOutputsFailure;
        #endregion

        #region GetClaimableOutputs
        /// <summary>
        /// Gets a list of claimable outputs that are still locked.
        /// </summary>
        /// <returns>A string that contains a payload with the list of locked outputs.</returns>
        Task<string> GetClaimableOutputs();
        event Action<string> GetClaimableOutputsSuccess;
        event Action GetClaimableOutputsFailure;
        #endregion
    }
}