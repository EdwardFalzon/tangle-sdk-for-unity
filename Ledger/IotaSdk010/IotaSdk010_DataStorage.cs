﻿using System;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using UnityEngine;

namespace Tangle.Ledger.IotaSdk010
{
    public partial class IotaSdk010
    {
        /// <summary>
        /// Sends a metadata and a tag to a recipient address and generates an output payload for PrepareTransaction.
        /// </summary>
        /// <returns>String that contains the outputs payload. Empty string if the command failed.</returns>
        public override async Task<string> BuildOutput()
        {
            // Generate the Json structure of the command.
            var buildOutput = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": {_wallet.Accounts[_wallet.CurrentAccountIndex].index},
                ""method"": {{
                  ""name"": ""prepareOutput"",
                  ""data"": {{
                    ""params"": {{
                      ""recipientAddress"": ""{_dataStorage.RecipientAddress}"",
                      ""features"": {{
                        ""tag"": ""{_dataStorage.Tag}"",
                        ""metadata"": ""{_dataStorage.Metadata}""
                      }},
                      ""storageDeposit"": {{
                        ""returnStrategy"": ""Gift"",
                        ""useExcessIfLow"": true
                      }},
                      ""amount"": ""0""
                    }}
                  }}
                }}
              }}
            }}";

            Debug.Log($"{buildOutput}");

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var buildOutputPtr = call_wallet_method(_wallet.WalletPtr, buildOutput);

            // Call the command's failure event in case there is an error when running the command then return and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.BuildOutput] ERROR: Unable to build output.");
                CallBuildOutput(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(buildOutputPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallBuildOutput(true, result);
            return result;
        }
    }
}
