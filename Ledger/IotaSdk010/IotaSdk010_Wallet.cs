using System;
using System.IO;
using System.Security;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using Newtonsoft.Json;
using Debug = UnityEngine.Debug;
using Security = Tangle.Utilities.Security;

namespace Tangle.Ledger.IotaSdk010
{
    /// <summary>
    /// This partial class declares all commands related to the Tangle wallet.
    /// </summary>
    public partial class IotaSdk010 : LedgerBase
    {
        /// <summary>
        /// Creates a new wallet at WalletLocation with the Password and a Stronghold at StrongholdLocation.
        /// Use preestablished variables in member Wallet.
        /// </summary>
        /// <returns>IntPtr Pointer to the wallet. Null if creation failed.</returns>
        public override async Task<IntPtr> CreateNewWallet()
        {
            var storagePath = Wallet.WalletLocation;
            var snapshotPath = Wallet.StrongholdLocation;
            var password = Wallet.Password;
            
            /*
            if (Directory.Exists(storagePath))
            {
                Debug.LogError($"[IotaSdk010.CreateNewWallet] ERROR: Directory {storagePath} already exists.");
                CallCreateWallet(false, IntPtr.Zero);
                File.Delete(string.Concat(storagePath, "LOCK"));
                return IntPtr.Zero;
            }
            */
            
            // Use constants and provided parameters to set up the walletOptions JSON file.
            var createWalletCommand = $@"
            {{
                ""storagePath"": ""{storagePath}"",
                ""clientOptions"":
                {{
                    ""nodes"": [ ""{_network.node}"" ]
                }},
                ""coinType"": {_network.cointype},
                ""secretManager"":
                {{
                    ""Stronghold"":
                    {{
                        ""snapshotPath"": ""{snapshotPath}"",
                        ""password"": ""{Security.Insecure(password)}""
                    }}
                }}
            }}";
            
            // Create an empty wallet.
            var walletPtr = create_wallet(createWalletCommand);
            ReportLastError($"Wallet created at {storagePath}. Snapshot file {snapshotPath} also created.");

            // At this time, if walletPtr is zero, we cannot proceed.
            if (walletPtr == IntPtr.Zero)
            {
                Debug.LogError($"[IotaSdk010.CreateNewWallet] ERROR: Wallet-creation failed.");
                CallCreateWallet(false, IntPtr.Zero);
                return IntPtr.Zero;
            }

            CallCreateWallet (true, walletPtr);
            return walletPtr;
        }

        /// <summary>
        /// Creates a new account inside a given wallet; Wallets can have multiple accounts.
        /// </summary>
        /// <returns>String containing JSON with account information</returns>
        public override async Task<string> CreateNewAccount()
        {
            // Sample of returned payload (this example is an empty wallet):
            // {
            //     "type"    : "account",
            //     "payload" :
            //     {
            //         "index"           : 0,
            //         "coinType"        : 4219,
            //         "alias"           : "Edward",
            //         "publicAddresses" :
            //         [
            //             {
            //                 "address"  : "rms1qpq5ejwks7chappql35ce8a7qq7s3ulvvvq659dygnrexw4qhgg3x6ex279",
            //                 "keyIndex" : 0,
            //                 "internal" : false,
            //                 "used"     : false
            //             }
            //         ],
            //         "internalAddresses"           : [],
            //         "addressesWithUnspentOutputs" : [],
            //         "outputs"                     : {},
            //         "lockedOutputs"               : [],
            //         "unspentOutputs"              : {},
            //         "transactions"                : {},
            //         "pendingTransactions"         : [],
            //         "incomingTransactions"        : {},
            //         "nativeTokenFoundries"        : {}
            //     }
            // }


            var alias = Wallet.NewAccountName;
            
            // Prepare the createAccount JSON command.
            string createAccountCommand = $@"
            {{
                ""name"": ""createAccount"",
                ""data"": 
                {{
                    ""alias"": ""{alias}"" 
                }}  
            }}";
            
            // Create an account in the given wallet.
            var account = call_wallet_method(Wallet.WalletPtr, createAccountCommand);
            if (ReportLastError())
            {
                CallCreateNewAccount(false);
                return null;
            }

            // Read JSON string from pointer. 
            var accountResultJson = Marshal.PtrToStringAnsi(account);
            
            // Convert JSON to AccountResult class.
            var accountResult = JsonConvert.DeserializeObject<AccountResult>(accountResultJson);

            // Convert back to JSON, but with human-friendly formatting.
            accountResultJson = JsonConvert.SerializeObject(accountResult, Formatting.Indented);
                
            #if DEBUG
                Debug.Log($"[IotaSdk010.CreateAccount] Account: >>{accountResultJson}<<");
            #endif
            
            CallCreateNewAccount(true, accountResultJson);
            return accountResultJson;
        }
        
        public override async Task<string> GenerateNewAddress()
        {
            /* Sample raw output when one address is requested:
            {
	            "type":"generatedEd25519Addresses",
	            "payload":
	            [
                    "rms1qzzk86qv30l4e85ljtccxa0ruy8y7u8zn2dle3g8dv2tl2m4cu227a7n2wj"
	            ]
            }
            */

            // Build the JSON file containing the data required for the command.
            var generateAddressCommand = $@"
            {{
                ""name"": ""generateEd25519Addresses"",
                ""data"": 
                {{
                    ""options"": 
                    {{
                        ""AddressIndex"": 0,
                        ""AccountIndex"": {Wallet.CurrentAccountIndex},
                        ""Bech32Hrp"": ""smr"",
                        ""range"":
                        {{
                            ""start"": 0,
                            ""end"": 1
                        }}
                    }}
                }}
            }}";

            /*
            {
                "name": "generateEd25519Addresses",
                "data": {
                    "options": {
                        "AddressIndex": 0,
                        "AccountIndex": 0,
                        "Bech32Hrp": "smr"
                    }
                }
            }
            */
            
            var addrPtr = call_secret_manager_method(Wallet.SecretManagerPtr, generateAddressCommand);
            if (addrPtr != IntPtr.Zero)
            {
                var addr = Marshal.PtrToStringAnsi(addrPtr);
                if (!ReportLastError($"Addresses created. Payload: {addr}"))
                {
                    CallGenerateAddress(true, addr);
                    return addr;
                }
                else
                {
                    CallGenerateAddress(false);
                    return null;    
                }
            }
            else
            {
                CallGenerateAddress(false);
                return null;
            }
        }
        
        /// <summary>
        /// Requests funds from the faucet of the currently selected network. At this time, the funds are requested for
        /// the first address in the current account of the current wallet. 
        /// </summary>
        /// <returns>A string containing the result of the request. Null if the network has no
        /// faucet URL.</returns>
        public override async Task<String> RequestFundsFromFaucet()
        {
            if (string.IsNullOrEmpty(_network.faucet))
            {
                CallRequestFundsFromFaucet(false);
                return null;
            }

            var address = Wallet.Accounts[Wallet.CurrentAccountIndex].publicAddresses[0].address;
            
            var requestFundsCommand = $@"
            {{
                ""name"": ""requestFundsFromFaucet"",
                ""data"":
                {{
                    ""url"": ""{_network.faucet}"",
                    ""address"": ""{address}""
                }}
            }}";

            if (Wallet.ClientPtr == IntPtr.Zero)
            {
                Wallet.ClientPtr = await GetClient(Wallet.WalletPtr);
            }
            var fundsPtr = call_client_method(Wallet.ClientPtr, requestFundsCommand);
            ReportLastError($"Funds requested from faucet {_network.faucet} for address {address}");

            var result = Marshal.PtrToStringAnsi(fundsPtr);
            
            #if DEBUG
                Debug.Log($"[IotaSdk010.RequestFundsFromFaucet] Result: {result}");
            #endif
            CallRequestFundsFromFaucet(true, result);
            return result;
        }

        /// <summary>
        /// Performs a SyncAccount on the current wallet account, then obtains all balances for the account.
        /// </summary>
        /// <returns>JSON-formatted string containing all balances.</returns>
        public override async Task<string> GetBalance()
        {
            // Sample of JSON result (this example is an empty account):
            // {
            //     "type"    : "balance",
            //     "payload" :
            //     {
            //         "baseCoin":
            //         {
            //             "total"     : "0",
            //             "available" : "0"
            //         },
            //         "requiredStorageDeposit":
            //         {
            //             "basic"     : "0",
            //             "alias"     : "0",
            //             "foundry"   : "0",
            //             "nft"       : "0"
            //         },
            //         "nativeTokens"  : [],
            //         "aliases"       : [],
            //         "foundries"     : [],
            //         "nfts"          : [],
            //         "potentiallyLockedOutputs" : {}
            //     }
            // }

            var getBalanceCommand = $@"
            {{
                ""name"": ""callAccountMethod"",
                ""data"":
                {{
                    ""method"":
                    {{
                        ""GetBalance"":
                        {{
                            ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].alias}""
                        }},
                        ""name"": ""getBalance""                     
                    }},
                    ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].alias}""
                }}
            }}";

            await SyncAccount();
//            await Task.Delay(100);
            
            var balancePtr = call_wallet_method(Wallet.WalletPtr, getBalanceCommand);
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.GetBalance] ERROR: Unable to obtain balances.");
                CallGetBalances(false);
                return String.Empty;
            }

            var balances = Marshal.PtrToStringAnsi(balancePtr);
            Debug.Log(balances);
            // Reformat Json to human-readable.
            //balances = JsonConvert.SerializeObject(JsonConvert.DeserializeObject<BalanceResult>(balances),
            //    Formatting.Indented);
            CallGetBalances(true, balances);
            return balances;
        }

        /// <summary>
        /// Sends a specified amount of baseCoin to a recipient account using its address.
        /// </summary>
        /// <returns></returns>
        public override async Task<string> Transaction()//float amountToSend, string coinType, string destinationAddress
        {
            var sendTransaction = $@"
                {{
                  ""name"": ""callAccountMethod"",
                  ""data"": 
                  {{
                    ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].index}"",
                    ""method"": 
                    {{
                      ""name"": ""send"",
                      ""data"": 
                      {{
                        ""amount"": ""{_wallet.SendAmount}"",
                        ""address"": ""{_wallet.DestinationAddress}"",
                        ""options"": 
                        {{
                            ""remainderValueStrategy"": 
                            {{
                                ""strategy"": ""ReuseAddress""
                            }},
                            ""allowMicroAmount"": true
                        }}
                      }}
                    }}
                  }}
                }}";

            // Send the wallet pointer and the json structure to the DLL.
            var sendTransactionPtr = call_wallet_method(Wallet.WalletPtr, sendTransaction);

            // Display an error message and raise the failure event of the command.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.SendTransaction] ERROR: Unable to send funds.");
                CallTransaction(false);
                return String.Empty;
            }

            // Convert the resulting IntPtr to a readable string.
            var result = Marshal.PtrToStringAnsi(sendTransactionPtr);
            Debug.Log(result);

            // Raise the success event of the command.
            CallTransaction(true, result);
            return result;
        }

/*
        public override async Task MicroTransaction(float amountToSend, string coinType, string destinationAddress)
        {
            Debug.Log("Initiating micro-transaction.");
        }
*/
        
        /// <summary>
        /// Creates a ready-to-go wallet with a mnemonic stored in the wallet's secret manager. The wallet will still
        /// need an account to be created. The secure mnemonic and pointers to the wallet and secret manager will be
        /// made available via their respective Action events.
        /// </summary>
        /// <param name="storagePath"></param>
        /// <param name="snapshotPath"></param>
        /// <param name="password"></param>
        public async Task CreateSecureWallet(string storagePath, string snapshotPath, SecureString password)
        {
            var walletPtr = await CreateNewWallet();
            await Task.Delay(100);
            var secretPtr = await GetSecretManager(walletPtr);
            await Task.Delay(100);
            var mnemonic = await RequestNewMnemonic();
            await Task.Delay(100);
            await StoreMnemonic(secretPtr, mnemonic);
            //await Task.Delay(100);
            //var addrPayload = await GenerateNewAddress(secretPtr);
            //await Task.Delay(100);

            // Temporary code to extract an address
            //var payload = Security.Insecure(addrPayload);
            //await Task.Delay(100);
            //var addr = payload.Substring(payload.IndexOf("rms1"), 63);
            //Debug.Log($"Addr: >>{addr}<<");
            
            //var clientPtr = await GetClient(walletPtr);
            //await Task.Delay(100);
            //if (!string.IsNullOrEmpty(_network.faucet)) await RequestFundsFromFaucet(clientPtr, addr);
            //await Task.Delay(100);
            CloseWallet();
            await Task.Delay(100);
        }
        
        protected virtual async Task<IntPtr> GetSecretManager(IntPtr walletPtr)
        {
            // Get a pointer to the new wallet's Secret Manager.
            var secretManagerPtr = get_secret_manager_from_wallet(walletPtr);
            if (ReportLastError("Secret Manager obtained from wallet."))
            {
                return IntPtr.Zero;
            }

            // At this time, if secretManagerPtr is zero, we cannot proceed.
            if (secretManagerPtr == IntPtr.Zero)
            {
                Debug.LogError($"[IotaSdk010.CreateNewWallet] ERROR: Cannot get Secret Manager from wallet!");
                // TODO: Invoke OnGetSecretManagerFailure.
                return IntPtr.Zero;
            }

            return secretManagerPtr;
        }

        protected override async Task<IntPtr> GetClient(IntPtr walletPtr)
        {
            // Get a pointer to the new wallet's Secret Manager.
            var clientPtr = get_client_from_wallet(walletPtr);
            ReportLastError("Client obtained from wallet.");

            // At this time, if secretManagerPtr is zero, we cannot proceed.
            if (clientPtr == IntPtr.Zero)
            {
                Debug.LogError($"[IotaSdk010.GetClient] ERROR: Cannot get Client from wallet!");
                // TODO: Invoke OnGetClientFailure.
                return IntPtr.Zero;
            }

            return clientPtr;
        }

        /// <summary>
        /// Synchronizes the locally stored account with the network.
        /// </summary>
        /// <returns>A string payload containing the updated balance of the account.</returns>
        public override async Task<string> SyncAccount()
        {
            // Prepare the json structure for the command.
            var syncAccount = $@"
            {{
                ""name"": ""callAccountMethod"",
                ""data"":
                {{
                    ""method"":
                    {{
                        ""name"": ""sync"",
                        ""data"":
                        {{

                        }}
                    }},
                    ""accountId"": ""{_wallet.Alias}""
                }}
            }}";

            // Send the wallet pointer and the json structure to the DLL.
            var syncAccountPtr = call_wallet_method(Wallet.WalletPtr, syncAccount);

            // Display an error message and raise the failure event of the command.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.GetBalance] ERROR: Unable to sync account.");
                CallSyncAccount(false);
                return String.Empty;
            }

            // Convert the resulting IntPtr to a readable string.
            var balances = Marshal.PtrToStringAnsi(syncAccountPtr);
            Debug.Log(balances);

            // Raise the success event of the command.
            CallSyncAccount(true, balances);
            return balances;
        }

        /// <summary>
        /// Retrieves the account stored in the wallet using its alias or name.
        /// </summary>
        /// <returns>A string payload containing the account details.</returns>
        public override async Task<string> GetAccountWithAlias()
        {
            // Prepare the json structure for the command.
            var getAccountWithAlias = $@"
            {{
                ""name"": ""getAccount"",
                ""data"":
                {{
                    ""accountId"": ""{_wallet.Alias}""
                }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var getAccountPtr = call_wallet_method(_wallet.WalletPtr, getAccountWithAlias);

            // Display an error message and raise the failure event of the command.
            if (ReportLastError())
            {
                CallGetAccountWithAlias(false);
                return null;
            }

            // Read JSON string from pointer. 
            var accountResultJson = Marshal.PtrToStringAnsi(getAccountPtr);

            // Convert JSON to AccountResult class.
            var accountResult = JsonConvert.DeserializeObject<AccountResult>(accountResultJson);

            // Convert back to JSON, but with human-friendly formatting.
            accountResultJson = JsonConvert.SerializeObject(accountResult, Formatting.Indented);

            #if DEBUG
            Debug.Log($"[IotaSdk010.GetAccountWithAlias] Account: >>{accountResultJson}<<");
            #endif

            // Raise the success event of the command.
            CallGetAccountWithAlias(true, accountResultJson);
            return accountResultJson;
        }

        /// <summary>
        /// Retrieves the list of indexes of the accounts present in the wallet.
        /// </summary>
        /// <returns>A string payload containing the list/array of indexes in the wallet.</returns>
        public override async Task<string> GetAccountIndexes()
        {
            // Generate the Json structure of the command.
            var getAccountIndexes = $@"
            {{
                ""name"": ""getAccountIndexes""
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var getAccountIndexesPtr = call_wallet_method(_wallet.WalletPtr, getAccountIndexes);

            // Call the command's failure event in case there is an error when running the command then returns and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.GetAccountIndexes] ERROR: Unable to retrieve accounts.");
                CallGetAccountIndexes(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var accountsIndexes = Marshal.PtrToStringAnsi(getAccountIndexesPtr);
            Debug.Log(accountsIndexes);

            // Call the command's success event.
            CallGetAccountIndexes(true, accountsIndexes);
            return accountsIndexes;
        }

        /// <summary>
        /// Retrieves the account stored in the wallet using its index.
        /// </summary>
        /// <returns>A string payload containing the account details.</returns>
        public override async Task<string> GetAccountWithIndex()
        {
            // Generate the Json structure of the command.
            var getAccountWithIndex = $@"
            {{
                ""name"": ""getAccount"",
                ""data"":
                {{
                    ""accountId"": ""{_wallet.Index}""
                }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var getAccountPtr = call_wallet_method(_wallet.WalletPtr, getAccountWithIndex);

            // Call the command's failure event in case there is an error when running the command then returns and empty string.
            if (ReportLastError())
            {
                CallGetAccountWithIndex(false);
                return null;
            }

            // Read JSON string from pointer. 
            var accountResultJson = Marshal.PtrToStringAnsi(getAccountPtr);

            // Convert JSON to AccountResult class.
            var accountResult = JsonConvert.DeserializeObject<AccountResult>(accountResultJson);

            // Convert back to JSON, but with human-friendly formatting.
            accountResultJson = JsonConvert.SerializeObject(accountResult, Formatting.Indented);

            #if DEBUG
            Debug.Log($"[IotaSdk010.GetAccountWithIndex] Account: >>{accountResultJson}<<");
            #endif

            // Call the command's success event.
            CallGetAccountWithIndex(true, accountResultJson);
            return accountResultJson;
        }

        /// <summary>
        /// Destroys the wallet pointer to close the wallet.
        /// </summary>
        /// <returns></returns>
        public override async Task<bool> CloseWallet()
        {
            var result = destroy_wallet(_wallet.WalletPtr);
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.GetBalance] ERROR: Unable to close wallet.");
                CallCloseWallet(result);
                return false;
            }
            if (!result)
            {
                Debug.LogError($"[IotaSdk010.GetBalance] ERROR: Unable to close wallet.");
                return result;
            }
            Debug.Log($"[IotaSdk010.GetBalance] Close Wallet: Wallet has been closed.");
            CallCloseWallet(result);
            return result;
        }
    }
}