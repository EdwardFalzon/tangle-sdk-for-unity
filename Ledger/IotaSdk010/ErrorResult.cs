namespace Tangle.Ledger.IotaSdk010
{
    /// <summary>
    /// ErrorResult is the multi-object class used to deserialise the JSON returned from any command that has resulted
    /// in an error. Use it for more-convenient access to the fields, rather than performing string searches
    /// in the raw JSON file. When deserialising, this structure is appropriate when "type" is "error".
    /// </summary>
    public class ErrorResult
    {
        public string type { get; set; }
        public ErrorPayload payload { get; set; }
    }

    public class ErrorPayload
    {
        public string type { get; set; }
        public string error { get; set; }
    }
}