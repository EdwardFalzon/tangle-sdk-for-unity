using System;
using System.Runtime.InteropServices;

namespace Tangle.Ledger.IotaSdk010
{
    /// <summary>
    /// This partial class declares all bindings required to access the IOTA SDK functions and methods in the compiled
    /// library. This is code for internal usage; you should not modify it.
    /// </summary>
    public partial class IotaSdk010
    {
        #region Operating system selection
            // Select the Iota SDK library according to operating system.
            // These are the compiled libraries released by Iota Foundation.
            // https://github.com/iotaledger/iota-sdk-native-bindings
            #if UNITY_STANDALONE_WIN
                private const string IotaSdkLibrary = "iota_sdk.dll";
            #elif UNITY_STANDALONE_LINUX
                private const string IotaSdkLibrary = "iota_sdk.so";
            #elif UNITY_STANDALONE_OSX
                private const string IotaSdkLibrary = "iota_sdk.dylib";
            #endif
        #endregion

        #region P/Invoke
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern IntPtr create_wallet(string options);
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern IntPtr call_wallet_method(IntPtr wallet, string method);
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern bool   destroy_wallet(IntPtr wallet);
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern IntPtr call_utils_method(string method);
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern string binding_get_last_error();
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern bool   listen_wallet(IntPtr options, string values);
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern IntPtr get_client_from_wallet(IntPtr options);
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern IntPtr call_client_method(IntPtr client, string method);
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern IntPtr call_secret_manager_method(IntPtr options, string values);
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern IntPtr create_secret_manager(string options);
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern IntPtr get_secret_manager_from_wallet(IntPtr wallet);
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern IntPtr call_utils_method();
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern bool   destroy_client(IntPtr clientPtr);
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern bool   destroy_secret_manager(IntPtr secretManagerPtr);
            [DllImport(IotaSdkLibrary, CallingConvention = CallingConvention.Cdecl)] private static extern bool   destroy_string(string stringValue);
        #endregion
    }
}