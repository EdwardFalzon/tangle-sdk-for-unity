﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Tangle.Ledger.Wallet;
using UnityEngine;

namespace Tangle.Ledger.IotaSdk010
{
    public partial class IotaSdk010
    {
        /// <summary>
        /// Mints an NFT and generates a preparedTransaction payload to use with SignAndSubmitTransaction.
        /// </summary>
        /// <returns>String that contains the preparedTransaction payload. Empty string if the command failed.</returns>
        public override async Task<string> MintNFT()
        {
            // Convert the NFT Tag into its hex value.
            byte[] bytes = Encoding.UTF8.GetBytes(_nft.nftTag);
            var tagHex = "0x" + BitConverter.ToString(bytes).Replace("-", "");

            // Generate the Json structure of the command.
            var mintNFT = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].index}"",
                ""method"": {{
                  ""name"": ""prepareMintNfts"",
                  ""data"": {{
                    ""params"": [{{
                      ""tag"": ""{tagHex}"",
                      ""issuer"": ""{_nft.nftIssuerAddress}"",
                      ""immutableMetadata"": ""{_nft.nftImmutableMetadata}""
                    }}]
                  }}
                }}
              }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var mintNFTPtr = call_wallet_method(_wallet.WalletPtr, mintNFT);

            // Call the command's failure event in case there is an error when running the command then return and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.MintNFT] ERROR: Unable to mint NFT.");
                CallMintNFT(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(mintNFTPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallMintNFT(true, result);
            return result;
        }

        /// <summary>
        /// Sends an NFT using an NFT ID to an indicated address and generates a preparedTransaction payload for
        /// SignAndSubmitTransaction.
        /// </summary>
        /// <returns>String that contains the preparedTransaction payload. Empty string if the command failed.</returns>
        public override async Task<string> SendNFT()
        {
            // Generate the Json structure of the command.
            var sendNFT = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": {_wallet.Accounts[_wallet.CurrentAccountIndex].index},
                ""method"": {{
                  ""name"": ""prepareSendNft"",
                  ""data"": {{
                    ""params"": [
                      {{
                        ""address"": ""{_nft.sendNftAddress}"",
                        ""nftId"": ""{_nft.sendNftId}""
                      }}
                    ]
                  }}
                }}
              }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var sendNFTPtr = call_wallet_method(_wallet.WalletPtr, sendNFT);

            // Call the command's failure event in case there is an error when running the command then return and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.SendNFT] ERROR: Unable to send NFT.");
                CallSendNFT(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(sendNFTPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallSendNFT(true, result);
            return result;
        }

        /// <summary>
        /// Burns one or more NFTs and generates a preparedTransaction payload
        /// for SignAndSubmitTransaction. Must pass an array of NFTs.
        /// </summary>
        /// <returns>String that contains the preparedTransaction payload. Empty string if the command failed.</returns>
        public override async Task<string> BurnNFT()
        {
            // Serialize the list of NFTIds for burning into a Json structure.
            var nftJson = JsonConvert.SerializeObject(_nft.burnNftIds);

            // Generate the Json structure of the command.
            var burnNFT = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": {_wallet.Accounts[_wallet.CurrentAccountIndex].index},
                ""method"": {{
                  ""name"": ""prepareBurn"",
                  ""data"": {{
                    ""burn"": {{
                      ""nfts"": {nftJson}
                    }}
                  }}
                }}
              }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var burnNFTPtr = call_wallet_method(_wallet.WalletPtr, burnNFT);

            // Call the command's failure event in case there is an error when running the command then return and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.BurnNFT] ERROR: Unable to burn NFT.");
                CallBurnNFT(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(burnNFTPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallBurnNFT(true, result);
            return result;
        }

        /// <summary>
        /// Destroys one or more Alias Outputs and generates a preparedTransaction payload
        /// for SignAndSubmitTransaction. Pass in an array/list of aliasIDs.
        /// </summary>
        /// <returns>String that contains the preparedTransaction payload. Empty string if the command failed.</returns>
        public override async Task<string> DestroyAlias()
        {
            // Serialize the list of AliasOutputIds into a Json structure.
            var aliasesJson = JsonConvert.SerializeObject(_nft.nftAliases);

            // Generate the Json structure of the command.
            var destroyAlias = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": {_wallet.Accounts[_wallet.CurrentAccountIndex].index},
                ""method"": {{
                  ""name"": ""prepareBurn"",
                  ""data"": {{
                    ""burn"": {{
                      ""aliases"": {aliasesJson}
                    }}
                  }}
                }}
              }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var destroyAliasPtr = call_wallet_method(_wallet.WalletPtr, destroyAlias);

            // Call the command's failure event in case there is an error when running the command then return and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.DestroyAlias] ERROR: Unable to destroy alias output.");
                CallDestroyAlias(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(destroyAliasPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallDestroyAlias(true, result);
            return result;
        }
    }
}
