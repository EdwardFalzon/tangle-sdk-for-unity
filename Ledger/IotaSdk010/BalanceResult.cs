using System.Collections.Generic;

namespace Tangle.Ledger.IotaSdk010
{
    /// <summary>
    /// BalanceResult is the multi-object class used to deserialise the JSON returned from a GetBalance command. Use it
    /// for more-convenient access to the fields, rather than performing string searches in the raw JSON file. When
    /// deserialising, this structure is appropriate when "type" is "balance".
    /// </summary>
    public class BalanceResult
    {
        public string type { get; set; }
        public BalancePayload payload { get; set; }
    }

    public class BalancePayload
    {
        public BaseCoin baseCoin { get; set; }
        public RequiredStorageDeposit requiredStorageDeposit { get; set; }
        public List<NativeToken> nativeTokens { get; set; }
        public List<string> aliases { get; set; }
        public List<string> foundries { get; set; }
        public List<string> nfts { get; set; }
        public Dictionary<string, string> potentiallyLockedOutputs { get; set; }
    }

    public class BaseCoin
    {
        public string total { get; set; }
        public string available { get; set; }
    }

    public class RequiredStorageDeposit
    {
        public string basic { get; set; }
        public string alias { get; set; }
        public string foundry { get; set; }
        public string nft { get; set; }
    }

    public class NativeToken
    {
        public string tokenId { get; set; }
        public string total { get; set; }
        public string available { get; set; }
        public string metadata { get; set; }
    }
}