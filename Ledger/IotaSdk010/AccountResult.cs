using System.Collections.Generic;
using Newtonsoft.Json;

namespace Tangle.Ledger.IotaSdk010
{
    /// <summary>
    /// AccountResult is the multi-object class used to deserialise the JSON returned from a CreateNewAccount or
    /// GetNewAccount command. Use it for more-convenient access to the fields, rather than performing string searches
    /// in the raw JSON file. When deserialising, this structure is appropriate when "type" is "account".
    /// </summary>
    public class AccountResult
    {
        public string type { get; set; }
        public AccountPayload payload { get; set; }
    }

    public class AccountPayload
    {
        public int index { get; set; }
        public int coinType { get; set; }
        public string alias { get; set; }
        public List<Address> publicAddresses { get; set; }
        public List<object> internalAddresses { get; set; }
        public List<object> addressesWithUnspentOutputs { get; set; }
        public Dictionary<string, object> outputs { get; set; }
        public List<object> lockedOutputs { get; set; }
        public Dictionary<string, object> unspentOutputs { get; set; }
        public Dictionary<string, object> transactions { get; set; }
        public List<object> pendingTransactions { get; set; }
        public Dictionary<string, object> incomingTransactions { get; set; }
        public Dictionary<string, object> nativeTokenFoundries { get; set; }
    }

    public class Address
    {
        public string address { get; set; }
        public int keyIndex { get; set; }
        public bool @internal { get; set; }
        public bool used { get; set; }
    }
}