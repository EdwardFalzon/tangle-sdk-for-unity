using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System;
using Debug = UnityEngine.Debug;
using UnityEngine.UIElements;
using System.Text;
using Tangle.Utilities;

namespace Tangle.Ledger.IotaSdk010
{
    public partial class IotaSdk010
    {
        /// <summary>
        /// Creates an Alias Output that is needed to create a foundry used to create native tokens. 
        /// The resulting payload will contain the essence data needed by SignAndSubmitTransaction to finalize the command.
        /// </summary>
        /// <returns>String that contains the preparedTransaction result. Empty string if the command failed.</returns>
        public override async Task<string> CreateAliasOutput()
        {
            // Generate the Json structure of the command.
            var createAliasOutput = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].index}"",
                ""method"": {{
                  ""name"": ""prepareCreateAliasOutput"",
                  ""data"": 
                  {{

                  }}
                }}
              }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var createAliasOutputPtr = call_wallet_method(_wallet.WalletPtr, createAliasOutput);

            // Call the command's failure event in case there is an error when running the command then return and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.CreateAliasOutput] ERROR: Unable to create alias output.");
                CallCreateAliasOutput(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(createAliasOutputPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallCreateAliasOutput(true, result);
            return result;
        }

        /// <summary>
        /// Creates a Native Token with a specific circulating supply and max supply as well as its details converted into a hex immutable metadata.
        /// </summary>
        /// <returns>String that contains the essence payload. Empty string if the command failed.</returns>
        public override async Task<string> CreateNativeToken()
        {
            // Convert the circulating supply and maximum supply values to hex.
            var cSupply = "0x" + _token.circulatingSupply.ToString("X");
            var mSupply = "0x" + _token.maximumSupply.ToString("X");

            Debug.Log($"CircSupply: {cSupply}\nMaxSupply: {mSupply}\nImmutableMetadata: {_token.tokenImmutableData}");

            // Generate the Json structure of the command.
            var createNativeToken = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].index}"",
                ""method"": {{
                  ""name"": ""prepareCreateNativeToken"",
                  ""data"": {{
                    ""params"": {{
                      ""circulatingSupply"": ""{cSupply}"",
                      ""maximumSupply"": ""{mSupply}"",
                      ""foundryMetadata"": ""{_token.tokenImmutableData}""
                    }}
                  }}
                }}
              }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var createNativeTokenPtr = call_wallet_method(_wallet.WalletPtr, createNativeToken);

            // Call the command's failure event in case there is an error when running the command then returns and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.CreateNativeToken] ERROR: Unable to create native token.");
                CallCreateNativeToken(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(createNativeTokenPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallCreateNativeToken(true, result);
            return result;
        }

        /// <summary>
        /// Sends a certain amount of native tokens with the token ID to a recipient address and generates an Outputs payload for 
        /// PrepareTransaction.
        /// </summary>
        /// <returns>String that contains the outputs payload. Empty string if the command failed.</returns>
        public override async Task<string> SendNativeToken()
        {
            // Convert the send amount value to hex.
            var amount = "0x" + _token.sendTokenAmount.ToString("X");

            // Generate the Json structure of the command.
            var sendNativeToken = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].index}"",
                ""method"": {{
                  ""name"": ""prepareOutput"",
                  ""data"": {{
                    ""params"": {{
                      ""recipientAddress"": ""{_token.recipientAddress}"",
                      ""assets"": {{
                        ""nativeTokens"": [
                          {{
                            ""id"": ""{_token.tokenID}"",
                            ""amount"": ""{amount}""
                          }}
                        ]
                      }},
                      ""storageDeposit"": {{
                        ""returnStrategy"": ""{_token.returnStrategy}""
                      }},
                      ""amount"": ""0""
                    }}
                  }}
                }}
              }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var sendNativeTokenPtr = call_wallet_method(_wallet.WalletPtr, sendNativeToken);

            // Call the command's failure event in case there is an error when running the command then returns and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.SendNativeToken] ERROR: Unable to send native token.");
                CallSendNativeToken(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(sendNativeTokenPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallSendNativeToken(true, result);
            return result;
        }

        /// <summary>
        /// Prepares the send transaction output and generates a preparedTransaction payload.
        /// Use this after running the SendNativeToken command and give it the result payload.
        /// The payload from SendNativeToken must be added into a list of Outputs that is serialized to Json.
        /// </summary>
        /// <returns>String that contains the essence payload. Empty string if the command failed.</returns>
        public override async Task<string> PrepareTransaction()
        {
            // Generate the Json structure of the command.
            var prepareTransaction = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].index}"",
                ""method"": {{
                  ""name"": ""prepareTransaction"",
                  ""data"": 
                  {{
                    ""outputs"": {_token.outputsPayload}
                  }}
                }}
              }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var prepareTransactionPtr = call_wallet_method(_wallet.WalletPtr, prepareTransaction);

            // Call the command's failure event in case there is an error when running the command then returns and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.PrepareTransaction] ERROR: Unable to prepare transaction.");
                CallPrepareTransaction(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(prepareTransactionPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallPrepareTransaction(true, result);
            return result;
        }

        /// <summary>
        /// Melts native tokens and destroys them. This will completely destroy the specified amount of tokens.
        /// Use this to destroy all tokens before destroying the foundry.
        /// </summary>
        /// <returns>String that contains the essence payload. Empty string if the command failed.</returns>
        public override async Task<string> MeltNativeToken()
        {
            // Convert the melt amount value to hex.
            var amount = "0x" + _token.meltTokenAmount.ToString("X");

            // Generate the Json structure of the command.
            var meltToken = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].index}"",
                ""method"": {{
                  ""name"": ""prepareMeltNativeToken"",
                  ""data"": {{
                    ""tokenId"": ""{_token.tokenID}"",
                    ""meltAmount"": ""{amount}""
                  }}
                }}
              }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var meltTokenPtr = call_wallet_method(_wallet.WalletPtr, meltToken);

            // Call the command's failure event in case there is an error when running the command then returns and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.MeltNativeToken] ERROR: Unable to melt native token.");
                CallMeltNativeToken(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(meltTokenPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallMeltNativeToken(true, result);
            return result;
        }

        /// <summary>
        /// Burns the native tokens and removes the specified amount from circulation.
        /// If you use this command, you might not be able to destroy the foundry associated by the native token.
        /// </summary>
        /// <returns>String that contains the essence payload. Empty string if the command failed.</returns>
        public override async Task<string> BurnNativeToken()
        {
            // Convert the burn amount value to hex.
            var amount = "0x" + _token.burnTokenAmount.ToString("X");

            // Generate the Json structure of the command.
            var burnToken = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].index}"",
                ""method"": {{
                  ""name"": ""prepareBurn"",
                  ""data"": {{
                    ""burn"": {{
                      ""nativeTokens"": {{
                        ""{_token.tokenID}"": ""{amount}""
                      }}
                    }}
                  }}
                }}
              }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var burnTokenPtr = call_wallet_method(_wallet.WalletPtr, burnToken);

            // Call the command's failure event in case there is an error when running the command then returns and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.BurnNativeToken] ERROR: Unable to burn native token.");
                CallBurnNativeToken(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(burnTokenPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallBurnNativeToken(true, result);
            return result;
        }

        /// <summary>
        /// Gets the token scheme that contains the amount minted, melted, and maximum supply. 
        /// The account must contain the foundry ID passed or it will not return anything.
        /// </summary>
        /// <returns>String that contains the result payload. Empty string if the command failed.</returns>
        public override async Task<string> GetUnspentOutputs()
        {
            // Generate the Json structure of the command.
            var getUnspentOutputs = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].index}"",
                ""method"": {{
                  ""name"": ""unspentOutputs"",
                  ""data"": {{
                    ""filterOptions"": {{
                      ""foundryIds"": [
                        ""{_token.foundryID}""
                      ]
                    }}
                  }}
                }}
              }}
            }}";

            Debug.Log(getUnspentOutputs);

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var getUnspentOutputsPtr = call_wallet_method(_wallet.WalletPtr, getUnspentOutputs);

            // Call the command's failure event in case there is an error when running the command then returns and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.GetUnspentOuputs] ERROR: Unable to get unspent outputs.");
                CallGetUnspentOuputs(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(getUnspentOutputsPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallGetUnspentOuputs(true, result);
            return result;
        }

        /// <summary>
        /// Finalizes a transaction and sends it to the network. 
        /// The preparedTransaction payload must be passed to this by the other commands.
        /// </summary>
        /// <returns>String that contains the result payload. Empty string if the command failed.</returns>
        public override async Task<string> SignAndSubmitTransaction()
        {
            // Generate the Json structure of the command.
            var signAndSubmitTransaction = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].index}"",
                ""method"": {{
                  ""name"": ""signAndSubmitTransaction"",
                  ""data"": {{
                    ""preparedTransactionData"": {_token.preparedTransactionPayload}
                  }}
                }}
              }}
            }}";

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var signAndSubmitTransactionPtr = call_wallet_method(_wallet.WalletPtr, signAndSubmitTransaction);

            // Call the command's failure event in case there is an error when running the command then returns and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.SignAndSubmitTransaction] ERROR: Unable to sign and submit transaction.");
                CallSignAndSubmitTransaction(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(signAndSubmitTransactionPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallSignAndSubmitTransaction(true, result);
            return result;
        }

        /// <summary>
        /// Destroys a foundry and generates a preparedTransaction payload for SignAndSubmitTransaction. 
        /// The native tokens associated by the foundry must all be melted from all accounts holding it.
        /// </summary>
        /// <returns>String that contains the essence payload. Empty string if the command failed.</returns>
        public override async Task<string> DestroyFoundry()
        {
            // Generate the Json structure of the command.
            var destroyFoundry = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": ""{_wallet.Accounts[_wallet.CurrentAccountIndex].index}"",
                ""method"": {{
                  ""name"": ""prepareBurn"",
                  ""data"": {{
                    ""burn"": {{
                      ""foundries"": [
                        ""{_token.destroyFoundryID}""
                      ]
                    }}
                  }}
                }}
              }}
            }}";

            Debug.Log(destroyFoundry);

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var destroyFoundryPtr = call_wallet_method(_wallet.WalletPtr, destroyFoundry);

            // Call the command's failure event in case there is an error when running the command then returns and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.DestroyFoundry] ERROR: Unable to destroy foundry.");
                CallDestroyFoundry(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(destroyFoundryPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallDestroyFoundry(true, result);
            return result;
        }

        /// <summary>
        /// Claims the locked outputs in the account. This method can claim multiple locked outputs at once.
        /// </summary>
        /// <returns>Returns a Sent Transaction result similar to Sign and Submit Transaction.</returns>
        public override async Task<string> ClaimOutputs()
        {
            // Generate the Json structure of the command.
            var claim = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": {_wallet.Accounts[_wallet.CurrentAccountIndex].index},
                ""method"": {{
                  ""name"": ""claimOutputs"",
                  ""data"": {{
                    ""outputIdsToClaim"":
                      {_token.claimOutputIds}
                  }}
                }}
              }}
            }}";

            Debug.Log(claim);

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var claimPtr = call_wallet_method(_wallet.WalletPtr, claim);

            // Call the command's failure event in case there is an error when running the command then returns and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.ClaimOutputs] ERROR: Unable to claim outputs.");
                CallClaimOutputs(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(claimPtr);
            Debug.Log(result);

            // Call the command's success event.
            CallClaimOutputs(true, result);
            return result;
        }

        /// <summary>
        /// Gets the locked outputs that were sent from another account. You can change outputsToClaim field to either 
        /// NativeTokens, Nfts, MicroTransactions, Amount, or All to fine tune it.
        /// </summary>
        /// <returns>Returns a payload of the locked outputs in the account put into a list of strings.</returns>
        public override async Task<string> GetClaimableOutputs()
        {
            // Generate the Json structure of the command.
            var claimable = $@"
            {{
              ""name"": ""callAccountMethod"",
              ""data"": {{
                ""accountId"": {_wallet.Accounts[_wallet.CurrentAccountIndex].index},
                ""method"": {{
                  ""name"": ""claimableOutputs"",
                  ""data"": {{
                    ""outputsToClaim"": ""All""
                  }}
                }}
              }}
            }}";

            Debug.Log(claimable);

            // Call the method of the IotaSDK dll then pass the wallet pointer and the Json structure of the command.
            var claimablePtr = call_wallet_method(_wallet.WalletPtr, claimable);

            // Call the command's failure event in case there is an error when running the command then returns and empty string.
            if (ReportLastError())
            {
                Debug.LogError($"[IotaSdk010.GetClaimableOutputs] ERROR: Unable to get claimable outputs.");
                CallGetClaimableOutputs(false);
                return String.Empty;
            }

            // Convert the pointer result of the command to a string.
            var result = Marshal.PtrToStringAnsi(claimablePtr);
            Debug.Log(result);

            // Call the command's success event.
            CallGetClaimableOutputs(true, result);
            return result;
        }
    }
}