using System;
using System.Security;
using Tangle.Ledger.Wallet;
using UnityEngine;
using UnityEngine.Events;
using Security = Tangle.Utilities.Security;

namespace Tangle.Ledger
{
	/// <summary>
	/// LedgerEvents is a bridge between the Unity Editor and the commands and events for both the Ledger and Wallet.
	/// It is used by Unity designers who wish to wire up Ledger commands and events from within the Editor. This is
	/// useful for UI elements like buttons to initiate a ledger command, and for in-scene objects that should react to
	/// ledger events or changes in Wallet state. Place this component in the same scene (usually on the same
	/// GameObject) as the Ledger component being used.
	///
	/// For UI buttons (etc), wire the interaction events to the public methods found on THIS component; for responsive
	/// objects (such as a coin-balance display, etc) add the component's method to the exposed UnityEvents on THIS
	/// component.
	///
	/// This is a separate component so the Ledger can be swapped out for another without losing all the in-editor
	/// connections that have been wired up.
	/// </summary>
	[DisallowMultipleComponent]
	public partial class LedgerEvents : MonoBehaviour
	{
        /// <summary>
        /// Because LedgerEvents is just a bridge, we need to specify a LedgerBase to pass through all events and
        /// method-calls.
        /// </summary>
        [SerializeField] protected LedgerBase _baseLedgerPassthrough;
        protected ILedger _ledgerPassthrough
        {
            get { return _baseLedgerPassthrough; }
            set { _baseLedgerPassthrough = (LedgerBase)value; }
        }

        /// <summary>
        /// Because LedgerEvents is just a bridge, we need to specify a WalletBase to pass through all events and
        /// method-calls.
        /// </summary>
        [SerializeField] protected WalletBase _baseWalletPassthrough;
        protected IWallet _walletPassthrough
        {
            get { return _baseWalletPassthrough; }
            set { _baseWalletPassthrough = (WalletBase)value; }
        }

        /// <summary>
        /// Just like the Wallet, we also specify the TokenBase to pass through all events and method-calls for tokens.
        /// </summary>
        [SerializeField] protected TokenBase _baseTokenPassthrough;
        protected IToken _tokenPassthrough
        {
            get { return _baseTokenPassthrough; }
            set { _baseTokenPassthrough = (TokenBase)value; }
        }

        /// <summary>
        /// Specify the NFTBase so we could pass through all events and method calls for NFTs.
        /// </summary>
        [SerializeField] protected NftBase _baseNftPassthrough;
        protected INft _nftPassthrough
        {
            get { return _baseNftPassthrough; }
            set { _baseNftPassthrough = (NftBase)value; }
        }

        /// <summary>
        /// Specify the DataStorageBase so we could pass through all events and method calls for DataStorage.
        /// </summary>
        [SerializeField] protected DataStorageBase _baseDataStoragePassthrough;
        protected IDataStorage _dataStoragePassthrough
        {
            get { return _baseDataStoragePassthrough; }
            set { _baseDataStoragePassthrough = (DataStorageBase)value; }
        }

        protected virtual void Awake()
        {
            AssignLedgerComponents();
        }

        /// <summary>
        /// Receive or seek a LedgerBase component and assigns it to the requisite variable.
        /// </summary>
        private void AssignLedgerComponents(ILedger newLedger = null, IWallet newWallet = null, IToken newToken = null, INft newNft = null, IDataStorage newDataStorage = null)
        {
            if (newLedger != null) _ledgerPassthrough = newLedger;
            if (newWallet != null) _walletPassthrough = newWallet;
            if (newToken != null) _tokenPassthrough = newToken;
            if (newNft != null) _nftPassthrough = newNft;
            if (newDataStorage != null) _dataStoragePassthrough = newDataStorage;


            // _ledgerPassthrough and _walletPassthrough must not be null, so if null, check the GameObject and parents
            // for ILedger and IWallet components.
            _ledgerPassthrough ??= GetComponentInParent<ILedger>();
            _walletPassthrough ??= GetComponentInParent<IWallet>();
            _tokenPassthrough ??= GetComponentInParent<IToken>();
            _nftPassthrough ??= GetComponentInParent<INft>();
            _dataStoragePassthrough ??= GetComponentInParent<IDataStorage>();

            // If still null, perform a slower search across the scene.
            _ledgerPassthrough ??= (ILedger)FindObjectOfType<LedgerBase>();
            _walletPassthrough ??= (IWallet)FindObjectOfType<WalletBase>();
            _tokenPassthrough ??= (IToken)FindObjectOfType<TokenBase>();
            _nftPassthrough ??= (INft)FindObjectOfType<NftBase>();
            _dataStoragePassthrough ??= (IDataStorage)FindObjectOfType<DataStorageBase>();
        }

        protected virtual void OnEnable()
        {
            if (_ledgerPassthrough == null) AssignLedgerComponents();
            if (_ledgerPassthrough != null)
            {
                SubscribeWallet();
                SubscribeTokens();
                SubscribeNFT();
                SubscribeDataStorage();
            }
        }

        protected virtual void OnDisable() 
        { 
            UnsubscribeWallet(); 
            UnsubscribeTokens();
            UnsubscribeNFT();
            UnsubscribeDataStorage();
        }
    }
}