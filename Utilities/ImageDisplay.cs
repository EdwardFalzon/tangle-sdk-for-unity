using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

namespace Tangle.Utilities
{
    [RequireComponent(typeof(RawImage))]
    public class ImageDisplay : MonoBehaviour
    {
        [SerializeField] protected string _url = "";

        protected RawImage _image;
        
        protected virtual void OnEnable()
        {
            _image = GetComponent<RawImage>();

            StartCoroutine(DownloadImage(_url));
        }

        private IEnumerator DownloadImage(string url)
        {
            var request = Downloader.GetImage(_url);
            yield return request;

            if (request.webRequest.result == UnityWebRequest.Result.Success)
            {
                var texture = DownloadHandlerTexture.GetContent(request.webRequest);
                GetComponent<RawImage>().texture = texture;
            }
            else
            {
                Debug.LogError("Failed to download image: " + request.webRequest.error);
            }
        }
        
        
    }
}