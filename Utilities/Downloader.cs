using UnityEngine.Networking;

namespace Tangle.Utilities
{
    public static class Downloader
    {
        public static UnityWebRequestAsyncOperation GetImage(string url)
        {
            return UnityWebRequestTexture.GetTexture(url).SendWebRequest();
        }
   }
}