namespace Tangle.Utilities
{
    /// <summary>
    /// Simple type-converters such as string to Int and string to Unsigned Long.
    /// </summary>
    public static class Convert
    {
        public static ulong ToUlong(string value)
        {
            if (ulong.TryParse(value, out ulong ulongValue))
            {
                return ulongValue;
            }
            else
            {
                return 0;
            }
        }

        public static int ToInt(string value)
        {
            if (int.TryParse(value, out int intValue))
            {
                return intValue;
            }
            else
            {
                return -1;
            }
        }
        
        public static uint ToUint(string value)
        {
            if (uint.TryParse(value, out uint uintValue))
            {
                return uintValue;
            }
            else
            {
                return 0;
            }
        }
    }
}