using System.Collections.Generic;
using UnityEngine.Events;

namespace Tangle.Utilities
{
    public class DictionaryEvent : UnityEvent<Dictionary<string, string>>
    {
        
    }
}