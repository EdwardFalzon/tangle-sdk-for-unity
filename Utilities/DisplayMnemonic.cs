using System;
using UnityEngine;
using TMPro;
using Tangle.Ledger.Wallet;

namespace Tangle.Utilities
{
    public class DisplayMnemonic : MonoBehaviour
    {
        public WalletBase _wallet;
        public TMP_InputField mnemonic;

        public void Show()
        {
            CancelInvoke(nameof(HideMnemonic));
            mnemonic.text = Security.Insecure(_wallet.SecureMnemonic);
            Invoke(nameof(HideMnemonic), 10f);
        }

        public void HideMnemonic()
        {
            mnemonic.text = "";
        }
    }
}