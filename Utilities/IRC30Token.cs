using Newtonsoft.Json;

namespace Tangle.Utilities
{
    public class IRC30Token
    {
        public readonly string standard = "IRC30";
        public string name;
        public string description;
        public string symbol;
        public uint decimals;
        public string url;
        public string logoUrl;
        public string logo;

        public IRC30Token SetName(string name)
        {
            this.name = name;
            return this;
        }

        public IRC30Token SetDescription(string description)
        {
            this.description = description;
            return this;
        }

        public IRC30Token SetSymbol(string symbol)
        {
            this.symbol = symbol;
            return this;
        }

        public IRC30Token SetDecimals(uint decimals)
        {
            this.decimals = decimals;
            return this;
        }

        public IRC30Token SetUrl(string url)
        {
            this.url = url;
            return this;
        }

        public IRC30Token SetLogoUrl(string logoUrl)
        {
            this.logoUrl = logoUrl;
            return this;
        }

        public IRC30Token SetLogo(string logo)
        {
            this.logo = logo;
            return this;
        }

        public string ToJson()
        {
            var json = JsonConvert.SerializeObject(this);
            return json;
        }
    }
}