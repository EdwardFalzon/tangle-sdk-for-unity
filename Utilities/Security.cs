using System;
using System.Security;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace Tangle.Utilities
{
    /// <summary>
    /// The Security class helps with keeping sensitive information as secure as possible, using secure storage
    /// and encryption.
    /// </summary>
    public static class Security
    {
        /// <summary>
        /// Converts a string into a SecureString, moving it into secure memory, making it more difficult to read,
        /// then making it read-only. Some sensitive values must be passed and stored as SecureString.
        /// </summary>
        /// <param name="phrase">The string to secure</param>
        /// <returns>The secured version of the string</returns>
        public static SecureString Secure(string phrase)
        {
            SecureString securePhrase = new SecureString();
            
            try
            {
                foreach (char c in phrase) securePhrase.AppendChar(c);
                securePhrase.MakeReadOnly();
            }
            finally
            {
                // Clear the plain text password from memory
                Array.Clear(phrase.ToCharArray(), 0, phrase.Length);
            }

            return securePhrase;
        }

        /// <summary>
        /// Converts a SecureString back to a plain, insecure string. Recommended to use this method as a delegate,
        /// so no variable stores the sensitive plain text.
        /// </summary>
        /// <param name="securePhrase">The secure phrase</param>
        /// <returns>The unsecured phrase as a plain string</returns>
        public static string Insecure(SecureString incomingPhrase)
        {
            if (incomingPhrase == null || incomingPhrase.Length == 0)
            {
                Debug.LogError($"[Security.Insecure] ERROR: securePhrase is empty;");
                return "SecureString Error";
            }

            IntPtr ptr = IntPtr.Zero;

            try
            {
                ptr = Marshal.SecureStringToBSTR(incomingPhrase);
                return Marshal.PtrToStringBSTR(ptr);
            }
            finally
            {
                if (ptr != IntPtr.Zero)
                {
                    // Clear the plain text password from memory
                    Marshal.ZeroFreeBSTR(ptr);
                }
            }
        }

        /// <summary>
        /// Wipes the contents of a dictionary before clearing the dictionary of all entries. Useful when a dictionary
        /// contains sensitive information that should not linger in memory.
        /// </summary>
        /// <param name="dic">The dictionary to be wiped</param>
        /// <typeparam name="T1">The key type</typeparam>
        /// <typeparam name="T2">The value type</typeparam>
        public static void ClearDictionary<T1, T2>(ref Dictionary<T1, T2> dic)
        {
            // foreach (var key in dic.Keys)
            // {
            //     dic[key] = default;
            // }
            dic.Clear();
        }
    }
}