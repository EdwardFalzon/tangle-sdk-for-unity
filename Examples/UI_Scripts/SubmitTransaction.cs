using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tangle.Ledger;

public class SubmitTransaction : MonoBehaviour
{
    public LedgerEvents ledger;

    // Method to prepare and submit a transaction
    public void PrepareTransaction(string result)
    {
        Debug.Log(result);
        var preparedTransaction = JsonConvert.DeserializeObject<PreparedTransaction>(result);
        var serializedPayload = JsonConvert.SerializeObject(preparedTransaction.payload);
        ledger.SetPreparedTransactionPayload(serializedPayload);  // Set the prepared transaction payload in the ledger
        Debug.Log(serializedPayload);
        ledger.SignAndSubmitTransaction(); // Sign and submit the transaction using the ledger
    }

    // Method to prepare and submit a create native token transaction
    public void PrepareCreateNativeTokenTransaction(string result)
    {
        Debug.Log(result);
        var preparedTransaction = JsonConvert.DeserializeObject<PreparedCreateNativeTokenTransaction>(result);
        var serializedPayload = JsonConvert.SerializeObject(preparedTransaction.payload.transaction);
        ledger.SetPreparedTransactionPayload(serializedPayload); // Set the prepared transaction payload in the ledger
        Debug.Log(serializedPayload);
        ledger.SignAndSubmitTransaction(); // Sign and submit the transaction using the ledger   
    }
}


