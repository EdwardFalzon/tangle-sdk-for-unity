using System.Collections;
using System.Collections.Generic;
using Tangle.Ledger.IotaSdk010;
using UnityEngine;
using Tangle.Utilities;
using TMPro;

public class LoadNfts : MonoBehaviour
{
    public IotaSdk010 iotaSdk010;
    public TMP_Dropdown nftDropdown;

    // When the script starts, load the NFT dropdown with options. 
    void Start()
    {
        LoadNftDropdown(iotaSdk010.Wallet.Balances[Convert.ToInt(iotaSdk010.Wallet.Index)].nfts);
    }

    // Load the NFT dropdown with the provided options.
    public void LoadNftDropdown(List<string> dropdownOptions)
    {
        //Clear any existing options in the dropdown
        nftDropdown.ClearOptions();

        foreach (object option in dropdownOptions)
        {
            // Add a new option to the dropdown with the string representation of the option
            nftDropdown.options.Add(new TMP_Dropdown.OptionData(option.ToString()));
        }

        nftDropdown.RefreshShownValue();
    }

}
