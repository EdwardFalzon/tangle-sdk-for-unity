using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CopyAddress : MonoBehaviour
{
    public TextMeshProUGUI addressText; // Reference to the TextMeshProUGUI component

    public void CopyAddressToClipboard()
    {
        if (addressText != null)
        {
            GUIUtility.systemCopyBuffer = addressText.text;
            Debug.Log("Copied to clipboard: " + addressText.text);
        }
    }
}
