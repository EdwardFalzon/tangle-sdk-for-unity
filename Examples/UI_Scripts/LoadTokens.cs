using System.Collections;
using System.Collections.Generic;
using Tangle.Ledger.IotaSdk010;
using UnityEngine;
using Tangle.Utilities;
using TMPro;

public class LoadTokens : MonoBehaviour
{
    public IotaSdk010 iotaSdk010;
    public TMP_Dropdown tokensDropdown;

    // Fetches the native tokens from the wallet balance, and converts the wallet index to integer to get the correct balance from Balances.
    void Start()
    {
        LoadTokensDropdown(iotaSdk010.Wallet.Balances[Convert.ToInt(iotaSdk010.Wallet.Index)].nativeTokens);
    }
    // This method populates the dropdown element with the native tokens, ensuring any previous options are cleared and the new list is displayed.
    public void LoadTokensDropdown(List<NativeToken> dropdownOptions)
    {
        tokensDropdown.ClearOptions();

        foreach (var option in dropdownOptions)
        {
            tokensDropdown.options.Add(new TMP_Dropdown.OptionData(option.tokenId));
        }

        tokensDropdown.RefreshShownValue();
    }

}