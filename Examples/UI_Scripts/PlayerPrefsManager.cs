using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerPrefsManager : MonoBehaviour
{
    [SerializeField] private TMP_InputField walletTextField;
    [SerializeField] private TMP_InputField strongholdTextField;
    [SerializeField] private TMP_InputField passwordTextField;

    private void Start()
    {
        LoadFromPlayerPrefs();
        SetFocusToPassword();
    }

    public void SaveToPlayerPrefs(string walletDirectory, string strongholdDirectory)
    {
        PlayerPrefs.SetString("WalletDirectory", walletTextField.text);
        PlayerPrefs.SetString("StrongholdDirectory", strongholdTextField.text);
        PlayerPrefs.Save();
    }

    private void LoadFromPlayerPrefs()
    {
        if (PlayerPrefs.HasKey("WalletDirectory"))
        {
            walletTextField.text = PlayerPrefs.GetString("WalletDirectory");
            SetFocusToPassword();
            Debug.Log("Wallet Directory loaded: " + walletTextField);
        }
        else
        {
            Debug.Log("Wallet Directory not found in PlayerPrefs.");
        }

        if (PlayerPrefs.HasKey("StrongholdDirectory"))
        {
            strongholdTextField.text = PlayerPrefs.GetString("StrongholdDirectory");
            SetFocusToPassword();
            Debug.Log("Stronghold Directory loaded: " + strongholdTextField);
        }
        else
        {
            Debug.Log("Stronghold Directory not found in PlayerPrefs.");
        }
    }

    private void SetFocusToPassword()
    {
        // Set focus to the password field
        passwordTextField.Select();
        passwordTextField.ActivateInputField();
    }
}
