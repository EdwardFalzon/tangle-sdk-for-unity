using System.Collections;
using System.Collections.Generic;
using Tangle.Ledger.IotaSdk010;
using UnityEngine;
using Tangle.Utilities;
using TMPro;

public class LoadFoundry : MonoBehaviour
{
    public IotaSdk010 iotaSdk010;
    public TMP_Dropdown foundryDropdown;

    // Loads the foundry ID to the dropdown element.
    void Start()
    {
        LoadFoundryDropdown(iotaSdk010.Wallet.Balances[Convert.ToInt(iotaSdk010.Wallet.Index)].foundries);
    }
    // This method populates the dropdown element with the foundry IDs, ensuring any previous options are cleared and the new list is displayed.
    public void LoadFoundryDropdown(List<string> dropdownOptions)
    {
        foundryDropdown.ClearOptions();

        foreach (var option in dropdownOptions)
        {
            foundryDropdown.options.Add(new TMP_Dropdown.OptionData(option));
        }

        foundryDropdown.RefreshShownValue();
    }

}
