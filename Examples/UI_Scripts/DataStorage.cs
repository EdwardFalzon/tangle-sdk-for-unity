using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Tangle.Ledger;
using Tangle.Utilities;
using TMPro;
using UnityEngine;

public class DataStorage : MonoBehaviour
{
    public LedgerEvents ledger;
    public TMP_InputField recipientAddress;
    public TMP_InputField transactionTag;
    public TMP_InputField transactionMetadata;

    public void GetOutputsPayload(string json)
    {
        List<Output> outputs = new();
        var outputsPayload = JsonConvert.DeserializeObject<OutputsPayload>(json);
        outputs.Add(outputsPayload.payload);
        var payload = JsonConvert.SerializeObject(outputs);
        ledger.SetOutputsPayload(payload);
        ledger.PrepareTransaction();
    }
    public void SendData()
    {
        var hexTag = ToHex(transactionTag.text);
        var hexMetadata = ToHex(transactionMetadata.text);

        ledger.SetOutputRecipientAddress(recipientAddress.text);
        ledger.SetOutputTag(hexTag);
        ledger.SetOutputMetadata(hexMetadata);
        ledger.BuildOutput();
    }
    private string ToHex(string str)
    {
        byte[] bytes = Encoding.UTF8.GetBytes(str);
        string hexVal = "0x" + BitConverter.ToString(bytes).Replace("-", "");

        return hexVal;
    }
}
