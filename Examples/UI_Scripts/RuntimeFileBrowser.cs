using UnityEngine;
using System.Collections;
using System.IO;
using SimpleFileBrowser;
using UnityEngine.UI;
using TMPro;
using System;

public class RuntimeFileBrowser : MonoBehaviour
{
    // For displaying the selected directory and stronghold
    public TMP_InputField directoryTextField;
    public TMP_InputField strongholdTextField;
    public TMP_InputField strongholdTextFieldCreate;
    public TMP_InputField directoryTextFieldCreate;

    void Start()
    {
        FileBrowser.SetFilters(true, new FileBrowser.Filter("Stronghold", ".stronghold"), new FileBrowser.Filter("Text Files", ".txt", ".pdf"));
        FileBrowser.SetDefaultFilter(".stronghold");
        FileBrowser.SetExcludedExtensions(".lnk", ".tmp", ".zip", ".rar", ".exe");
        FileBrowser.AddQuickLink("Users", "C:\\Users", null);
    }


    public void OpenLoadDialog(TMP_InputField inp)
    {
        FileBrowser.ShowLoadDialog(
            (paths) =>
            {
                string selectedDirectory = paths[0];
                Debug.Log("Selected Directory: " + selectedDirectory);
                selectedDirectory = selectedDirectory.Replace("\\", "/"); // Replace backslashes with forward slashes                                                          
                //selectedDirectory = selectedDirectory.Replace(" ", ""); // Remove spaces from the selected directory
                Debug.Log("Selected Directory (After Removing Spaces): " + selectedDirectory);
                inp.text = selectedDirectory; // Update the directory text field with the selected directory
            },
            () => { Debug.Log("Canceled"); },
            FileBrowser.PickMode.Folders, false, null, null, "Select Directory", "Select");
    }

    //public void PathStrongholdExplorer()
    //{
    //    FileBrowser.ShowLoadDialog(
    //        (paths) =>
    //        {
    //            string selectedStronghold = paths[0];
    //            Debug.Log("Selected Stronghold: " + selectedStronghold);
    //            selectedStronghold = selectedStronghold.Replace("\\", "/"); // Replace backslashes with forward slashes
    //            //selectedStronghold = selectedStronghold.Replace(" ", ""); // Remove spaces from the selected directory
    //            Debug.Log("Selected Directory (After Removing Spaces): " + selectedStronghold);
    //            strongholdTextField.text = selectedStronghold; // Update the stronghold text field with the selected stronghold
    //        },
    //        () => { Debug.Log("Canceled"); },
    //        FileBrowser.PickMode.FilesAndFolders, false, null, null, "Select Stronghold", "Select");
    //}

    public void OpenSaveDialog(TMP_InputField inp)
    {
        FileBrowser.ShowSaveDialog(
            (paths) =>
            {
                string selectedStronghold = paths[0];
                Debug.Log("Selected Stronghold: " + selectedStronghold);
                selectedStronghold = selectedStronghold.Replace("\\", "/"); // Replace backslashes with forward slashes
                //selectedStronghold = selectedStronghold.Replace(" ", ""); // Remove spaces from the selected directory
                Debug.Log("Selected Directory (After Removing Spaces): " + selectedStronghold);
                inp.text = selectedStronghold; // Update the stronghold text field with the selected stronghold
            },
            () => { Debug.Log("Canceled"); },
            FileBrowser.PickMode.FilesAndFolders, false, null, null, "Select Stronghold", "Select");
    }

    //public void PathDirectoryExplorerCreate()
    //{
    //    FileBrowser.ShowLoadDialog(
    //        (paths) =>
    //        {
    //            string selectedDirectory = paths[0];
    //            Debug.Log("Selected Directory: " + selectedDirectory);
    //            selectedDirectory = selectedDirectory.Replace("\\", "/"); // Replace backslashes with forward slashes                                                          
    //            //selectedDirectory = selectedDirectory.Replace(" ", ""); // Remove spaces from the selected directory
    //            Debug.Log("Selected Directory (After Removing Spaces): " + selectedDirectory);
    //            directoryTextFieldCreate.text = selectedDirectory; // Update the directory text field with the selected directory
    //        },
    //        () => { Debug.Log("Canceled"); },
    //        FileBrowser.PickMode.Folders, false, null, null, "Select Directory", "Select");
    //}

    private void OnLoginCreateWalletClick()
    {
        OpenLoadDialog(directoryTextField);
    }

    private void OnCreateNewWalletClick()
    {
        OpenLoadDialog(directoryTextFieldCreate);
    }

    private void OnLoginSelectStrongholdClick()
    {
        OpenSaveDialog(strongholdTextField);
    }

    private void OnCreateNewStrongholdClick()
    {
        OpenSaveDialog(strongholdTextFieldCreate);
    }
}