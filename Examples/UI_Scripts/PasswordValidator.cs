using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PasswordValidator : MonoBehaviour
{
    [SerializeField] private TMP_InputField passwordField;
    [SerializeField] private TMP_InputField confirmPasswordField;
    [SerializeField] private Button createButton;
    //Check if null
    [SerializeField] private TMP_InputField directoryField;
    [SerializeField] private TMP_InputField strongholdField;

    public void Start()
    {
        // Disable the continue button initially
        createButton.interactable = false;
    }

    // Call this method when the user changes the confirm password field
    public void OnConfirmPasswordChanged()
    {
        ValidatePasswords();
    }

    private void ValidatePasswords()
    {
        string password = passwordField.text;
        string confirmPassword = confirmPasswordField.text;
        //string directory = directoryField.text;
        //string stronghold = strongholdField.text;

        if (password == confirmPassword)
        {
            // Passwords match, enable the button
            createButton.interactable = true;
        }
        else
        {
            // Passwords don't match, disable the button
            createButton.interactable = false;
        }
    }
}
