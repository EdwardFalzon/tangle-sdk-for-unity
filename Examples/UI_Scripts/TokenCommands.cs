using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Tangle.Ledger;
using Tangle.Utilities;
using TMPro;
using UnityEngine;
using Convert = Tangle.Utilities.Convert;

public class TokenCommands : MonoBehaviour
{
    public LedgerEvents ledger;
    public TMP_InputField tokenName;
    public TMP_InputField tokenDescription;
    public TMP_InputField tokenDecimal;
    public TMP_InputField tokenSymbol;
    public TMP_InputField tokenUrl;
    public TMP_InputField tokenLogoUrl;
    public TMP_InputField circulatingSupply;
    public TMP_InputField maximumSupply;
    public TMP_InputField sendAddress;
    public TMP_InputField sendAmount;
    
    public TMP_Dropdown tokenIdSend;
    public TMP_Dropdown tokenIdBurn;
    public TMP_Dropdown tokenIdMelt;
    public TMP_Dropdown destroyFoundry;
    public TMP_Dropdown destroyAlias;
    public TMP_Dropdown returnStrategy;

    // Method to generate token's immutable metadata and pass it to LedgerEvents.SetNFTImmutableMetadata.
    public void GenerateImmutableMetadata()
    {
        //Set the attributes for the native token.
        IRC30Token irc30Token = new IRC30Token()
            .SetName(tokenName.text)
            .SetDescription(tokenDescription.text)
            .SetSymbol(tokenSymbol.text)
            .SetDecimals(Convert.ToUint(tokenDecimal.text))
            .SetLogoUrl(tokenLogoUrl.text)
            .SetUrl(tokenUrl.text);

        //Convert the object to Json after attributes are filled up.
        string jsonString = irc30Token.ToJson();

        //Convert the Json string to its hex representation.
        byte[] bytes = Encoding.UTF8.GetBytes(jsonString);
        string metadata = "0x" + BitConverter.ToString(bytes).Replace("-", "");

        // Instead of displaying the metadata, directly pass it to LedgerEvents.SetTokenImmutableMetadata.
        ledger.SetTokenImmutableMedata(metadata);
        ledger.SetMaximumSupply(maximumSupply.text);
        ledger.SetCirculatingSupply(circulatingSupply.text);
        ledger.CreateNativeToken();

        Debug.Log($"Immutable Metadata: {metadata}");
    }
    // Method to be called for melting the tokens.
    public void MeltToken()
    {
        ledger.SetTokenID(tokenIdMelt.options[tokenIdMelt.value].text);
        Debug.Log(tokenIdMelt.value);
        ledger.MeltNativeToken();
    }

    // Method to be called for burning the tokens.
    public void BurnToken()
    {
        ledger.SetTokenID(tokenIdBurn.options[tokenIdBurn.value].text);
        Debug.Log(tokenIdBurn.value);
        ledger.BurnNativeToken();
    }

    // Method to be called to destroy foundry.
    public void DestroyFoundry()
    {
        ledger.SetDestroyFoundryID(destroyFoundry.options[destroyFoundry.value].text);
        Debug.Log(destroyFoundry.value);
        ledger.DestroyFoundry();
    }

    // Method to be called to destroy alias.
    public void DestroyAlias()
    {
        List<string> aliasIds = new();
        aliasIds.Add(destroyAlias.options[destroyAlias.value].text);
        ledger.SetNftAliases(aliasIds);
        Debug.Log(destroyAlias.value);
        ledger.DestroyAlias();
    }

    public void GetOutputsPayload(string json)
    {
        List<Output> outputs = new();
        var outputsPayload = JsonConvert.DeserializeObject<OutputsPayload>(json);
        outputs.Add(outputsPayload.payload);
        var payload = JsonConvert.SerializeObject(outputs);
        ledger.SetOutputsPayload(payload);
        ledger.PrepareTransaction();
    }
    public void SendNativeToken()
    {
        ledger.SetRecipientAddress(sendAddress.text);
        Debug.Log(tokenIdSend.value);
        ledger.SetSendTokenAmount(sendAmount.text);
        ledger.SetTokenID(tokenIdSend.options[tokenIdSend.value].text);
        ledger.SetReturnStrategy(returnStrategy.options[returnStrategy.value].text);
        ledger.SendNativeToken();
    }
}
