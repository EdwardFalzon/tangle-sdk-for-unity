using System.Collections;
using System.Collections.Generic;
using Tangle.Ledger.IotaSdk010;
using UnityEngine;
using Tangle.Utilities;
using TMPro;

public class LoadAlias : MonoBehaviour
{
    public IotaSdk010 iotaSdk010;
    public TMP_Dropdown aliasDropdown;

    public void LoadAliasDropdown()
    {
        GenerateAliasDropdown(iotaSdk010.Wallet.Balances[Convert.ToInt(iotaSdk010.Wallet.Index)].aliases);
    }

    // This method populates the dropdown element with the foundry IDs, ensuring any previous options are cleared and the new list is displayed.
    public void GenerateAliasDropdown(List<string> dropdownOptions)
    {
        aliasDropdown.ClearOptions();

        foreach (var option in dropdownOptions)
        {
            aliasDropdown.options.Add(new TMP_Dropdown.OptionData(option));
        }

        aliasDropdown.RefreshShownValue();
    }

    private void OnEnable()
    {
        LoadAliasDropdown();
    }

}