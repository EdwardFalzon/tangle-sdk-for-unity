using System.Collections;
using System.Collections.Generic;
using Tangle.Ledger;
using Tangle.Ledger.IotaSdk010;
using Tangle.Ledger.Wallet;
using Tangle.Utilities;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DisplayBalanceAndAccounts : MonoBehaviour
{
    public TMP_Text balanceText;
    public TMP_Text accountNameText;
    public TMP_Text accountAddressText;
    public GameObject TangleLedger;
    //public float refresh = 5f;

    private IotaSdk010 iotaSdk010;
    private LedgerEvents ledger;

    void Start()
    {

        // Check if Tangle SDK GameObject is found
        if (TangleLedger != null)
        {
            // Get the Wallet component attached to the Tangle SDK GameObject
            iotaSdk010 = TangleLedger.GetComponent<IotaSdk010>();
            ledger = TangleLedger.GetComponent<LedgerEvents>();

            if (iotaSdk010 != null)
            {
                // Call UpdateWalletUI immediately and then every 3 seconds
                InvokeRepeating(nameof(SyncAccount), 3f, 60f);
            }
            else
            {
                Debug.LogError("Wallet component not found on Tangle SDK GameObject!");
            }
        }
        else
        {
            Debug.LogError("Tangle SDK GameObject not found!");
        }
    }

    public void UpdateWalletUI()
    {
        try
        {
            // Check if the base coin balance is null for the current wallet index. If the base coin balance is null, display "null" in the balance text.
            // If the base coin balance is not null, display the total amount of base coin in the balance text.
            if (iotaSdk010.Wallet.Balances[Convert.ToInt(iotaSdk010.Wallet.Index)].baseCoin == null)
                balanceText.text = "null";
            else
            {
                double totalBalance = Convert.ToInt(iotaSdk010.Wallet.Balances[iotaSdk010.Wallet.CurrentAccountIndex].baseCoin.total) / 1000000.0;
                string formattedBalance = string.Format("{0:0.##} SMR", totalBalance);
                balanceText.text = formattedBalance;
            }
                

            // Display current account name.
            // Check if the account alias is null for the current account index.
            if (iotaSdk010.Wallet.Accounts[iotaSdk010.Wallet.CurrentAccountIndex].alias == null)
            {
                accountNameText.text = "null";
                accountAddressText.text = "null";
            }
            else
            {
                // If the account alias is not null, display the alias in the account name text.
                // Display the first public address associated with the account in the account address text
                accountNameText.text = iotaSdk010.Wallet.Accounts[iotaSdk010.Wallet.CurrentAccountIndex].alias;
                accountAddressText.text = iotaSdk010.Wallet.Accounts[iotaSdk010.Wallet.CurrentAccountIndex].publicAddresses[0].address;
            }
        }
        catch
        {
            Debug.Log("catch catch");
        }
           
    }
    // Call the SyncAccount method on the ledger object to synchronize the account data
    public void SyncAccount()
    {

        ledger.SyncAccount();
    }

}
