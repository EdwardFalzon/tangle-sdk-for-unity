using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Tangle.Ledger;
using TMPro;
using UnityEngine;

public class NFTCommands : MonoBehaviour
{
    public LedgerEvents ledger;
    public TMP_InputField issuerAddress;
    public TMP_InputField nftTag;
    public TMP_InputField nftName;
    public TMP_InputField collectionName;
    public TMP_InputField nftUrl;
    public TMP_InputField issuerName;
    public TMP_InputField description;
    public TMP_InputField attribType1;
    public TMP_InputField attribVal1;
    public TMP_InputField attribType2;
    public TMP_InputField attribVal2;
    public TMP_InputField sendAddress;
    public TMP_Dropdown nftIdSend;
    public TMP_Dropdown nftIdBurn;

    // Method to generate NFT's immutable metadata and pass it to LedgerEvents.SetNFTImmutableMetadata
    public void GenerateAndSetImmutableMetadata()
    {
        //Set the attributes for the NFT.
        NftIrc27Builder nftIrc27Builder = new NftIrc27Builder("image/jpeg", nftName.text, nftUrl.text)
                .SetCollectionName(collectionName.text)
                .SetIssuerName(issuerName.text)
                .SetDescription(description.text)
                .AddAttribute(attribType1.text, attribVal1.text)
                .AddAttribute(attribType2.text, attribVal2.text);

        //Convert the object to Json after attributes are filled up.
        string jsonString = nftIrc27Builder.ToJson();

        //Convert the Json string to its hex representation.
        byte[] bytes = Encoding.UTF8.GetBytes(jsonString);
        string metadata = "0x" + BitConverter.ToString(bytes).Replace("-", "");

        // Instead of displaying the metadata, directly pass it to LedgerEvents.SetNFTImmutableMetadata
        ledger.SetNftImmutableMetadata(metadata);
        ledger.SetNftIssuerAddress(issuerAddress.text);
        ledger.SetNftTag(nftTag.text);
        ledger.MintNFT();

        Debug.Log("Immutable Metadata generated and set successfully.");
    }
    // Method to call for sending the NFT.
    public void SendNft()
    {
        ledger.SetSendNftAddress(sendAddress.text);
        Debug.Log(nftIdSend.value);
        ledger.SetSendNftId(nftIdSend.options[nftIdSend.value].text);
        ledger.SendNFT();
    }
    // Method to call for burning the NFT.
    public void BurnNft()
    {
        List<string> nftIds = new();
        nftIds.Add(nftIdBurn.options[nftIdBurn.value].text);
        ledger.SetBurnNftId(nftIds);
        Debug.Log(nftIdBurn.value);
        ledger.BurnNFT();
    }
}
