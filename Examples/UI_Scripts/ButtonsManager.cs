using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine;
using Tangle.Ledger;
using Newtonsoft.Json;
using Tangle.Ledger.IotaSdk010;
using Tangle.Utilities;
using TMPro;

public class ButtonsManager : MonoBehaviour
{
    public LedgerEvents ledger;
    public IotaSdk010 iotaSdk010;
    public GameObject LoginScreen;
    public GameObject Dashboard;
    public GameObject CreateWalletScreen;
    public GameObject CreateMnemonicScreen;
    public GameObject SidePanel;
    public GameObject CreateAccountPanel;
    public GameObject WalletPanel;
    public GameObject NFTPanel;
    public GameObject TokenPanel;
    public GameObject DataStoragePanel;
    public TMP_Dropdown SelectAccount;

    public Button mnemonicsContinueButton;

    private bool isLoggedIn = false;
    private bool isCreatingAccount = false;
    private bool isNewAccount = false;

    private AccountIndex accountIndex;

    private void Start()
    {
        // Initial state of the continue button should be disabled
        mnemonicsContinueButton.interactable = false;
    }

    // Method for handling button clicks
    public void OnButtonClick(string buttonName)
    {
        if (buttonName == "btnLoginWallet")
        {
            OnLoginButtonClick();
        }
        else if (buttonName == "btnCreateWallet")
        {
            OnCreateWalletButtonClick();
        }
        else if (buttonName == "btnWallet")
        {
            ActivateWalletPanel();
        }
        else if (buttonName == "btnTokens")
        {
            ActivateTokenPanel();
        }
        else if (buttonName == "btnNFT")
        {
            ActivateNFTPanel();
        }
        else if (buttonName == "btnDataStorage")
        {
            ActivateDataStoragePanel();
        }
        else if (buttonName == "btnLogout")
        {
            OnLogoutButtonClick();
        }
    }

    // Handles the log in button activity. Executed when the log in button is clicked.
    public void OnLoginButtonClick()
    {
        isLoggedIn = true;
        isCreatingAccount = false;
        Debug.Log("Log in button clicked");
    }

    // Handles the create wallet button activity. Executed when the log in button is clicked.
    public void OnCreateWalletButtonClick()
    {
        isLoggedIn = false;
        isCreatingAccount = true;
        Debug.Log("Create wallet button clicked");
    }

    // Handles the logout button click event
    public void OnLogoutButtonClick()
    {
        // Update login status and reset account creation status.
        isLoggedIn = false;
        isCreatingAccount = false; 

        // Deactivate all panels
        Dashboard.SetActive(false);
        CreateWalletScreen.SetActive(false);
        CreateMnemonicScreen.SetActive(false);
        CreateAccountPanel.SetActive(false);
        NFTPanel.SetActive(false);
        TokenPanel.SetActive(false);
        DataStoragePanel.SetActive(false);

        // Activate the login screen
        LoginScreen.SetActive(true);

        Debug.Log("Logged out");
    }

    // Method to activate wallet panel
    public void ActivateWalletPanel()
    {
        WalletPanel.SetActive(true);
        TokenPanel.SetActive(false);
        NFTPanel.SetActive(false);
        DataStoragePanel.SetActive(false);
    }

    // Method to activate token panel
    public void ActivateTokenPanel()
    {
        WalletPanel.SetActive(false);
        TokenPanel.SetActive(true);
        NFTPanel.SetActive(false);
        DataStoragePanel.SetActive(false);
    }

    // Method to activate NFT panel
    public void ActivateNFTPanel()
    {
        WalletPanel.SetActive(false);
        TokenPanel.SetActive(false);
        NFTPanel.SetActive(true);
        DataStoragePanel.SetActive(false);
    }

    // Method to activate data storage panel
    public void ActivateDataStoragePanel()
    {
        WalletPanel.SetActive(false);
        TokenPanel.SetActive(false);
        NFTPanel.SetActive(false);
        DataStoragePanel.SetActive(true);
    }

    // Method to handle success, this will be called after a successful login or account creation.
    public void OnClickSuccess()
    {
        Debug.Log("Button click success");

        if (isLoggedIn)
        {
            Debug.Log("is logging in");
            ledger.GetAccountIndexes();
            Debug.Log("got account indexes");
        }
        else if (isCreatingAccount)
        {
            Debug.Log("is creating new account");
            CreateWalletScreen.SetActive(false);
            CreateMnemonicScreen.SetActive(true);
            CreateAccountPanel.SetActive(false);
        }
    }

    // Handles the successful retrieval of account index data
    public void OnIndexRetrievalSuccess(string json)
    {
        accountIndex = JsonConvert.DeserializeObject<AccountIndex>(json);

        if (accountIndex.payload.Count > 0 && !isNewAccount)
        {
            ledger.SetAccountIndex(accountIndex.payload[0]);
            ledger.GetAccountWithIndex();
            LoadDropdown(accountIndex.payload);
        }
        else if (isNewAccount)
        {
            var newAccount = accountIndex.payload.Count - 1;
            ledger.SetAccountIndex(accountIndex.payload[newAccount]);
            ledger.GetAccountWithIndex();
            LoadDropdown(accountIndex.payload);
        }
    }

    // Handles the successful retrieval of account data.
    // Set the alias for the ledger to the alias of the current account in the wallet and synchronize the account data with the ledger.
    public void OnAccountRetrievalSuccess()
    {
        ledger.SetAlias(iotaSdk010.Wallet.Accounts[Convert.ToInt(iotaSdk010.Wallet.Index)].alias);
        ledger.SyncAccount();
    }

    // Loads the dropdown menu with the provided options
    public void LoadDropdown(List<string> dropdownOptions)
    {
        SelectAccount.ClearOptions();

        foreach(string option in dropdownOptions)
        {
            SelectAccount.options.Add(new TMP_Dropdown.OptionData("Account " + option));
        }

        SelectAccount.options.Add(new TMP_Dropdown.OptionData("Create Account"));
        SelectAccount.value = dropdownOptions.Count - 1;
        SelectAccount.RefreshShownValue();
        isNewAccount = false;
    }

    // Loads the account based on the selected option in the dropdown
    public void LoadAccountWithDropdown()
    {
        var pickedAccount = SelectAccount.value;
        if (pickedAccount < SelectAccount.options.Count - 1)
        {
            ledger.SetAccountIndex(pickedAccount.ToString());
            ledger.GetAccountWithIndex();
        }
        else if (pickedAccount >= SelectAccount.options.Count - 1)
        {
            Dashboard.SetActive(false);
            CreateAccountPanel.SetActive(true);
        }
    }

    public void DisplayNewAccount()
    {
        isNewAccount = true;
    }
}

public class AccountIndex
{
    public string type { get; set; }
    public List<string> payload { get; set; }
}

