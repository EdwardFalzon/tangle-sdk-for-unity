using System;
using System.Collections;
using TMPro;
using UnityEngine;

namespace Tangle.Examples
{
    /// <summary>
    /// A handy tool to display a notification to the player. Attach to a TMP object, then set it to listen to
    /// LedgerEvents.
    /// </summary>
    public class Notifications : MonoBehaviour
    {
        [SerializeField] protected Color _successColor = Color.green;
        [SerializeField] protected Color _failureColor = Color.red;
        [SerializeField] protected float _revealTime = 0.5f;
        [SerializeField] protected float _displayTime = 3f;
        [SerializeField] protected float _hideTime = 0.5f;
        
        protected TMP_Text _notificationText;

        protected void Awake()
        {
            _notificationText = GetComponent<TMP_Text>();
        }

        public void DisplaySuccess(string notification)
        {
            StartCoroutine(DisplayNotification(notification, _successColor));
        }

        public void DisplayFailure(string notification)
        {
            StartCoroutine(DisplayNotification(notification, _failureColor));
        }

        protected IEnumerator DisplayNotification(string notification, Color color)
        {
            yield return StartCoroutine(RevealNotification(notification, color));
            yield return new WaitForSeconds(_displayTime);
            yield return StartCoroutine(HideNotification());
        }

        protected IEnumerator RevealNotification(string newNotification, Color color)
        {
            // Clear the visible string and set the color.
            _notificationText.text = string.Empty;
            _notificationText.color = color;
            
            // Calculate the delay between revealing each letter, based on the over-all _revealTime.
            float delay = _revealTime / (float)newNotification.Length;

            for (int i = 1; i < newNotification.Length; i++)
            {
                _notificationText.text = newNotification.Substring(0, i);
                yield return new WaitForSeconds(delay);
            }
        }

        protected IEnumerator HideNotification()
        {
            // If the notifciation text is already empty, there's nothing to do here.
            if (string.IsNullOrEmpty(_notificationText.text)) yield return null;
            
            // Calculate the delay between removing each letter, based on the over-all _hideTime.
            float delay = _hideTime / _notificationText.text.Length;
            
            while (!string.IsNullOrEmpty(_notificationText.text))
            {
                _notificationText.text = _notificationText.text.Substring(0, _notificationText.text.Length - 1);
                yield return new WaitForSeconds(delay);
            }
        }
    }
}
