using UnityEngine;

namespace Tangle
{
    [CreateAssetMenu(fileName = "Network", menuName = "Tangle/Network", order = 1)]
    public class NetworkDetails : ScriptableObject
    {
        [Tooltip("Node")]
        public string node;
        
        [Tooltip("Cointype")]
        public string cointype;
        
        [Tooltip("Faucet address, if any")]
        public string faucet;
    }
}